/*
 *  Document   : op_auth_signup.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Sign Up Page
 */

var OpAuthSignUp = function() {
    // Init Sign Up Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    var initValidationSignUp = function(){
        jQuery('.js-validation-signup').validate({
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'daftar-nama': {
                    required: true,
                    minlength: 3
                },
                'daftar-email': {
                    required: true,
                    email: true
                },
                'daftar-phone': {
                    required: true,
                    email: true
                },
                'daftar-password': {
                    required: true,
                    minlength: 5
                },
                'daftar-knf_password': {
                    required: true,
                    equalTo: '#signup-password'
                }
            },
            messages: {
                'daftar-nama': {
                    required: 'Nama Lengkap Tidak Boleh Kosong',
                    minlength: 'Nama Lengkap Tidak Boleh Kurang Dari 3 Karakter'
                },
                'daftar-email': 'Alamat Email Tidak Boleh Kosong',
                'daftar-phone': 'No. Handphone Tidak Boleh Kosong',
                'daftar-password': {
                    required: 'Kata Sandi Tidak Boleh Kosong',
                    minlength: 'Kata Sandi Tidak Boleh Kurang Dari 5 Karakter'
                },
                'daftar-knf_password': {
                    required: 'Konfirmasi Kata Sandi Tidak Boleh Kosong',
                    minlength: 'Konfirmasi Kata Sandi Tidak Boleh Kurang Dari 5 Karakter',
                    equalTo: 'Konfirmasi Kata Sandi Tidak Sama'
                },
            }
        });
    };

    return {
        init: function () {
            // Init SignUp Form Validation
            initValidationSignUp();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ OpAuthSignUp.init(); });