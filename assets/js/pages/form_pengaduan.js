/*
 *  Document   : be_forms_wizard.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Form Wizard Page
 */

var BeFormWizard = function() {
    // Init Wizard defaults
    var initWizardDefaults = function(){
        jQuery.fn.bootstrapWizard.defaults.tabClass         = 'nav nav-tabs';
        jQuery.fn.bootstrapWizard.defaults.nextSelector     = '[data-wizard="next"]';
        jQuery.fn.bootstrapWizard.defaults.previousSelector = '[data-wizard="prev"]';
        jQuery.fn.bootstrapWizard.defaults.firstSelector    = '[data-wizard="first"]';
        jQuery.fn.bootstrapWizard.defaults.lastSelector     = '[data-wizard="lsat"]';
        jQuery.fn.bootstrapWizard.defaults.finishSelector   = '[data-wizard="finish"]';
        jQuery.fn.bootstrapWizard.defaults.backSelector     = '[data-wizard="back"]';
    };

    // Init simple wizard, for more examples you can check out https://github.com/VinceG/twitter-bootstrap-wizard
    var initWizardSimple = function(){
        jQuery('.js-wizard-simple').bootstrapWizard({
            onTabShow: function(tab, navigation, index) {
                var percent = ((index + 1) / navigation.find('li').length) * 100;

                // Get progress bar
                var progress = navigation.parents('.block').find('[data-wizard="progress"] > .progress-bar');

                // Update progress bar if there is one
                if (progress.length) {
                    progress.css({ width: percent + 1 + '%' });
                }
            }
        });
    };

    var initWizardValidation = function(){
        // Get forms
        var formClassic     = jQuery('.js-wizard-validation-classic-form');

        // Prevent forms from submitting on enter key press
        formClassic.on('keyup keypress', function (e) {
            var code = e.keyCode || e.which;

            if (code === 13) {
                e.preventDefault();
                return false;
            }
        });

        // Init form validation on classic wizard form
        var validatorClassic = formClassic.validate({
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'nama': {
                    required: true,
                    minlength: 2
                },
                'tmp_lahir': {
                    required: true,
                },
                'tgl_lahir': {
                    required: true,
                },
                'jk': {
                    required: true,
                },
                'no_passport': {
                    required: true
                },
                'id_kecamatan': {
                    required: true,
                },
                'id_kelurahan': {
                    required: true,
                },
                'alamat_asal': {
                    required: true,
                },
                'alamat_kerja': {
                    required: true
                },
                'embarsi': {
                    required: true
                },
                'debarsi': {
                    required: true
                },
                // PPTKIS
                'nm_pptkis': {
                    required: true,
                    minlength: 2
                },
                'negara': {
                    required: true,
                },
                'mulai_kerja': {
                    required: true,
                },
                'akhir_kerja': {
                    required: true,
                },
                'lama_kerja': {
                    required: true
                },
                'nm_agency': {
                    required: true
                },
                'f_nama_mjk': {
                    required: true,
                },
                'l_nama_mjk': {
                    required: true,
                },
                'jml_mjk': {
                    required: true,
                },
                'alamat_mjk': {
                    required: true
                },
                'masalah': {
                    required: true
                },
                'tuntutan': {
                    required: true
                }
            },
            messages: {
                'nama': {
                    required: 'Nama Lengkap Tidak Boleh Kosong',
                    minlength: 'Nama Lengkap Tidak Boleh Kurang Dari 2 Karakter'
                },
                'tmp_lahir': {
                    required: 'Tempat Lahir Tidak Boleh Kosong',
                    minlength: 'Tempat Lahir Tidak Boleh Kurang Dari 2 Karakter'
                },
                'tgl_lahir': 'Tanggal Lahir Tidak Boleh Kosong',
                'jk': 'Jenis Kelamin Tidak Boleh Kosong',
                'no_passport': 'No. Passport Tidak Boleh Kosong',
                'id_kecamatan': 'Kecamatan Tidak Boleh Kosong',
                'id_kelurahan': 'Kelurahan Tidak Boleh Kosong',
                'alamat_asal': 'Alamat Asal Tidak Boleh Kosong',
                'alamat_kerja': 'Alamat Kerja Tidak Boleh Kosong',
                'embarsi': 'Embarkasi Tidak Boleh Kosong',
                'debarsi': 'Debarkasi Tidak Boleh Kosong',
                'nm_pptkis': 'Nama PPTKIS Tidak Boleh Kosong',
                'negara': 'Negara Penempatan Tidak Boleh Kosong',
                'mulai_kerja': 'Tanggal Mulai Kerja Tidak Boleh Kosong',
                'akhir_kerja': 'Tanggal Terakhir Kerja Tidak Boleh Kosong',
                'lama_kerja': 'Lama Kerja Tidak Boleh Kosong',
                'nm_agency': 'Nama Agency Tidak Boleh Kosong',
                'f_nama_mjk': 'Nama Awal Majikan Tidak Boleh Kosong',
                'l_nama_mjk': 'Nama Akhir Majikan Tidak Boleh Kosong',
                'jml_mjk': 'Jumlah Majikan Tidak Boleh Kosong',
                'alamat_mjk': 'Alamat Majikan Tidak Boleh Kosong',
                'masalah': 'Masalah Tidak Boleh Kosong',
                'tuntutan': 'Tuntutan Tidak Boleh Kosong',
            }
        });


        // Init classic wizard with validation
        jQuery('.js-wizard-validation-classic').bootstrapWizard({
            tabClass: '',
            onTabShow: function(tab, navigation, index) {
                var percent = ((index + 1) / navigation.find('li').length) * 100;

                // Get progress bar
                var progress = navigation.parents('.block').find('[data-wizard="progress"] > .progress-bar');

                // Update progress bar if there is one
                if (progress.length) {
                    progress.css({ width: percent + 1 + '%' });
                }
            },
            onNext: function(tab, navigation, index) {
                if( !formClassic.valid() ) {
                    validatorClassic.focusInvalid();
                    return false;
                }
            },
            onTabClick: function(tab, navigation, index) {
                jQuery('a', navigation).blur();
        return false;
            }
        });

        // Init wizard with validation
        jQuery('.js-wizard-validation-material').bootstrapWizard({
            tabClass: '',
            onTabShow: function(tab, navigation, index) {
                var percent = ((index + 1) / navigation.find('li').length) * 100;

                // Get progress bar
                var progress = navigation.parents('.block').find('[data-wizard="progress"] > .progress-bar');

                // Update progress bar if there is one
                if (progress.length) {
                    progress.css({ width: percent + 1 + '%' });
                }
            },
            onNext: function(tab, navigation, index) {
                if( !formMaterial.valid() ) {
                    validatorMaterial.focusInvalid();
                    return false;
                }
            },
            onTabClick: function(tab, navigation, index) {
                jQuery('a', navigation).blur();
        return false;
            }
        });
    };

    return {
        init: function () {
            // Init Wizard Defaults
            initWizardDefaults();

            // Init Form Simple Wizard
            initWizardSimple();

            // Init Form Validation Wizard
            initWizardValidation();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ BeFormWizard.init(); });
