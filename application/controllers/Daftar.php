<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->database();
        $this->load->model('Pelapor_m');
        $this->load->library('session');
        $this->load->library('user_agent');

        if ($this->session->userdata('pelapor_login') == 1)
        redirect(base_url() . 'dashboard', 'refresh');
    }

    public function index(){

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {

          $this->front('auth/daftar');

        } else {

            $activation_code = md5(rand().time());
            $email = $this->input->post('daftar-email');

            $data = array(
                'nama' => $this->input->post('daftar-nama'),
                'email' => $email,
                'phone' => $this->input->post('daftar-phone'),
                'password' => $this->bcrypt->hash_password($this->input->post('daftar-password',TRUE)),
                'status' => '0',
                'kd_aktivasi' => $activation_code,
            );

            $subject = 'Selamat Datang Di Dimariaja.com!';
            $message = $this->load->view('mail/aktivasi', $data , TRUE);
            $kirim = kirim_email($email, $subject, $message);

            // Send email
            if ($kirim) {
              $this->Pelapor_m->simpan($data);
              $this->berhasil($data);
            } else {
              $this->session->set_flashdata('error_message', 'Gagal Kirim Email');
              redirect(site_url('daftar'));
            }
        }
    }

    public function berhasil($data = array()){
      // var_dump($data);
      $this->front('auth/berhasil', $data);
      // $this->load->view('mail/aktivasi', FALSE);
    }


    public function aktivasi($kode = ''){

	      if ($kode == '') {
	        // jika masuk ke lupa_password/reset tanpa $kode
	        redirect(base_url('daftar'), 'refresh');
	      }

	      $query = $this->Pelapor_m->get_by_activation($kode);
	      // $row = $query->row();
	      // print_r($query->row());die();

	      if ($query->num_rows() > 0) {
	        // jika ditemukan record data pada db berdasarkan kode aktivasi
	        // buat kode aktivasi baru u/ update pada record data sebelumnya
	        $kd_aktivasi_baru = md5(rand().time());
	        // record data yg akan diganti pada db
	        $data = array(
	          'status' => '1',
	          'kd_aktivasi' => $kd_aktivasi_baru,
	        );

	        if ($this->Pelapor_m->update("kd_aktivasi = '$kode'", $data)){
	          // jika update sukses
	          // tampilkan pesan sukses dan redirect ke home
	          $this->session->set_flashdata('success_message', 'Akun anda sudah aktif, silahkan login!');
	          redirect(base_url(), 'refresh');

	        }else {
	          // jika update gagal
	          // tampilkan pesan gagal dan redirect ke halaman sebelumnya
	          $this->session->set_flashdata('error_message', 'Aktivasi akun terjadi kesalahan, mohon ulangi!');
	          redirect($this->agent->referrer(), 'refresh');
	          // print_r($q);die();

	        }

	      }else {
	        // jika tidak ditemukan record data pada db berdasarkan kode aktivasi
	        // tampilkan pesan gagal dan redirect ke lupa_password
	        $this->session->set_flashdata('error_message', 'Kode unik tidak cocok, silahkan periksa kode unik kembali!');
	        redirect(base_url(), 'refresh');

	      }
	  }


    public function verify_email(){
       $activation_code = $this->uri->segment(2);

       $query = $this->Guest_m->cek_activation($activation_code);

      if ($query->num_rows() > 0) {
          $row = $query->row();

          $this->session->set_flashdata('error_message', 'Login Gagal! Cek Password Anda');
          redirect(base_url().'login', 'refresh');
      }else{
          $this->session->set_flashdata('error_message', 'Login Gagal! Cek Password Anda');
          redirect(base_url(), 'refresh');
        }

    }


    private function _rules(){
        $this->form_validation->set_rules('daftar-nama', 'Nama Lengkap', 'trim|required');
        $this->form_validation->set_rules('daftar-email', 'Alamat Email', 'trim|required|is_unique[pelapor.email]');
        $this->form_validation->set_rules('daftar-phone', 'No. Handphone', 'trim|required|is_unique[pelapor.phone]');
        $this->form_validation->set_rules('daftar-password', 'Kata Sandi', 'trim|required');
        $this->form_validation->set_rules('daftar-knf_password', 'Konfirmasi Kata Sandi', 'trim|required|matches[daftar-password]');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }



}
