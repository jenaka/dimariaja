<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kontak extends MY_Controller {

  function __construct() {
    parent::__construct();

    $this->load->database();
    $this->load->library('session');
    $this->load->model('Kontak_m');

  }

    public function index(){

        $data = array(
            'page_title' => 'Kontak',
            'sub_title' => 'Hubungi Kami'
        );
        $this->front('kontak', $data);

    } //end fungsi index

    public function send(){ //Fungsi kirim pesan pada halaman kontak
      // data yang dikirim kedalam database dimasukkan kedalam var $data
      $data = array(
        'nama' => $this->input->post('nama'),
        'email' => $this->input->post('email'),
        'subjek' => $this->input->post('subjek'),
        'pesan' => $this->input->post('pesan'),
      );

      if ($this->Kontak_m->save($data)) {
        // jika pesan berhasil terkirim
        $this->session->set_flashdata('success_message', 'Terima kasih atas pesannya!');
        redirect(base_url('kontak'));
      }else {
        // jika pesan gagal terkirim
        $this->session->set_flashdata('error_message', 'Pesan anda gagal terkirim, silahkan coba lagi!');
        redirect(base_url('kontak'));
      }

    } //end fungsi send

}
