<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Panduan extends MY_Controller {

  function __construct(){
    parent::__construct();
  }

    public function index(){

      $data = array(
          'page_title' => 'Panduan',
          'sub_title' => 'Panduan Laporan Pengaduan',
      );
      $this->front('panduan', $data);
    } // end function index


}
