<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengaduan extends MY_Controller {

    function __construct(){
        parent::__construct();
    }

    public function index($rowno=0){

        $search_text = "";
        if($this->input->post('sort_by') != NULL ){
          $search_text = $this->input->post('sort_by');
          $this->session->set_userdata(array("sort_by"=>$search_text));
        }else{
          if($this->session->userdata('sort_by') != NULL){
            $search_text = $this->session->userdata('sort_by');
          }
        }

        $rowperpage = 20;
        if($rowno != 0){
          $rowno = ($rowno-1) * $rowperpage;
        }
     
        $allcount = $this->Pengaduan_m->total_pengaduan($search_text);

        // Get records
        $pengaduan_data = $this->Pengaduan_m->get_pengaduan($rowno,$rowperpage,$search_text);
     
        // Pagination Configuration
        $config['base_url'] = base_url().'guest/invoices/';
        $config['first_url'] = base_url() . 'guest/invoices/';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;

        $config['full_tag_open'] = '<div class="page-pagination"><nav aria-label="Pagination"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';

        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_link'] = '<span aria-hidden="true"><i class="fa fa-chevron-right"></i></span>';
        $config['next_tag_close'] = '</li>';

        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_link'] = '<span aria-hidden="true"><i class="fa fa-chevron-left"></i></span>';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="page-item active"><a href="" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open']    = '<li class="page-item">';
        $config['num_tag_close']  = '</li>';

        $config['first_link']    = FALSE;
        $config['last_link']  = FALSE;

        $config['attributes'] = array('class' => 'page-link');


        // Initialize
        $this->pagination->initialize($config);
     
        $data['pagination'] = $this->pagination->create_links();
        $data['pengaduan_data'] = $pengaduan_data;
        $data['row'] = $rowno;
        $data['search'] = $search_text;

        $this->pelapor('pengaduan/index', $data);
    }


    public function tambah(){

        $data = array(
            'page_title'   => 'Home',
            'nama' => set_value('nama'),
            'no_passport' => set_value('no_passport'),
            'tmp_lahir' => set_value('tmp_lahir'),
            'tgl_lahir' => set_value('tgl_lahir'),
            'jk' => set_value('jk'),
            'id_kecamatan' => set_value('id_kecamatan'),
            'id_kelurahan' => set_value('id_kelurahan'),
            'alamat_asal' => set_value('alamat_asal'),
            'alamat_kerja' => set_value('alamat_kerja'),
            'embarsi' => set_value('embarsi'),
            'debarsi' => set_value('debarsi'),
            'nm_pptkis' => set_value('nm_pptkis'),
            'negara' => set_value('negara'),
            'mulai_kerja' => set_value('mulai_kerja'),
            'akhir_kerja' => set_value('akhir_kerja'),
            'lama_kerja' => set_value('lama_kerja'),
            'nm_agency' => set_value('nm_agency'),
            'alamat_agency' => set_value('alamat_agency'),
            'f_nama_mjk' => set_value('f_nama_mjk'),
            'l_nama_mjk' => set_value('l_nama_mjk'),
            'jml_mjk' => set_value('jml_mjk'),
            'badan_usaha' => set_value('badan_usaha'),
            'alamat_mjk' => set_value('alamat_mjk'),
            'no_id' => set_value('no_id'),
            'Kecamatan_data' => $this->Wilayah_m->get_wilayah_all(),
            'kelurahan_data' => $this->Wilayah_m->get_kelurahan_all(),
            'kecamatan_selected' => '',
            'kelurahan_selected' => '',
        );
        $this->pelapor('pengaduan/tambah', $data);
    }

    public function simpan(){

        $this->rules();
         if ($this->form_validation->run() == FALSE) {
            $this->tambah();
        } else {
            $id_pengaduan = $this->Pengaduan_m->id_pengaduan();

            $data = array(
                'id_pengaduan' => $id_pengaduan,
                'nama' => $this->input->post('nama',TRUE),
                'no_passport' => $this->input->post('no_passport',TRUE),
                'tmp_lahir' => $this->input->post('tmp_lahir',TRUE),
                'tgl_lahir' => date("Y-m-d", strtotime($this->input->post('tgl_lahir', TRUE))),
                'id_kecamatan' => $this->input->post('id_kecamatan',TRUE),
                'id_kelurahan' => $this->input->post('id_kelurahan',TRUE),
                'alamat_asal' => $this->input->post('alamat_asal',TRUE),
                'alamat_kerja' => $this->input->post('alamat_kerja',TRUE),
                'embarsi' => $this->input->post('embarsi',TRUE),
                'debarsi' => $this->input->post('debarsi',TRUE),
                'nm_pptkis' => $this->input->post('nm_pptkis',TRUE),
                'negara' => $this->input->post('negara',TRUE),
                'mulai_kerja' => date("Y-m-d", strtotime($this->input->post('mulai_kerja', TRUE))),
                'akhir_kerja' => date("Y-m-d", strtotime($this->input->post('akhir_kerja', TRUE))),
                'lama_kerja' => $this->input->post('lama_kerja',TRUE),
                'nm_agency' => $this->input->post('nm_agency',TRUE),
                'alamat_agency' => $this->input->post('alamat_agency',TRUE),
                'f_nama_mjk' => $this->input->post('f_nama_mjk',TRUE),
                'l_nama_mjk' => $this->input->post('l_nama_mjk',TRUE),
                'jml_mjk' => $this->input->post('jml_mjk',TRUE),
                'badan_usaha' => $this->input->post('badan_usaha',TRUE),
                'alamat_mjk' => $this->input->post('alamat_mjk',TRUE),
                'tuntutan' => $this->input->post('tuntutan',TRUE),
                'masalah' => $this->input->post('masalah',TRUE),
                'status_pengaduan' => '0',
                'status_pmi' => '0', 
                'id_pelapor' => $this->session->userdata('pelapor_id')
            );

            $uploadData = array();

            if(!empty($_FILES['lampiran']['name'])){
                $filesCount = count($_FILES['lampiran']['name']);
                for($i = 0; $i < $filesCount; $i++){
                    $_FILES['userFile']['name'] = $_FILES['lampiran']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['lampiran']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['lampiran']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['lampiran']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['lampiran']['size'][$i];

                    $uploadPath = 'uploads/pengaduan/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'jpeg|jpg|png|pdf|pptx|ppt|docx|doc|';
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if($this->upload->do_upload('userFile')){
                        $fileData = $this->upload->data();
                        $uploadData[$i]['path'] = $fileData['file_name'];;
                        $uploadData[$i]['id_pengaduan'] = $id_pengaduan;
                    }
                }
            }

            $this->Pengaduan_m->simpan($data, $uploadData);
            $this->session->set_flashdata('success_message', 'Pengaduan Berhasil Dikirim');
            redirect(site_url('pengaduan'));
        }
    }


    private function rules(){
        $this->form_validation->set_rules('nama', 'Nama Lengkap PMI', 'trim|required');
        $this->form_validation->set_rules('no_passport', 'No. Passport', 'trim|required');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat Lahir', 'trim|required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'trim|required');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'trim|required');
        $this->form_validation->set_rules('id_kecamatan', 'Kecamatan', 'trim|required');
        $this->form_validation->set_rules('id_kelurahan', 'Kelurahan', 'trim|required');
        $this->form_validation->set_rules('alamat_asal', 'Alamat Asal', 'trim|required');
        $this->form_validation->set_rules('alamat_kerja', 'Alamat Kerja', 'trim|required');
        $this->form_validation->set_rules('embarsi', 'Embarkasi', 'trim|required');
        $this->form_validation->set_rules('debarsi', 'Debarkasi', 'trim|required');
        $this->form_validation->set_rules('nm_pptkis', 'Nama PPTKIS', 'trim|required');
        $this->form_validation->set_rules('negara', 'Negara Tujuan', 'trim|required');
        $this->form_validation->set_rules('mulai_kerja', 'Tanggal Mulai Kerja', 'trim|required');
        $this->form_validation->set_rules('akhir_kerja', 'Tanggal Kembali Ke Daerah Asal', 'trim|required');
        $this->form_validation->set_rules('lama_kerja', 'Lama Bekerja', 'trim|required');
        $this->form_validation->set_rules('nm_agency', 'Nama Agency', 'trim|required');
        $this->form_validation->set_rules('alamat_agency', 'Alamat Agency', 'trim|required');


        $this->form_validation->set_rules('f_nama_mjk', 'Nama Awal Majikan', 'trim|required');
        $this->form_validation->set_rules('l_nama_mjk', 'Nama Akhir Majikan', 'trim|required');
        $this->form_validation->set_rules('jml_mjk', 'Jumlah Majikan', 'trim|required');
        // $this->form_validation->set_rules('badan_usaha', 'Badan Usaha', 'trim|required');
        $this->form_validation->set_rules('alamat_mjk', 'Alamat Majikan', 'trim|required');

        $this->form_validation->set_rules('masalah', 'Masalah', 'trim|required');
        $this->form_validation->set_rules('tuntutan', 'Tuntutan', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function detail($id){

        $row = $this->Pengaduan_m->get_by_id($id);

        if ($row) {

            if($row->jk == 'L'){
                $jk = 'Laki-Laki';
            }else{
                $jk = 'Perempuan';
            }

            $data = array(
                'title' => 'Data pengaduan',
                'sub' => 'Detail Data pengaduan',
                'action' => '',
                'id' => $row->id_pengaduan,
                'bukti_pengaduan' => $this->Pengaduan_m->get_bukti_all($id),
                'status' => $row->status_pengaduan,
                'tuntutan' => $row->tuntutan,
                'masalah' => $row->masalah,
                'kec_pmi' => $row->kec_pmi,
                'kel_pmi' => $row->kel_pmi,
                'status_pmi' => $row->status_pmi,
                'nama' => $row->nama,
                'no_passport' => $row->no_passport,
                'tmp_lahir' => $row->tmp_lahir,
                'tgl_lahir' => $row->tgl_lahir,
                'jk' => $jk,
                'alamat_asal' => $row->alamat_asal,
                'alamat_kerja' => $row->alamat_kerja,
                'embarsi' => $row->embarsi,
                'debarsi' => $row->debarsi,
                'nm_pptkis' => $row->nm_pptkis,
                'negara' => $row->negara,
                'mulai_kerja' => $row->mulai_kerja,
                'akhir_kerja' => $row->akhir_kerja,
                'lama_kerja' => $row->lama_kerja,
                'nm_agency' => $row->nm_agency,
                'alamat_agency' => $row->alamat_agency,
                'f_nama_mjk' => $row->f_nama_mjk,
                'l_nama_mjk' => $row->l_nama_mjk,
                'jml_mjk' => $row->jml_mjk,
                'badan_usaha' => $row->badan_usaha,
                'alamat_mjk' => $row->alamat_mjk,
            );

              $this->pelapor('pengaduan/detail', $data);
            
      } else {
          $this->session->set_flashdata('error_message', 'Data pengaduan Tidak Ditemukan');
          redirect(site_url('pengaduan'));
      }
    }

}