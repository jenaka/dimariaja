<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends MY_Controller {

	function __construct(){
	    parent::__construct();
  	}

    public function index(){

    	if($this->session->userdata('pelapor_login') == 1){

    		$data = array(
	            'page_title' => 'Home',
	        );

	        $this->pelapor('beranda', $data);

    	}else{

	        $this->form_validation->set_rules('login-email', 'Alamat Email', 'trim|required');
	        $this->form_validation->set_rules('login-password', 'Kata Sandi', 'trim|required');
	        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

	        if ($this->form_validation->run() == FALSE) {

						$data = array(
	            'page_title' => 'Home',
		        );

		        $this->front('home', $data);

	        } else {

	          $email = strtolower($this->input->post('login-email'));
			      $password = $this->input->post('login-password');

			      $credential = array('email' => $email);

			      $query = $this->Pelapor_m->check_email($email);

			      if ($query->num_rows() > 0) {
							// jika email terdaftar pada db

		          $row = $query->row();

		          if($this->bcrypt->check_password($password, $row->password)){
								// jika password input cocok dengan password pada db
								// set userdata sebagai session ketika berhasil login
	              $this->session->set_userdata('pelapor_login', '1');
	              $this->session->set_userdata('pelapor_id', $row->id_pelapor);
								$this->session->set_userdata('pelapor_nama', $row->nama);
	              $this->session->set_userdata('pelapor_mail', $row->email);
								$this->session->set_userdata('pelapor_phone', $row->phone);
	              $this->session->set_userdata('pelapor_status', $row->status);
	              $this->session->set_userdata('login_type','Pelapor');

	              $this->session->set_flashdata('success_message', 'Login Berhasil');
	              redirect(base_url(), 'refresh');

		          }else{
								// jika password input tidak cocok dengan password pada db, maka refresh page login
		            $this->session->set_flashdata('error_message', 'Login Gagal! Password Salah');
		            redirect(base_url(), 'refresh');
		          }


		      	}else {
							// jika email tidak terdaftar pada db
							$this->session->set_flashdata('error_message', 'Login Gagal! Email Tidak Terdaftar');
							redirect(base_url(), 'refresh');

		      	}
	       }
	    }
    }


    public function logout(){
    	$this->session->sess_destroy();
      $this->session->set_flashdata('logout_notification', 'logged_out');
      redirect(base_url(), 'refresh');
    }


    // public function lupa_pw(){
		//
		// 	$this->form_validation->set_rules('lupa-email', 'Alamat Email', 'trim|required');
    //   $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		//
    // 	if ($this->form_validation->run() == FALSE) {
		//
    //       $this->front('auth/lupa_pw');
		//
    //     } else {
		//
    //     	$data = array('email' => $this->input->post('lupa-email'));
    //     	$query = $this->db->get_where('pelapor', $data);
		//
    //     	if ($query->num_rows() > 0) {
		//
    //     		$row = $query->row();
		//
    //     		$data = array(
    //     			'pelapor_nama' => $row->nama,
    //     			'kd_aktivasi' => $row->kd_aktivasi,
    //     		);
		//
    //     		$email = $row->email;
		// 				$subject = 'Reset password';
    //     		$message = $this->load->view('mail/reset_pw', $data , TRUE);
    //     		$kirim = kirim_email($email, $subject, $message);
		//
    //     		$this->session->set_flashdata('success_message', 'Instruksi untuk melakukan reset sandi sedang dikirimkan. Silakan cek e-mail kamu.');
    //         redirect(base_url(), 'refresh');
		//
    //       }else{
		//
    //         $this->session->set_flashdata('error_message', 'Email Tidak Terdaftar');
    //         redirect(base_url('lupa-kata-sandi'), 'refresh');
		//
    //       }
    //     }
		//
    // 	// $this->load->view('mail/reset_pw');
    // }

}
