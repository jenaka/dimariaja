<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lupa_password extends MY_Controller {

  function __construct(){
    parent::__construct();

    $this->load->library('user_agent');
  }

    public function index(){

			$this->form_validation->set_rules('lupa-email', 'Alamat Email', 'trim|required');
      $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

    	  if ($this->form_validation->run() == FALSE) {

          $this->front('auth/lupa_pw');

        } else {

        	$data = array('email' => $this->input->post('lupa-email'));
        	$query = $this->db->get_where('pelapor', $data);

        	if ($query->num_rows() > 0) {

        		$row = $query->row();

        		$data = array(
        			'pelapor_nama' => $row->nama,
        			'kd_aktivasi' => $row->kd_aktivasi,
        		);

        		$email = $row->email;
						$subject = 'Reset password';
        		$message = $this->load->view('mail/reset_pw', $data , TRUE);
        		$kirim = kirim_email($email, $subject, $message);

        		$this->session->set_flashdata('success_message', 'Instruksi untuk melakukan reset sandi sedang dikirimkan. Silakan cek e-mail kamu.');
            redirect(base_url(), 'refresh');

          }else{

            $this->session->set_flashdata('error_message', 'Email Tidak Terdaftar');
            redirect(base_url('lupa_password'), 'refresh');

          }
        }

    	// $this->load->view('mail/reset_pw');
    }

    public function reset($kode = ''){

      if ($kode == '') {
        // jika masuk ke lupa_password/reset tanpa $kode
        redirect(base_url('lupa_password'), 'refresh');
      }

      // rules untuk dipenuhi dibawah
      $this->form_validation->set_rules('lupa-pw', 'Password baru', 'trim|required');
      $this->form_validation->set_rules('knf-lupa-pw', 'Konfirmasi password Baru', 'trim|required|matches[lupa-pw]');
      $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

      if ($this->form_validation->run() == FALSE) {
        // jika rules di form_validation tidak terpenuhi
        $data = array(
          'kode' => $kode,
        );
        $this->front('auth/reset_pw', $data);

      }else {
        // jika rules di form_validation sudah terpenuhi
        // ambil data pada record db berdasarkan kode aktivasi
        $query = $this->Pelapor_m->get_by_activation($kode);
        // $row = $query->row();

        if ($query->num_rows() > 0) {
          // jika ditemukan record data pada db berdasarkan kode aktivasi
          // buat kode aktivasi baru u/ update pada record data sebelumnya
          $kd_aktivasi_baru = md5(rand().time());
          // record data yg akan diganti pada db
          $data = array(
            'password' => $this->bcrypt->hash_password($this->input->post('lupa-pw',TRUE)),
            'kd_aktivasi' => $kd_aktivasi_baru,
          );

          if ($this->Pelapor_m->update("kd_aktivasi = '$kode'", $data)){
            // jika update sukses
            // tampilkan pesan sukses dan redirect ke home
            $this->session->set_flashdata('success_message', 'Password berhasil diubah, silahkan login!');
            redirect(base_url(), 'refresh');

          }else {
            // jika update gagal
            // tampilkan pesan gagal dan redirect ke halaman sebelumnya
            $this->session->set_flashdata('error_message', 'Password gagal diubah, silahkan ulangi kembali!');
            redirect($this->agent->referrer(), 'refresh');
            // print_r($q);die();

          }

        }else {
          // jika tidak ditemukan record data pada db berdasarkan kode aktivasi
          // tampilkan pesan gagal dan redirect ke lupa_password
          $this->session->set_flashdata('error_message', 'Kode unik salah, silahkan kirim kode unik kembali!');
          redirect(base_url('lupa_password'), 'refresh');

        }
      }
		} //end function reset

}
