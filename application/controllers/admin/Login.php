<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->database();
        $this->load->library('session');
        $this->load->model('User_m');
        $this->load->model('Operator_m');
    }

    public function index() {
      if ($this->session->userdata('users_login') == 1 or $this->session->userdata('operator_login') == 1)
            redirect(base_url() . 'admin/beranda', 'refresh');

        $this->load->view('admin/login');
    }

    public function validate_login() {
      $identity = strtolower($this->input->post('identity'));
      $password = $this->input->post('password');

      $query = $this->User_m->check_email($identity);

      if ($query->num_rows() > 0) {
          $row = $query->row();

          if($this->bcrypt->check_password($password, $row->password)){

            if($row->active == 1){

              $this->session->set_userdata('users_login', '1');
              $this->session->set_userdata('id', $row->id);
              $this->session->set_userdata('login_user_id', $row->id);
              $this->session->set_userdata('nama', $row->nama);
              $this->session->set_userdata('id_akses', $row->id_akses);
              $this->session->set_userdata('login_type', $row->akses);
              $this->session->set_flashdata('success_message', 'Login Berhasil');

              redirect(base_url() . 'admin/beranda', 'refresh');
            }else{
              $this->session->set_flashdata('error_message', 'Login Gagal! Akun Anda Belum Aktif');
              redirect(base_url() . 'admin', 'refresh');
            }
          }else{
        		$this->session->set_flashdata('error_message', 'Login Gagal! Cek Password Anda');
      		  redirect(base_url() . 'admin', 'refresh');
      	  }
      }

      $query = $this->Operator_m->check_email($identity);
      if ($query->num_rows() > 0) {
          $row = $query->row();

          if($this->bcrypt->check_password($password, $row->password)){

            if($row->active == 1){

              $this->session->set_userdata('users_login', '1');
              $this->session->set_userdata('operator_id', $row->id);
              $this->session->set_userdata('login_user_id', $row->id);
              $this->session->set_userdata('nama', $row->nama);
              $this->session->set_userdata('kecamatan', $row->id_kecamatan);
              $this->session->set_userdata('kelurahan', $row->id_kelurahan);
              $this->session->set_userdata('login_type', 'Operator');
              $this->session->set_flashdata('success_message', 'Login Berhasil');

              redirect(base_url() . 'admin/beranda', 'refresh');
            }else{
              $this->session->set_flashdata('error_message', 'Login Gagal! Akun Anda Belum Aktif');
              redirect(base_url() . 'admin/login', 'refresh');
            }
          }else{
            $this->session->set_flashdata('error_message', 'Login Gagal! Cek Password Anda');
            redirect(base_url() . 'admin/login', 'refresh');
          }
      }


      $this->session->set_flashdata('error_message', 'Login Gagal! Email Tidak Terdaftar');
    	redirect(base_url() . 'admin', 'refresh');

    }

    public function logout() {
        $this->session->sess_destroy();
        $this->session->set_flashdata('logout_notification', 'logged_out');
        redirect(base_url().'admin', 'refresh');
    }

}
