<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengaturan extends MY_Controller {

  function __construct(){
    parent::__construct();
  }

    public function index(){

        $row = $this->Pengguna_m->get_by_session();

        if($this->input->post('username') != $row->username) {
            $username_unik =  '|is_unique[users.username]';
        } else {
           $username_unik =  '';
        }

        if($this->input->post('email') != $row->email) {
            $email_unik =  '|is_unique[users.email]';
        } else {
           $email_unik =  '';
        }

        if($this->input->post('phone') != $row->telp) {
            $phone_unik =  '|is_unique[users.telp]';
        } else {
           $phone_unik =  '';
        }

        $this->form_validation->set_rules('nama', 'Nama Lengkap', 'trim|required');
        $this->form_validation->set_rules('email', 'Alamat Email', 'trim|required'.$email_unik);
        $this->form_validation->set_rules('username', 'Username', 'trim|required'.$username_unik);
        $this->form_validation->set_rules('phone', 'No. Handphone', 'trim|required'.$phone_unik);
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == TRUE) {
            $nama  = $this->input->post('nama');
            $email = $this->input->post('email');
            $username = $this->input->post('username');
            $phone = $this->input->post('phone');

            // update nama, email, phone lama pada db dengan yg baru yg sudah di input
            $this->db->set('nama', $nama);
            $this->db->set('username', $username);
            $this->db->set('email', $email);
            $this->db->set('telp', $phone);
            $this->db->where('id', $this->session->userdata('id'));
            $this->db->update('users');

            // perbarui session untuk nama, email, phone dengan data terbaru
            $this->session->set_userdata('nama', $nama);
            $this->session->set_flashdata('success_message', 'Data diri anda berhasil diubah');
            redirect(base_url('admin/pengaturan'), 'refresh');

        }else {
            $row = $this->Pengguna_m->get_by_session();

            $data = array(
                'title' => 'Pengaturan',
                'sub' => 'Pengaturan Pengguna',
                'nama' => $row->nama,
                'email' => $row->email,
                'username' => $row->username,
                'phone' => $row->telp,
            );

            if($this->session->userdata('login_type') == 'Admin'){
                $this->admin('pengaturan', $data);
            }else{
                $this->temp('pengaturan', $data);
            }
        }
    }

    public function ubah_info(){

      $this->form_validation->set_rules('nama', 'Nama Lengkap', 'trim|required');
      $this->form_validation->set_rules('email', 'E-mail', 'trim|required');
      $this->form_validation->set_rules('phone', 'No. handphone', 'trim|required');
      $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

      if ($this->form_validation->run() == FALSE) {

        redirect(base_url('admin/pengaturan#info-umum'));

      }else {

        // assign inputan nama, email, phone lama ke $nama, $email, $phone
        $nama  = $this->input->post('nama');
        $email = $this->input->post('email');
        $telp = $this->input->post('phone');

        // update nama, email, phone lama pada db dengan yg baru yg sudah di input
        $this->db->set('nama', $nama);
        $this->db->set('email', $email);
        $this->db->set('telp', $telp);
        $this->db->where('id_akses', $this->session->userdata('id_akses'));
        $this->db->update('users');

        // perbarui session untuk nama, email, phone dengan data terbaru
        $this->session->set_userdata('nama', $nama);
        // $this->session->set_userdata('mail', $email);
        // $this->session->set_userdata('phone', $telp);
        $this->session->set_flashdata('success_message', 'Data diri anda berhasil diubah');
        redirect(base_url('admin/pengaturan#info-umum'), 'refresh');

      }

    }

    public function ubah_pw(){

        $this->form_validation->set_rules('ubah-lama', 'Kata Sandi Lama', 'trim|required');
        $this->form_validation->set_rules('ubah-baru', 'Kata Sandi Baru', 'trim|required');
        $this->form_validation->set_rules('ubah-knf', 'Konfirmasi Kata Sandi Baru', 'trim|required|matches[ubah-baru]');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == TRUE){
                $row = $this->Pelapor_m->get_by_session();
                if($this->bcrypt->check_password($password, $row->password)){

                    $this->db->set('password', '');
                    $this->db->where('id_pelapor', $this->session->userdata('pelapor_id'));
                    $this->db->update('pelapor');
                    $this->session->set_flashdata('error_message', 'Email Tidak Terdaftar');
                    redirect(base_url(), 'refresh');
              }else{
                    $this->session->set_flashdata('error_message', 'Email Tidak Terdaftar');
                    redirect(base_url(), 'refresh');
              }
        }else{
            if($this->session->userdata('login_type') == 'Admin'){
                $this->admin('pengaturan', $data);
            }else{
                $this->temp('pengaturan', $data);
            }
        }
    }

}
