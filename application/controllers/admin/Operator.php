<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Operator extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Operator_m');
        if ($this->session->userdata('users_login') != 1)
                redirect(base_url() . 'login', 'refresh');
    }

    public function index(){

        $data = array(
        'title' => 'Operator',
        'sub' => 'Kelola Operator',
        'operator_data' => $this->Operator_m->get_all_operator(),
        'id_kecamatan' => set_value('id_kecamatan'),
        'id_kelurahan' => set_value('id_kelurahan'),
        'kecamatan_data' => $this->Wilayah_m->get_wilayah_all(),
        'kelurahan_data' => $this->Wilayah_m->get_kelurahan_all(),
        'kecamatan_selected' => '',
        'kelurahan_selected' => '',
        );


        if($this->session->userdata('login_type') == 'Superadmin' || $this->session->userdata('login_type') == 'Admin'){
          $this->admin('operator/index', $data);
        }else{
          $this->temp('operator/index', $data);
        }
    }

    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('nama') == ''){
            $data['inputerror'][] = 'nama';
            $data['error_string'][] = 'Nama Lengkap Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if($this->input->post('id_kecamatan') == ''){
            $data['inputerror'][] = 'id_kecamatan';
            $data['error_string'][] = 'Kecamatan Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if($this->input->post('id_kelurahan') == ''){
            $data['inputerror'][] = 'id_kelurahan';
            $data['error_string'][] = 'Kelurahan Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if($this->input->post('username') == ''){
            $data['inputerror'][] = 'username';
            $data['error_string'][] = 'Username Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }else{

            $row = $this->db->get_where('operator', array('username' => $this->input->post('username')));
            if ($row->num_rows() > 0) {
                $data['inputerror'][] = 'username';
                $data['error_string'][] = 'Username Sudah Digunakan';
                $data['status'] = FALSE;
            }
        }

        if($this->input->post('email') == ''){
            $data['inputerror'][] = 'email';
            $data['error_string'][] = 'Alamat Email Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }else{

            $row = $this->db->get_where('operator', array('email' => $this->input->post('email')));
            if ($row->num_rows() > 0) {
                $data['inputerror'][] = 'email';
                $data['error_string'][] = 'Alamat Email Sudah Digunakan';
                $data['status'] = FALSE;
            }
        }

        if($this->input->post('password') == ''){
            $data['inputerror'][] = 'password';
            $data['error_string'][] = 'Kata Sandi Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if($this->input->post('re_password') == ''){
            $data['inputerror'][] = 're_password';
            $data['error_string'][] = 'Konfirmasi Kata Sandi Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }else if($this->input->post('re_password') <> $this->input->post('password')){
            $data['inputerror'][] = 're_password';
            $data['error_string'][] = 'Konfirmasi Kata Sandi Tidak Sama Dengan Kata Sandi';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

    public function simpan(){

       $this->_validate();

        $data = array(
          'nama' => $this->input->post('nama',TRUE),
          'email' => $this->input->post('email',TRUE),
          'username' => $this->input->post('username',TRUE),
          'telp' => $this->input->post('telp',TRUE),
          'id_kecamatan' => $this->input->post('id_kecamatan',TRUE),
          'id_kelurahan' => $this->input->post('id_kelurahan',TRUE),
          'password' => $this->bcrypt->hash_password($this->input->post('password',TRUE)),
          'active' => '1',
        );

        $insert = $this->Pengguna_m->tambah($data);
        echo json_encode(array("status" => TRUE));
    }

    public function edit($id){
        $data = $this->Operator_m->get_by_id($id);
        echo json_encode($data);
    }

    public function update(){
        $data = array(
          'nama' => $this->input->post('nama',TRUE),
          'email' => $this->input->post('email',TRUE),
          'username' => $this->input->post('username',TRUE),
          'telp' => $this->input->post('telp',TRUE),
          'id_kecamatan' => $this->input->post('id_kecamatan',TRUE),
          'id_kelurahan' => $this->input->post('id_kelurahan',TRUE),
          'password' => $this->bcrypt->hash_password($this->input->post('password',TRUE)),
          'active' => '1',
        );

        $this->Operator_m->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function trash(){

        $data = array(
            'title' => 'Operator',
            'sub' => 'Operator Dihapus',
            'operator_data' => $this->Operator_m->get_trash(),
        );

        $this->admin('operator/trash', $data);
    }

    public function aktif($id){
        $data = array('active' => '1');
        $this->Operator_m->aktivasi($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function nonaktif($id){
        $data = array('active' => '0');
        $this->Operator_m->aktivasi($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function hapus($id){
        $data = array('dihapus' => '1');
        $this->Operator_m->hapus($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function restore($id){
        $data = array('dihapus' => '0');
        $this->Operator_m->hapus($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function permanen($id){
        $this->Operator_m->permanen($id);
        echo json_encode(array("status" => TRUE));
    }

}
