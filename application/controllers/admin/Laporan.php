<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Laporan extends MY_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Pengaduan_m');
    if ($this->session->userdata('users_login') != 1)
            redirect(base_url() . 'login', 'refresh');
  }

  

  public function pengaduan(){
        $this->form_validation->set_rules('tgl_mulai', 'Tanggal Pertama', 'trim|required');
        $this->form_validation->set_rules('tgl_akhir', 'Tanggal Kedua', 'trim|required');

        if ($this->form_validation->run() === FALSE){

            $data = array(
                'title' => 'Laporan',
                'sub' => 'Laporan Pengaduan',
                'action' => site_url('laporan/pengaduan'),
                'tgl_mulai' => set_value('tgl_mulai', TRUE),
                'tgl_akhir' => set_value('tgl_akhir', TRUE),
                'pengaduan_data' => '',
            );

            $this->temp('laporan/pengaduan', $data);
        }else{

            $tgl_mulai = date("Y-m-d", strtotime($this->input->post('tgl_mulai', TRUE)));
            $tgl_akhir = date("Y-m-d", strtotime($this->input->post('tgl_akhir', TRUE)));

            if($this->session->userdata('login_type') == 'Operator'){

                $id_kecamatan = $this->session->userdata('kecamatan');
                $id_kelurahan = $this->session->userdata('kelurahan');

                $pengaduan = $this->Pengaduan_m->get_laporan_operator($tgl_mulai, $tgl_akhir, $id_kecamatan, $id_kelurahan);

            }else if($this->session->userdata('login_type') == 'BNP2TKI' || $this->session->userdata('login_type') == 'BNP3TKI' || $this->session->userdata('login_type') == 'KEMENLU' || $this->session->userdata('login_type') == 'KEMNAKER'){
                
                $pengaduan = $this->Pengaduan_m->get_laporan_terusan($tgl_mulai, $tgl_akhir);

            }else{
                $pengaduan = $this->Pengaduan_m->get_laporan($tgl_mulai, $tgl_akhir);
            }


             $data = array(
                'title' => 'Laporan',
                'sub' => 'Laporan Pengaduan ',
                'action' => '',
                'tgl_mulai' => $this->input->post('tgl_mulai', TRUE),
                'tgl_akhir' => $this->input->post('tgl_akhir', TRUE),
                'pengaduan_data' => $pengaduan,
            );

              $this->temp('laporan/pengaduan', $data);
        }
  }

  public function pengaduan_unduh(){

        $tgl_mulai = date("Y-m-d", strtotime($this->input->get('tgl_mulai')));
        $tgl_akhir = date("Y-m-d", strtotime($this->input->get('tgl_akhir')));

        if($this->session->userdata('login_type') == 'Operator'){

                $id_kecamatan = $this->session->userdata('kecamatan');
                $id_kelurahan = $this->session->userdata('kelurahan');

                $pengaduan = $this->Pengaduan_m->get_laporan_operator($tgl_mulai, $tgl_akhir, $id_kecamatan, $id_kelurahan);

            }else if($this->session->userdata('login_type') == 'BNP2TKI' || $this->session->userdata('login_type') == 'BNP3TKI' || $this->session->userdata('login_type') == 'KEMENLU' || $this->session->userdata('login_type') == 'KEMNAKER'){
                
                $pengaduan = $this->Pengaduan_m->get_laporan_terusan($tgl_mulai, $tgl_akhir);

            }else{
                $pengaduan = $this->Pengaduan_m->get_laporan($tgl_mulai, $tgl_akhir);
            }

            // Create new Spreadsheet object
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            // Set document properties
            $spreadsheet->getProperties()->setCreator('SIDUKA-PMI KBB')
            ->setLastModifiedBy('SIDUKA-PMI KBB')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Laporan Pengaduan');

            $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('D1', 'LAPORAN PENGADUAN KASUS')
            ->setCellValue('D3', 'TANGGAL '.$this->input->get('tgl_mulai').' s/d '.$this->input->get('tgl_akhir'))
            ;

            $styleJudul = [
                    'font' => [
                        'bold' => true,
                        'size' => 18,
                    ],
                    'alignment' => [
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'wrapText' => true,
                    ],
                ];

                $styleSub = [
                    'font' => [
                        'bold' => true,
                        'size' => 14,
                    ],
                    'alignment' => [
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'wrapText' => true,
                    ],
                ];

            $spreadsheet->getActiveSheet()->getStyle('D1:G2')->applyFromArray($styleJudul);
            $spreadsheet->getActiveSheet()->getStyle('D3:G3')->applyFromArray($styleSub);
            $spreadsheet->getActiveSheet()->mergeCells('D1:G2');
            $spreadsheet->getActiveSheet()->mergeCells('D3:G3');
            // Add some data
            $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A5', 'NO')
            ->setCellValue('B5', 'NAMA PPTKIS')
            ->setCellValue('C5', 'DASAR SURAT DARI/')
            ->setCellValue('C6', 'NOMOR & TANGGAL SURAT/')
            ->setCellValue('C7', 'NMR & TGL SURAT MASUK')
            ->setCellValue('D5', 'NAMA TKI &')
            ->setCellValue('D6', 'DAERAH ASAL')
            ->setCellValue('D7', 'NEGARA TUJUAN')            
            ->setCellValue('E5', 'PERMASALAHAN TKI')
            ->setCellValue('E6', 'PRA / MASA PENEMPATAN / PURNA')
            ->setCellValue('F5', 'TINDAK LANJUT DI BP3TKI BANDUNG')
            ->setCellValue('F6', '1')
            ->setCellValue('G6', '2')
            ->setCellValue('H5', 'MONITORING STATUS PENGADUAN')
            ->setCellValue('H6', '1')
            ->setCellValue('I6', '2')
            ->setCellValue('J5', 'KET')
            ;

            $styleArray = [
                    'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'wrapText' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ];

            $spreadsheet->getActiveSheet()->getStyle('A5:J7')->applyFromArray($styleArray);

            $spreadsheet->getActiveSheet()->mergeCells('A5:A7');
            $spreadsheet->getActiveSheet()->mergeCells('B5:B7');
            $spreadsheet->getActiveSheet()->mergeCells('E6:E7');
            $spreadsheet->getActiveSheet()->mergeCells('F5:G5');
            $spreadsheet->getActiveSheet()->mergeCells('F6:F7');
            $spreadsheet->getActiveSheet()->mergeCells('G6:G7');
            $spreadsheet->getActiveSheet()->mergeCells('H5:I5');
            $spreadsheet->getActiveSheet()->mergeCells('H6:H7');
            $spreadsheet->getActiveSheet()->mergeCells('I6:I7');
            $spreadsheet->getActiveSheet()->mergeCells('J5:J7');

            // Miscellaneous glyphs, UTF-8
            $i=8;
            $no=1;
            foreach($pengaduan as $data) {

                $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $no)
                ->setCellValue('B'.$i, $data->nm_pptkis)
                ->setCellValue('C'.$i, "")
                ->setCellValue('D'.$i, $data->nama."\nKab. Bandung Barat\n".$data->negara)
                ->setCellValue('E'.$i, $data->masalah)
                ->setCellValue('F'.$i, "")
                ->setCellValue('G'.$i, "")
                ->setCellValue('H'.$i, "")
                ->setCellValue('I'.$i, "")
                ->setCellValue('J'.$i, "");
                
                $styleisi = [
                    'alignment' => [
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP,
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                        'wrapText' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ];

                $spreadsheet->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleisi);
                $spreadsheet->getActiveSheet()->getStyle('B'.$i)->applyFromArray($styleisi);
                $spreadsheet->getActiveSheet()->getStyle('C'.$i)->applyFromArray($styleisi);
                $spreadsheet->getActiveSheet()->getStyle('D'.$i)->applyFromArray($styleisi);
                $spreadsheet->getActiveSheet()->getStyle('E'.$i)->applyFromArray($styleisi);
                $spreadsheet->getActiveSheet()->getStyle('F'.$i)->applyFromArray($styleisi);
                $spreadsheet->getActiveSheet()->getStyle('G'.$i)->applyFromArray($styleisi);
                $spreadsheet->getActiveSheet()->getStyle('H'.$i)->applyFromArray($styleisi);
                $spreadsheet->getActiveSheet()->getStyle('I'.$i)->applyFromArray($styleisi);
                $spreadsheet->getActiveSheet()->getStyle('J'.$i)->applyFromArray($styleisi);

                $i++;
                $no++;
            }
            $sheet->getRowDimension(5)->setRowHeight(35);

            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(26);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(28);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(21);
            $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(50);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(12);
            $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(12);
            $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(15);

            $spreadsheet->getActiveSheet()->setTitle('Laporan Pengaduan');

            $spreadsheet->setActiveSheetIndex(0);

            // Redirect output to a client’s web browser (Xlsx)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="Laporan Pengaduan '.$this->input->get('tgl_mulai').' s/d '.$this->input->get('tgl_akhir').'.xlsx"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');
            exit;
  }

  public function pmi(){
    $this->form_validation->set_rules('tgl_mulai', 'Tanggal Pertama', 'trim|required');
    $this->form_validation->set_rules('tgl_akhir', 'Tanggal Kedua', 'trim|required');

    if ($this->form_validation->run() === FALSE){

        $data = array(
            'title' => 'Laporan',
            'sub' => 'Laporan PMI',
            'action' => site_url('laporan/PMI'),
            'tgl_mulai' => set_value('tgl_mulai', TRUE),
            'tgl_akhir' => set_value('tgl_akhir', TRUE),
            'pengaduan_data' => '',
        );

        $this->temp('laporan/pengaduan', $data);
    }else{

        $tgl_mulai = date("Y-m-d", strtotime($this->input->post('tgl_mulai', TRUE)));
        $tgl_akhir = date("Y-m-d", strtotime($this->input->post('tgl_akhir', TRUE)));

        if($this->session->userdata('login_type') == 'Operator'){

            $id_kecamatan = $this->session->userdata('kecamatan');
            $id_kelurahan = $this->session->userdata('kelurahan');

            $pengaduan = $this->Pengaduan_m->get_laporan_operator($tgl_mulai, $tgl_akhir, $id_kecamatan, $id_kelurahan);

        }else if($this->session->userdata('login_type') == 'BNP2TKI' || $this->session->userdata('login_type') == 'BNP3TKI' || $this->session->userdata('login_type') == 'KEMENLU' || $this->session->userdata('login_type') == 'KEMNAKER'){
            
            $pengaduan = $this->Pengaduan_m->get_laporan_terusan($tgl_mulai, $tgl_akhir);

        }else{
            $pengaduan = $this->Pengaduan_m->get_laporan($tgl_mulai, $tgl_akhir);
        }


         $data = array(
            'title' => 'Laporan',
            'sub' => 'Laporan Pengaduan ',
            'action' => '',
            'tgl_mulai' => $this->input->post('tgl_mulai', TRUE),
            'tgl_akhir' => $this->input->post('tgl_akhir', TRUE),
            'pengaduan_data' => $pengaduan,
        );

          $this->temp('laporan/pengaduan', $data);
    }
  }


}