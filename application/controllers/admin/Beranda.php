<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Beranda extends MY_Controller {

  function __construct(){
    parent::__construct();
    }

    public function index() {

        if($this->session->userdata('login_type') == 'Operator'){

          $id_kecamatan = $this->session->userdata('kecamatan');
          $id_kelurahan = $this->session->userdata('kelurahan');

          $total_kasus = $this->Beranda_m->count_kasus_operator($id_kecamatan, $id_kelurahan);
          $kasus_proses = $this->Beranda_m->kasus_proses_operator($id_kecamatan, $id_kelurahan);

          $get_chart = $this->Beranda_m->get_Chart_operator();

        }else if($this->session->userdata('login_type') == 'BNP2TKI' || $this->session->userdata('login_type') == 'KEMENLU' || $this->session->userdata('login_type') == 'KEMNAKER' || $this->session->userdata('login_type') == 'DISNAKERTRANS-JABAR'){

          $id_akses = $this->session->userdata('id_akses');

          $total_kasus = $this->Beranda_m->count_kasus_terusan($id_akses);
          $kasus_proses = $this->Beranda_m->kasus_proses_terusan($id_akses);

          $get_chart = $this->Beranda_m->get_chart_atasan();

        }else{

          $total_kasus = $this->Beranda_m->count_kasus_all();
          $kasus_proses = $this->Beranda_m->kasus_proses_all();

          $get_chart = $this->Beranda_m->get_chart_admin();

        }

        $data = array(
            'title' => 'Beranda',
            'sub' => 'Selamat Datang '.$this->session->userdata('nama'),
            'total_kasus' => $total_kasus,
            'kasus_proses' => $kasus_proses,
        );

         foreach($get_chart->result_array() as $row){
           $data['total'][]=(float)$row['Januari'];
           $data['total'][]=(float)$row['Februari'];
           $data['total'][]=(float)$row['Maret'];
           $data['total'][]=(float)$row['April'];
           $data['total'][]=(float)$row['Mei'];
           $data['total'][]=(float)$row['Juni'];
           $data['total'][]=(float)$row['Juli'];
           $data['total'][]=(float)$row['Agustus'];
           $data['total'][]=(float)$row['September'];
           $data['total'][]=(float)$row['Oktober'];
           $data['total'][]=(float)$row['November'];
           $data['total'][]=(float)$row['Desember'];
          }

        if($this->session->userdata('login_type') == 'Superadmin' || $this->session->userdata('login_type') == 'Admin'){

          $data['total_pelapor'] = $this->Beranda_m->count_pelapor_all();
          $data['total_pesan'] = $this->Beranda_m->count_kontak_pesan();

          $this->admin('beranda_admin', $data);

        }else{

          $this->temp('beranda', $data);

        }
    }

}
