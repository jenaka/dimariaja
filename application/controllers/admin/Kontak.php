<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kontak extends MY_Controller {

  function __construct(){
    parent::__construct();
        if ($this->session->userdata('users_login') != 1)
        redirect(base_url() . 'admin', 'refresh');
    }

    public function index() {

         $data = array(
            'title' => 'Kontak Pesan',
            'sub'   => 'Kelola Kontak Pesan',
            'contact_data' => $this->Kontak_m->get_all(),
        );

        if($this->session->userdata('login_type') == 'Superadmin' || $this->session->userdata('login_type') == 'Admin'){
          $this->admin('kontak', $data);
        }else{
          $this->temp('kontak', $data);
        }
    }

    public function delete($id){

      die('oke');

    }

    public function detail($id){
        $data = $this->Kontak_m->get_by_id($id);
        echo json_encode($data);
    }


    public function get_inbox(){

        $this->db->where('is_read', '0');
        $this->db->from('kontak');
        $total = $this->db->count_all_results();

        $this->db->where('is_read', '0');
        $this->db->from('kontak');
        $inbox = $this->db->get()->result();

        $output = '';

        if($total > 0 ){
            foreach($inbox as $data){
                $output .= '<li style="padding-left: 3px">
                            <a href="'. base_url('admin/kontak') .'" class="px-0" onclick="return true; detail('. $data->id .');">
                                <div  class="font-w600">
                                    '. $data->nama .' | '. $data->email .'
                                </div>
                                <div class="font-size-m text-muted">'. $data->subjek .'</div>
                            </a>
                        </li>';
            }
        }else{
            $output .= '<li style="padding-left: 3px">
                           <div  class="font-w600">
                                Tidak Ada Kontak Pesan Baru
                            </div>
                        </li>';
        }

        $data = array(
            'notification' => $output,
            'total'  => $total
        );

        echo json_encode($data);
    }


}
