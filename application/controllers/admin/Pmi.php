<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


require_once('./vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Pmi extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pmi_m');
        $this->load->model('Wilayah_m');
        $this->load->library('form_validation');
    }

    public function index(){

    	if($this->session->userdata('login_type') == 'Operator'){

	        $id_kecamatan = $this->session->userdata('kecamatan');
	        $id_kelurahan = $this->session->userdata('kelurahan');

	        $pmi = $this->Pmi_m->get_pmi_op($id_kecamatan, $id_kelurahan);

	    }else{
	        $pmi = $this->Pmi_m->get_all();
	    }

        $data = array(
        	'title' => 'PMI',
            'sub' => 'Data PMI',
            'pmi_data' => $pmi,
        );
        $this->temp('pmi/index', $data);
    }

    public function detail($id){
        $row = $this->Pmi_m->get_by_id($id);
        if ($row) {

        	if($row->jk == 'L'){
        		$jk = 'Laki-laki';
        	}else{
        		$jk = 'Perempuan';
        	}

            $data = array(
            	'title' => 'PMI',
            	'sub' => 'Detail Data PMI',
				'id' => $row->id,
				'nama' => $row->nama,
				'no_passport' => $row->no_passport,
				'umur' => $row->umur,
				'jk' => $jk,
				'id_kecamatan' => $row->id_kecamatan,
				'id_kelurahan' => $row->id_kelurahan,
				'alamat_asal' => $row->alamat_asal,
				'alamat_kerja' => $row->alamat_kerja,
				'embarsi' => $row->embarsi,
				'debarsi' => $row->debarsi,
				'nm_pptkis' => $row->nm_pptkis,
				'negara' => $row->negara,
				'mulai_kerja' => $row->mulai_kerja,
				'akhir_kerja' => $row->akhir_kerja,
				'lama_kerja' => $row->lama_kerja,
				'nm_agency' => $row->nm_agency,
				'alamat_agency' => $row->alamat_agency,
				'f_nama_mjk' => $row->f_nama_mjk,
				'l_nama_mjk' => $row->l_nama_mjk,
				'jml_mjk' => $row->jml_mjk,
				'badan_usaha' => $row->badan_usaha,
				'alamat_mjk' => $row->alamat_mjk,
				'no_id' => $row->no_id,
				'nama_kecamatan' => $row->nama_kecamatan,
				'nama_kelurahan' => $row->nama_kelurahan,
				'lampiran_data' => $this->Pmi_m->get_lampiran($row->id),
			);
            $this->temp('pmi/detail', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pmi'));
        }
    }

    public function tambah(){


        $data = array(
            'title' => 'Data PMI',
            'sub' => 'Tambah Data PMI',
            'action' => site_url('pmi/simpan'),
		    'id' => $this->Pmi_m->id_pmi(),
		    'nama' => set_value('nama'),
		    'no_passport' => set_value('no_passport'),
		    'umur' => set_value('umur'),
		    'jk' => set_value('jk'),
		    'id_kecamatan' => set_value('id_kecamatan'),
		    'id_kelurahan' => set_value('id_kelurahan'),
		    'alamat_asal' => set_value('alamat_asal'),
		    'alamat_kerja' => set_value('alamat_kerja'),
		    'embarsi' => set_value('embarsi'),
		    'debarsi' => set_value('debarsi'),
		    'nm_pptkis' => set_value('nm_pptkis'),
		    'negara' => set_value('negara'),
		    'mulai_kerja' => set_value('mulai_kerja'),
		    'akhir_kerja' => set_value('akhir_kerja'),
		    'lama_kerja' => set_value('lama_kerja'),
		    'nm_agency' => set_value('nm_agency'),
		    'alamat_agency' => set_value('alamat_agency'),
		    'f_nama_mjk' => set_value('f_nama_mjk'),
		    'l_nama_mjk' => set_value('l_nama_mjk'),
		    'jml_mjk' => set_value('jml_mjk'),
		    'badan_usaha' => set_value('badan_usaha'),
		    'alamat_mjk' => set_value('alamat_mjk'),
		    'no_id' => set_value('no_id'),
		    'Kecamatan_data' => $this->Wilayah_m->get_wilayah_all(),
        	'kelurahan_data' => $this->Wilayah_m->get_kelurahan_all(),
        	'kecamatan_selected' => '',
            'kelurahan_selected' => '',
		);

        $this->temp('pmi/form', $data);
    }
    
    public function simpan(){
        $this->form_validation->set_rules('nama', 'Nama TKI', 'trim|required');
		$this->form_validation->set_rules('no_passport', 'No. Passport', 'trim|required');
		$this->form_validation->set_rules('umur', 'Umur', 'trim|required');
		if($this->session->userdata('login_type') <> 'Operator'){
            $this->form_validation->set_rules('id_kecamatan', 'Kecamatan', 'trim|required');
            $this->form_validation->set_rules('id_kelurahan', 'Kelurahan', 'trim|required');

            $kec = $this->input->post('id_kecamatan',TRUE);
            $kel = $this->input->post('id_kelurahan',TRUE);
        }else{
            $kec = $this->session->userdata('kecamatan');
            $kel = $this->session->userdata('kelurahan');
        }
		$this->form_validation->set_rules('alamat_asal', 'Alamat Asal', 'trim|required');
		$this->form_validation->set_rules('alamat_kerja', 'Alamat Kerja', 'trim|required');
		$this->form_validation->set_rules('embarsi', 'Embarsi', 'trim|required');
		$this->form_validation->set_rules('nm_pptkis', 'Nama PPTKIS', 'trim|required');
		$this->form_validation->set_rules('negara', 'Negara Tujuan', 'trim|required');
		$this->form_validation->set_rules('mulai_kerja', 'Tanggal Mulai Kerja', 'trim|required');


        if ($this->form_validation->run() == FALSE) {
            $this->tambah();
        } else {
            $data = array(
            	'id' => $this->input->post('id',TRUE),
				'nama' => $this->input->post('nama',TRUE),
				'no_passport' => $this->input->post('no_passport',TRUE),
				'umur' => $this->input->post('umur',TRUE),
				'id_kecamatan' => $kec,
				'id_kelurahan' => $kel,
				'alamat_asal' => $this->input->post('alamat_asal',TRUE),
				'alamat_kerja' => $this->input->post('alamat_kerja',TRUE),
				'embarsi' => $this->input->post('embarsi',TRUE),
				'debarsi' => $this->input->post('debarsi',TRUE),
				'nm_pptkis' => $this->input->post('nm_pptkis',TRUE),
				'negara' => $this->input->post('negara',TRUE),
				'mulai_kerja' => date("Y-m-d", strtotime($this->input->post('mulai_kerja', TRUE))),
				'akhir_kerja' => date("Y-m-d", strtotime($this->input->post('akhir_kerja', TRUE))),
				'lama_kerja' => $this->input->post('lama_kerja',TRUE),
				'nm_agency' => $this->input->post('nm_agency',TRUE),
				'alamat_agency' => $this->input->post('alamat_agency',TRUE),
				'f_nama_mjk' => $this->input->post('f_nama_mjk',TRUE),
				'l_nama_mjk' => $this->input->post('l_nama_mjk',TRUE),
				'jml_mjk' => $this->input->post('jml_mjk',TRUE),
				'badan_usaha' => $this->input->post('badan_usaha',TRUE),
				'alamat_mjk' => $this->input->post('alamat_mjk',TRUE),
				'no_id' => $this->input->post('no_id',TRUE),
	    	);

            $uploadData = array();

            if(!empty($_FILES['lampiran']['name'])){
                $filesCount = count($_FILES['lampiran']['name']);
                for($i = 0; $i < $filesCount; $i++){
                    $_FILES['userFile']['name'] = $_FILES['lampiran']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['lampiran']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['lampiran']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['lampiran']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['lampiran']['size'][$i];

                    $uploadPath = 'uploads/pmi/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'jpeg|jpg|png|pdf|pptx|ppt|docx|doc|';
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if($this->upload->do_upload('userFile')){
                        $fileData = $this->upload->data();
                        $uploadData[$i]['path'] = $fileData['file_name'];;
                        $uploadData[$i]['id_pmi'] = $this->input->post('id',TRUE);
                    }
                }
            }

            $this->Pmi_m->insert($data, $uploadData);
           	$this->session->set_flashdata('success_message', 'Data PMI Berhasil Ditambahkan');
            redirect(site_url('pmi'));
        }
    }
    
    public function edit($id){
        $row = $this->Pmi_m->get_by_id($id);

        if ($row) {
            $data = array(
                'title' => 'PMI',
                'sub' => 'Edit Data PMI',
                'action' => site_url('pmi/update'),
				'id' => set_value('id', $row->id),
				'nama' => set_value('nama', $row->nama),
				'no_passport' => set_value('no_passport', $row->no_passport),
				'umur' => set_value('umur', $row->umur),
				'jk' => set_value('jk', $row->jk),
				'id_kecamatan' => set_value('id_kecamatan', $row->id_kecamatan),
				'id_kelurahan' => set_value('id_kelurahan', $row->id_kelurahan),
				'alamat_asal' => set_value('alamat_asal', $row->alamat_asal),
				'alamat_kerja' => set_value('alamat_kerja', $row->alamat_kerja),
				'embarsi' => set_value('embarsi', $row->embarsi),
				'debarsi' => set_value('debarsi', $row->debarsi),
				'nm_pptkis' => set_value('nm_pptkis', $row->nm_pptkis),
				'negara' => set_value('negara', $row->negara),
				'mulai_kerja' => set_value('mulai_kerja', $row->mulai_kerja),
				'akhir_kerja' => set_value('akhir_kerja', $row->akhir_kerja),
				'lama_kerja' => set_value('lama_kerja', $row->lama_kerja),
				'nm_agency' => set_value('nm_agency', $row->nm_agency),
				'alamat_agency' => set_value('alamat_agency', $row->alamat_agency),
				'f_nama_mjk' => set_value('f_nama_mjk', $row->f_nama_mjk),
				'l_nama_mjk' => set_value('l_nama_mjk', $row->l_nama_mjk),
				'jml_mjk' => set_value('jml_mjk', $row->jml_mjk),
				'badan_usaha' => set_value('badan_usaha', $row->badan_usaha),
				'alamat_mjk' => set_value('alamat_mjk', $row->alamat_mjk),
				'no_id' => set_value('no_id', $row->no_id),
				'Kecamatan_data' => $this->Wilayah_m->get_wilayah_all(),
	        	'kelurahan_data' => $this->Wilayah_m->get_kelurahan_all(),
	        	'kecamatan_selected' => '',
	            'kelurahan_selected' => '',
				'lampiran_data' => $this->Pmi_m->get_lampiran($row->id),
	    	);

            $this->temp('pmi/edit', $data);
        } else {
           $this->session->set_flashdata('error_message', 'Data PMI Tidak Ditemukan');
            redirect(site_url('pmi'));
        }
    }
    
    public function update(){
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
				'nama' => $this->input->post('nama',TRUE),
				'no_passport' => $this->input->post('no_passport',TRUE),
				'umur' => $this->input->post('umur',TRUE),
				'id_kecamatan' => $this->input->post('id_kecamatan',TRUE),
				'id_kelurahan' => $this->input->post('id_kelurahan',TRUE),
				'alamat_asal' => $this->input->post('alamat_asal',TRUE),
				'alamat_kerja' => $this->input->post('alamat_kerja',TRUE),
				'embarsi' => $this->input->post('embarsi',TRUE),
				'debarsi' => $this->input->post('debarsi',TRUE),
				'nm_pptkis' => $this->input->post('nm_pptkis',TRUE),
				'negara' => $this->input->post('negara',TRUE),
				'mulai_kerja' => $this->input->post('mulai_kerja',TRUE),
				'akhir_kerja' => $this->input->post('akhir_kerja',TRUE),
				'lama_kerja' => $this->input->post('lama_kerja',TRUE),
				'nm_agency' => $this->input->post('nm_agency',TRUE),
				'alamat_agency' => $this->input->post('alamat_agency',TRUE),
				'f_nama_mjk' => $this->input->post('f_nama_mjk',TRUE),
				'l_nama_mjk' => $this->input->post('l_nama_mjk',TRUE),
				'jml_mjk' => $this->input->post('jml_mjk',TRUE),
				'badan_usaha' => $this->input->post('badan_usaha',TRUE),
				'alamat_mjk' => $this->input->post('alamat_mjk',TRUE),
				'no_id' => $this->input->post('no_id',TRUE),
	    	);

	    	$uploadData = array();

            if(!empty($_FILES['lampiran']['name'])){
                $filesCount = count($_FILES['lampiran']['name']);
                for($i = 0; $i < $filesCount; $i++){
                    $_FILES['userFile']['name'] = $_FILES['lampiran']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['lampiran']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['lampiran']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['lampiran']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['lampiran']['size'][$i];

                    $uploadPath = 'uploads/pmi/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'jpeg|jpg|png|pdf|pptx|ppt|docx|doc|';
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if($this->upload->do_upload('userFile')){
                        $fileData = $this->upload->data();
                        $uploadData[$i]['path'] = $fileData['file_name'];;
                        $uploadData[$i]['id_pmi'] = $this->input->post('id',TRUE);
                    }
                }
            }

            $this->Pmi_m->update($this->input->post('id', TRUE), $data, $uploadData);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('pmi'));
        }
    }
    
    public function fill_form($id){
    	$data =  $this->Pmi_m->get_by_id($id);

    	echo json_encode($data);
    }

    public function _rules(){
		$this->form_validation->set_rules('nama', 'Nama TKI', 'trim|required');
		$this->form_validation->set_rules('no_passport', 'No. Passport', 'trim|required');
		$this->form_validation->set_rules('umur', 'Umur', 'trim|required');
		$this->form_validation->set_rules('id_kecamatan', 'Kecamatan', 'trim|required');
		$this->form_validation->set_rules('id_kelurahan', 'Kelurahan', 'trim|required');
		$this->form_validation->set_rules('alamat_asal', 'Alamat Asal', 'trim|required');
		$this->form_validation->set_rules('alamat_kerja', 'Alamat Kerja', 'trim|required');
		$this->form_validation->set_rules('embarsi', 'Embarsi', 'trim|required');
		$this->form_validation->set_rules('nm_pptkis', 'Nama PPTKIS', 'trim|required');
		$this->form_validation->set_rules('negara', 'Negara Tujuan', 'trim|required');
		$this->form_validation->set_rules('mulai_kerja', 'Tanggal Mulai Kerja', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }


    public function hapus($id){
        $data = array('dihapus' => '1');
        $this->Pmi_m->hapus($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function trash(){

        if($this->session->userdata('login_type') == 'Operator'){

            $id_kecamatan = $this->session->userdata('kecamatan');
            $id_kelurahan = $this->session->userdata('kelurahan');

            $pmi = $this->Pmi_m->get_trash_op($id_kecamatan, $id_kelurahan);

        }else{
            $pmi = $this->Pmi_m->get_trash();
        }

        $data = array(
            'title' => 'PMI',
            'sub' => 'Data PMI Dihapus',
            'pmi_data' => $pmi,
        );

        $this->temp('pmi/trash', $data);
    }

    public function restore($id){
        $data = array('dihapus' => '0');
        $this->Pmi_m->hapus($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function permanen($id){
        $this->Pmi_m->permanen($id);
        echo json_encode(array("status" => TRUE));
    }

    public function hapus_lampiran($id){
        $row = $this->Pmi_m->get_file_id($id);

        if(file_exists('uploads/pmi/'.$row->path) && $row->path)
            unlink('uploads/pmi/'.$row->path);

        $this->Pengaduan_m->hapus_file($id);
        echo json_encode(array("status" => TRUE));
    }

    public function download(){

        if($this->session->userdata('login_type') == 'Operator'){

            $id_kecamatan = $this->session->userdata('kecamatan');
            $id_kelurahan = $this->session->userdata('kelurahan');

            $pmi = $this->Pmi_m->get_pmi_op($id_kecamatan, $id_kelurahan);

        }else{
            $pmi = $this->Pmi_m->get_all();
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('SIDUKA-PMI KBB')
        ->setLastModifiedBy('SIDUKA-PMI KBB')
        ->setTitle('Office 2007 XLSX Test Document')
        ->setSubject('Office 2007 XLSX Test Document')
        ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
        ->setKeywords('office 2007 openxml php')
        ->setCategory('Laporan Pengaduan');

        // Add some data
        $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A5', 'NO')
        ->setCellValue('B5', 'IDENTITAS TKI')
        ->setCellValue('B6', 'NAMA')
        ->setCellValue('C6', 'UMUR')
        ->setCellValue('D6', 'JENIS KELAMIN')
        ->setCellValue('E6', 'NO. PASSPORT')
        ->setCellValue('F6', 'ALAMAT ASAL INDONESIA')
        ->setCellValue('G6', 'ALAMAT DI LUAR NEGERI')
        ->setCellValue('H6', 'EMBARSI')            
        ->setCellValue('I6', 'DEBARSI')
        ;

        $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

        $spreadsheet->getActiveSheet()->getStyle('A5:I6')->applyFromArray($styleArray);

        $spreadsheet->getActiveSheet()->mergeCells('A5:A6');
        $spreadsheet->getActiveSheet()->mergeCells('B5:I5');


        // Miscellaneous glyphs, UTF-8
        $i=7;
        $no=1;
        foreach($pmi as $data) {

            if($data->jk == 'L'){
                $jk = 'Laki-laki';
            }else{
                $jk = 'Perempuan';
            }

            $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $no)
            ->setCellValue('B'.$i, $data->nama)
            ->setCellValue('C'.$i, $data->umur.' Tahun')
            ->setCellValue('D'.$i, $jk)
            ->setCellValue('E'.$i, $data->no_passport)
            ->setCellValue('F'.$i, $data->alamat_asal."\n Kel. ".$data->nama_kelurahan.' Kec. '.$data->nama_kecamatan."\n Kab. Bandung Barat")
            ->setCellValue('G'.$i, $data->alamat_kerja)
            ->setCellValue('H'.$i, $data->embarsi)
            ->setCellValue('I'.$i, $data->debarsi);
            
            $styleisi = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP,
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                    'wrapText' => true,
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

            $spreadsheet->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleisi);
            $spreadsheet->getActiveSheet()->getStyle('B'.$i)->applyFromArray($styleisi);
            $spreadsheet->getActiveSheet()->getStyle('C'.$i)->applyFromArray($styleisi);
            $spreadsheet->getActiveSheet()->getStyle('D'.$i)->applyFromArray($styleisi);
            $spreadsheet->getActiveSheet()->getStyle('E'.$i)->applyFromArray($styleisi);
            $spreadsheet->getActiveSheet()->getStyle('F'.$i)->applyFromArray($styleisi);
            $spreadsheet->getActiveSheet()->getStyle('G'.$i)->applyFromArray($styleisi);
            $spreadsheet->getActiveSheet()->getStyle('H'.$i)->applyFromArray($styleisi);
            $spreadsheet->getActiveSheet()->getStyle('I'.$i)->applyFromArray($styleisi);

            $i++;
            $no++;
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(14);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(36);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(36);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(12);

        $spreadsheet->getActiveSheet()->setTitle('Data PMI');

        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Data PMI.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        ob_end_clean();
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

}
