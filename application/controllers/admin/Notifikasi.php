<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Notifikasi extends MY_Controller
{

  function __construct()
  {
    parent::__construct();
    if ($this->session->userdata('users_login') != 1)
            redirect(base_url() . 'login', 'refresh');
  }

    public function index(){
        if($this->session->userdata('login_type') == 'Admin' || $this->session->userdata('login_type') == 'Superadmin' ){
            $judul = 'Pengaduan Baru';
            $kosong = 'Tidak ada Pengaduan Baru asdas';

            $this->db->where('status_pengaduan', '0');
            $this->db->select('pengaduan.id_pengaduan, status_pengaduan');
            $this->db->from('pengaduan');
            $total = $this->db->count_all_results();

            $this->db->select('id_pengaduan, nama_kelurahan, nama_kecamatan, pelapor.nama as nama_pengadu, tgl, status_pengaduan');
            $this->db->where('status_pengaduan', '0');
            $this->db->from('pengaduan');
            $this->db->join('kecamatan', 'id_kecamatan');
            $this->db->join('kelurahan', 'id_kelurahan');
            $this->db->join('pelapor', 'id_pelapor');
			$this->db->order_by('tgl', 'DESC');
            $inbox = $this->db->get()->result();

        }else if($this->session->userdata('login_type') == 'BNP2TKI' || $this->session->userdata('login_type') == 'BNP3TKI' || $this->session->userdata('login_type') == 'KEMENLU' || $this->session->userdata('login_type') == 'KEMNAKER' || $this->session->userdata('login_type') == 'DISNAKERTRANS-JABAR' || $this->session->userdata('login_type') == 'DISNAKERTRANS'){
            
            $judul = 'Pengaduan Baru';
            $kosong = 'Tidak ada Pengaduan Baru';

            $q1 = $this->db->query('SELECT * FROM pengaduan JOIN terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan JOIN kecamatan ON kecamatan.id_kecamatan=pengaduan.id_kecamatan JOIN kelurahan ON kelurahan.id_kelurahan=pengaduan.id_kelurahan WHERE terusan.id_akses = "'. $this->session->userdata('id_akses') .'" AND pengaduan.dihapus = "0" AND NOT EXISTS (SELECT * FROM baca_user WHERE baca_user.id_pengaduan=pengaduan.id_pengaduan)');
            $total = $q1->num_rows();

            $q1 = $this->db->query('SELECT pengaduan.id_pengaduan as id_pengaduan, nama_kelurahan, nama_kecamatan, pelapor.nama as nama_pengadu, tgl FROM pengaduan JOIN terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan JOIN kecamatan ON kecamatan.id_kecamatan=pengaduan.id_kecamatan JOIN kelurahan ON kelurahan.id_kelurahan=pengaduan.id_kelurahan JOIN pelapor ON pengaduan.id_pelapor=pelapor.id_pelapor WHERE terusan.id_akses = "'. $this->session->userdata('id_akses') .'" AND pengaduan.dihapus = "0" AND NOT EXISTS (SELECT * FROM baca_user WHERE baca_user.id_pengaduan=pengaduan.id_pengaduan)');
            $inbox = $q1->result();

            // $this->db->where('id_akses', $this->session->userdata('id_akses'));
            // $this->db->where('dihapus', '0');
            // $this->db->from('pengaduan');
            // $this->db->join('terusan', 'id_pengaduan');
            // $this->db->join('dibaca', 'id_pengaduan');
            // $total = $this->db->count_all_results();

            // $this->db->where('id_akses', $this->session->userdata('id_akses'));
            // $this->db->select('id_pengaduan, nama_kelurahan, nama_kecamatan, pelapor.nama as nama_pengadu, tgl');
            // $this->db->from('pengaduan');
            // $this->db->join('dibaca', 'id_pengaduan');
            // $this->db->join('kecamatan', 'id_kecamatan');
            // $this->db->join('kelurahan', 'id_kelurahan');
            // $this->db->join('terusan', 'id_pengaduan');
            // $this->db->join('pelapor', 'id_pelapor');
            // $inbox = $this->db->get()->result();
        }else if($this->session->userdata('login_type') == 'Operator'){
            $judul = 'Pengaduan Baru';
            $kosong = 'Tidak ada Pengaduan Baru';

            // $this->db->where('status_pmi', '0');
            // $this->db->where('pengaduan.id_kecamatan', $this->session->userdata('id_kecamatan'));
            // $this->db->where('pengaduan.id_kelurahan', $this->session->userdata('id_kelurahan'));
            // $this->db->from('pengaduan');
            // $total = $this->db->count_all_results();

            $id_kecamatan = $this->session->userdata('kecamatan');
            $id_kelurahan = $this->session->userdata('kelurahan');

            $total = $this->Beranda_m->kasus_proses_operator($id_kecamatan, $id_kelurahan);

            $this->db->select('id_pengaduan, nama_kelurahan, nama_kecamatan, pelapor.nama as nama_pengadu, tgl');
            $this->db->where('status_pmi', '0');
            $this->db->where('pengaduan.id_kecamatan', $id_kecamatan);
            $this->db->where('pengaduan.id_kelurahan', $id_kelurahan);
            $this->db->from('pengaduan');
            $this->db->join('kecamatan', 'id_kecamatan');
            $this->db->join('kelurahan', 'id_kelurahan');
            $this->db->join('pelapor', 'id_pelapor');
            $inbox = $this->db->get()->result();
        }

        $output = '';

        if($total > 0 ){
        	foreach($inbox as $data){
        		$output .= '<li style="padding-left: 3px">
                            <a class="px-0 py-5" href="'. base_url('admin/pengaduan/detail/').$data->id_pengaduan .'">
                                <div  class="font-w600">
                                    Kel. '. $data->nama_kelurahan .' - Kec. '. $data->nama_kecamatan .'
                                </div>
                                <div class="font-size-m text-muted">Pengadu : '. $data->nama_pengadu .'<br>Tanggal : '. $data->tgl .'</div>
                            </a>
                        </li>';
        	}
        }else{
        	$output .= '<li style="padding-left: 3px">
                           <div  class="font-w600">
                                Tidak Ada Pengaduan Baru
                            </div>
                        </li>';
        }

		$data = array(
			'notification' => $output,
    		'total'  => $total
		);

		echo json_encode($data);
    }

}