<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pelapor extends MY_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model('Pelapor_m');
    $this->load->library('email');
  }

    public function index(){

        $data = array(
            'title' => 'Pelapor',
            'sub' => 'Kelola Pelapor',
            'guest_data' => $this->Pelapor_m->get_all(),
          );

        if($this->session->userdata('login_type') == 'Superadmin' || $this->session->userdata('login_type') == 'Admin'){
          $this->admin('pelapor', $data);
        }else{
          $this->temp('pelapor', $data);
        }
    }

    public function simpan(){

        $this->_validate();

        $activation_code = md5(rand().time());
        $email = $this->input->post('email_address');

        $data = array(
            'full_name' => $this->input->post('full_name'),
            'email' => $email,
            'phone' => $this->input->post('phone'),
            'password' => $this->bcrypt->hash_password($this->input->post('password',TRUE)),
            'status' => '1',
            'activation_code' => $activation_code,
        );

        $insert = $this->Pelapor_m->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function edit($id){
        $data = $this->Pelapor_m->get_by_id($id);
        echo json_encode($data);
    }


    public function update($id){

        $data = array(
            'full_name' => $this->input->post('full_name'),
            'email' => $this->input->post('email_address'),
            'phone' => $this->input->post('phone'),
        );

        $this->Pelapor_m->update($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function deactive($id){
        $data = array(
            'status' => '0',
        );
        $this->Pelapor_m->aktivasi($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function activate($id){
        $data = array(
            'status' => '1',
        );
        $this->Pelapor_m->aktivasi($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function delete($id){
        $this->Pelapor_m->delete($id);
        echo json_encode(array("status" => TRUE));
    }


    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('full_name') == ''){
            $data['inputerror'][] = 'full_name';
            $data['error_string'][] = lang('full_name_field_error');
            $data['status'] = FALSE;
        }
        if($this->input->post('email_address') == ''){
            $data['inputerror'][] = 'email_address';
            $data['error_string'][] = lang('email_field_error');
            $data['status'] = FALSE;
        }else{

            $row = $this->db->get_where('guest', array('email' => $this->input->post('email_address')));
            if ($row->num_rows() > 0) {
                $data['inputerror'][] = 'email_address';
                $data['error_string'][] = lang('email_field_used');
                $data['status'] = FALSE;
            }
        }

        if($this->input->post('password') == ''){
            $data['inputerror'][] = 'password';
            $data['error_string'][] = lang('password_field_error');
            $data['status'] = FALSE;
        }

        if($this->input->post('re_password') == ''){
            $data['inputerror'][] = 're_password';
            $data['error_string'][] = lang('re_password_field_blank');
            $data['status'] = FALSE;
        }else if($this->input->post('re_password') <> $this->input->post('password')){
            $data['inputerror'][] = 're_password';
            $data['error_string'][] = lang('re_password_field_error');
            $data['status'] = FALSE;
        }

        if($this->input->post('phone') == ''){
            $data['inputerror'][] = 'phone';
            $data['error_string'][] = lang('phone_field_error');
            $data['status'] = FALSE;
        }else{

            $row = $this->db->get_where('guest', array('phone' => $this->input->post('phone')));
            if ($row->num_rows() > 0) {
                $data['inputerror'][] = 'phone';
                $data['error_string'][] = lang('phone_field_used');
                $data['status'] = FALSE;
            }
        }
        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

    private function _validate_edit(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('full_name') == ''){
            $data['inputerror'][] = 'full_name';
            $data['error_string'][] = lang('full_name_field_error');
            $data['status'] = FALSE;
        }
        if($this->input->post('email_address') == ''){
            $data['inputerror'][] = 'email_address';
            $data['error_string'][] = lang('email_field_error');
            $data['status'] = FALSE;
        }else{

            $row = $this->db->get_where('guest', array('email' => $this->input->post('email_address'), 'id_guest' => $this->input->post('id_guest')));
            if ($row->num_rows() < 0) {
                $data['inputerror'][] = 'email_address';
                $data['error_string'][] = lang('email_field_used');
                $data['status'] = FALSE;
            }
        }

        if($this->input->post('phone') == ''){
            $data['inputerror'][] = 'phone';
            $data['error_string'][] = lang('phone_field_error');
            $data['status'] = FALSE;
        }else{
            $row = $this->db->get_where('guest', array('phone' => $this->input->post('phone'), 'id_guest' => $this->input->post('id_guest')));
            if ($row->num_rows() < 0) {
                $data['inputerror'][] = 'phone';
                $data['error_string'][] = lang('phone_field_used');
                $data['status'] = FALSE;
            }
        }

        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

}
