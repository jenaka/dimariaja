<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Wilayah extends MY_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Wilayah_m');
       if ($this->session->userdata('users_login') != 1)
            redirect(base_url() . 'login', 'refresh');
  }

  public function index(){

  	$data = array(
      'title' => 'Wilayah',
      'sub' => 'Data Kecamatan',
  		'wilayah_data' => $this->Wilayah_m->get_wilayah_all(),
  		'nama_kecamatan' => set_value('nama_kecamatan'),
  	);


  	$this->temp('master/wilayah', $data);
  }

  public function tambah_kecamatan(){
  	$this->form_validation->set_rules('nama_kecamatan', 'Nama Kecamatan', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
        $this->jenis_surat();
    } else {
        $data = array(
                'nama_kecamatan' => ucwords($this->input->post('nama_kecamatan')),
            );

        $this->Wilayah_m->simpan_kecamatan($data);
        $this->session->set_flashdata('success_message', 'Data Kecamatan Baru Berhasil Ditambahkan');
        redirect(site_url('master/wilayah'));
    }

  }

  public function edit_kecamatan($id){

            $data = $this->Wilayah_m->get_kecamatan_by_id($id);

            echo json_encode($data);
    }

   public function update_kecamatan(){

	    $this->form_validation->set_rules('nama_kecamatan', 'Nama Kecamatan', 'trim|required');

	    if ($this->form_validation->run() == FALSE) {
	        $this->jenis_surat();
	    } else {
	        $data = array(
	                'nama_kecamatan' => ucwords($this->input->post('nama_kecamatan')),
	            );

	        $this->Wilayah_m->update_kecamatan(array('id_kecamatan' => $this->input->post('id_kecamatan')), $data);
	        $this->session->set_flashdata('success_message', 'Jenis Surat Berhasil Diperbaharui');
	        redirect(site_url('master/wilayah'));
	    }
   }

    public function hapus_kecamatan($id){
        $this->Master_m->hapus_jenis_surat($id);
        echo json_encode(array("status" => TRUE));
    }

	//Media 

    public function desa($id){

  	$data = array(
      'title' => 'Wilayah',
      'sub' => 'Data Kelurahan',
  		'desa_data' => $this->Wilayah_m->get_desa_all($id),
  		'nama_kelurahan' => set_value('nama_kelurahan'),
  	);


  	$this->temp('master/desa', $data);
  }

  	public function tambah_desa(){
	  	$this->form_validation->set_rules('nama_kelurahan', 'Nama Kelurahan', 'trim|required');
	    if ($this->form_validation->run() == FALSE) {
	        $this->jenis_surat();
	    } else {
	        $data = array(
	                'nama_kelurahan' => ucwords($this->input->post('nama_kelurahan')),
	            );

	        $this->Master_m->simpan_media($data);
	        $this->session->set_flashdata('success_message', 'Media Surat Baru Berhasil Ditambahkan');
	        redirect(site_url('master/media'));
	    }

  	}

  public function media_edit($id){

            $data = $this->Master_m->get_media_by_id($id);

            echo json_encode($data);
    }

   public function update_media(){

	    $this->form_validation->set_rules('nama', 'Nama Media Surat', 'trim|required');

	    if ($this->form_validation->run() == FALSE) {
	        $this->jenis_surat();
	    } else {
	        $data = array(
	                'nama' => ucwords($this->input->post('nama')),
	            );

	        $this->Master_m->update_media(array('id' => $this->input->post('id')), $data);
	        $this->session->set_flashdata('success_message', 'Media Surat Berhasil Diperbaharui');
	        redirect(site_url('master/media'));
	    }
   }

    public function hapus_media($id){
        $this->Master_m->hapus_media($id);
        echo json_encode(array("status" => TRUE));
    }

    // Unit Bidang

    public function bidang(){

    $data = array(
      'bidang_data' => $this->Master_m->get_bidang_all(),
      'nama' => set_value('nama'),
    );


    $this->temp('bidang', $data);
  }

    public function tambah_bidang(){
      $this->form_validation->set_rules('nama', 'Nama Bidang Surat', 'trim|required');
      if ($this->form_validation->run() == FALSE) {
          $this->jenis_surat();
      } else {
          $data = array(
                  'nama' => ucwords($this->input->post('nama')),
              );

          $this->Master_m->simpan_bidang($data);
          $this->session->set_flashdata('success_message', 'Bidang Surat Baru Berhasil Ditambahkan');
          redirect(site_url('master/bidang'));
      }

    }

  public function bidang_edit($id){

            $data = $this->Master_m->get_bidang_by_id($id);

            echo json_encode($data);
    }

   public function update_bidang(){

      $this->form_validation->set_rules('nama', 'Nama Bidang Surat', 'trim|required');

      if ($this->form_validation->run() == FALSE) {
          $this->jenis_surat();
      } else {
          $data = array(
                  'nama' => ucwords($this->input->post('nama')),
              );

          $this->Master_m->update_bidang(array('id' => $this->input->post('id')), $data);
          $this->session->set_flashdata('success_message', 'Bidang Surat Berhasil Diperbaharui');
          redirect(site_url('master/bidang'));
      }
   }

    public function hapus_bidang($id){
        $this->Master_m->hapus_bidang($id);
        echo json_encode(array("status" => TRUE));
    }

    

}