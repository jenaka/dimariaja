<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Pengaduan extends MY_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Pengaduan_m');
    $this->load->helper('tgl_indo');
  }

  public function index(){

        if($this->session->userdata('login_type') == 'Operator'){

            $id_kecamatan = $this->session->userdata('kecamatan');
            $id_kelurahan = $this->session->userdata('kelurahan');

            $pengaduan = $this->Pengaduan_m->get_pengaduan_operator($id_kecamatan, $id_kelurahan);

        }else if($this->session->userdata('login_type') == 'BNP2TKI' || $this->session->userdata('login_type') == 'BNP3TKI' || $this->session->userdata('login_type') == 'KEMENLU' || $this->session->userdata('login_type') == 'KEMNAKER' || $this->session->userdata('login_type') == 'DISNAKERTRANS-JABAR'){
            $id_akses = $this->session->userdata('id_akses');

            $pengaduan = $this->Pengaduan_m->get_pengaduan_terusan($id_akses);
        }else{
            $pengaduan = $this->Pengaduan_m->get_all_pengaduan();
        }

      	$data = array(
      		'title' => 'Pengaduan',
            'sub' => 'Kelola Pengaduan',
            'pengaduan_data' => $pengaduan,
      	);

        if($this->session->userdata('login_type') == 'Superadmin' || $this->session->userdata('login_type') == 'Admin'){
          $this->admin('pengaduan/index', $data);
        }else{
          $this->temp('pengaduan/index', $data);
        }
    }

    public function detail($id){

        $row = $this->Pengaduan_m->get_by_id($id);

        if ($row) {

            $cek = $this->db->get_where('baca_user', array('id_user' => $this->session->userdata('login_user_id')));

            if($cek->num_rows() == 0){

                $read = array(
                    'id_pengaduan' => $row->id_pengaduan,
                    'id_user' => $this->session->userdata('login_user_id'),
                );

                $this->db->insert('baca_user', $read);
            }

            if($row->jk == 'L'){
                $jk = 'Laki-Laki';
            }else{
                $jk = 'Perempuan';
            }

            $data = array(
                'title' => 'Data pengaduan',
                'sub' => 'Detail Data pengaduan',
                'action' => '',
                'id' => $row->id_pengaduan,
                'bukti_pengaduan' => $this->Pengaduan_m->get_bukti_all($id),
                'status' => $row->status_pengaduan,
                'tuntutan' => $row->tuntutan,
                'masalah' => $row->masalah,
                'kec_pmi' => $row->kec_pmi,
                'kel_pmi' => $row->kel_pmi,
                'status_pmi' => $row->status_pmi,
                'nama' => $row->nama,
                'no_passport' => $row->no_passport,
                'tmp_lahir' => $row->tmp_lahir,
                'tgl_lahir' => $row->tgl_lahir,
                'jk' => $jk,
                'alamat_asal' => $row->alamat_asal,
                'alamat_kerja' => $row->alamat_kerja,
                'embarsi' => $row->embarsi,
                'debarsi' => $row->debarsi,
                'nm_pptkis' => $row->nm_pptkis,
                'negara' => $row->negara,
                'mulai_kerja' => $row->mulai_kerja,
                'akhir_kerja' => $row->akhir_kerja,
                'lama_kerja' => $row->lama_kerja,
                'nm_agency' => $row->nm_agency,
                'alamat_agency' => $row->alamat_agency,
                'f_nama_mjk' => $row->f_nama_mjk,
                'l_nama_mjk' => $row->l_nama_mjk,
                'jml_mjk' => $row->jml_mjk,
                'badan_usaha' => $row->badan_usaha,
                'alamat_mjk' => $row->alamat_mjk,
            );

            if($this->session->userdata('login_type') == 'Superadmin' || $this->session->userdata('login_type') == 'Admin'){
              $this->admin('pengaduan/detail', $data);
            }else{
              $this->temp('pengaduan/detail', $data);
            }

      } else {
          $this->session->set_flashdata('error_message', 'Data pengaduan Tidak Ditemukan');
          redirect(site_url('pengaduan'));
      }
    }


   public function trash(){

        if($this->session->userdata('login_type') == 'Operator'){

            $id_kecamatan = $this->session->userdata('kecamatan');
            $id_kelurahan = $this->session->userdata('kelurahan');

            $pengaduan = $this->Pengaduan_m->get_trash_operator($id_kecamatan, $id_kelurahan);

        }else{
            $pengaduan = $this->Pengaduan_m->get_trash();
        }

        $data = array(
            'title' => 'Pengaduan',
            'sub' => 'Data Pengaduan Dihapus',
            'pengaduan_data' => $pengaduan,
        );

        $this->temp('pengaduan/trash', $data);
    }

    public function hapus_lampiran($id){
        $row = $this->Pengaduan_m->get_file_id($id);

        if(file_exists('uploads/pengaduan/'.$row->path) && $row->path)
            unlink('uploads/pengaduan/'.$row->path);

        $this->Pengaduan_m->hapus_file($id);
        echo json_encode(array("status" => TRUE));
    }

    public function verifikasi_pmi($id){
        $data = array('status_pmi' => '1');
        $this->Pengaduan_m->hapus($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function hapus($id){
        $data = array('dihapus' => '1');
        $this->Pengaduan_m->hapus($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function restore($id){
        $data = array('dihapus' => '0');
        $this->Pengaduan_m->hapus($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function permanen($id){
        $this->Pengaduan_m->permanen($id);
        echo json_encode(array("status" => TRUE));
    }

    public function teruskan(){

        $post = $this->input->post();
        $data = array();
        $total_post = count($post['terusan']);

        foreach($post['terusan'] AS $key => $val){
            $data[] = array(
                "id_pengaduan" => $this->input->post('id_pengaduan'),
                "id_akses" => $post['terusan'][$key]
            );
        }

        $this->Pengaduan_m->terusan_tambah($data);

        $this->db->set('status_pengaduan', '1');
        $this->db->where('id_pengaduan', $this->input->post('id_pengaduan'));
        $this->db->update('pengaduan');

        $this->session->set_flashdata('success_message', 'Data pengaduan Berhasil Diperbaharui');
        redirect(site_url('admin/pengaduan'));
    }

    public function cetak($id){

        $row = $this->Pengaduan_m->get_cetak_id($id);

        if ($row) {

            if($row->jk == 'L'){
                $jk_pmi = 'Laki-Laki';
            }else{
                $jk_pmi = 'perempuan';
            }

            $data = array(
                'title' => 'Data pengaduan',
                'sub' => 'Detail Data pengaduan',
                'action' => '',
                'id' => $row->id_pengaduan,
                'bukti_pengaduan' => $this->Pengaduan_m->get_bukti_all($id),
                'status' => $row->status_pengaduan,
                'tuntutan' => $row->tuntutan,
                'masalah' => $row->masalah,
                'kec_pmi' => $row->kec_pmi,
                'kel_pmi' => $row->kel_pmi,
                'status_pmi' => $row->status_pmi,
                'nama' => $row->nama,
                'no_passport' => $row->no_passport,
                'tmp_lahir' => $row->tmp_lahir,
                'tgl_lahir' => $row->tgl_lahir,
                'alamat_asal' => $row->alamat_asal,
                'alamat_kerja' => $row->alamat_kerja,
                'embarsi' => $row->embarsi,
                'debarsi' => $row->debarsi,
                'nm_pptkis' => $row->nm_pptkis,
                'negara' => $row->negara,
                'mulai_kerja' => $row->mulai_kerja,
                'akhir_kerja' => $row->akhir_kerja,
                'lama_kerja' => $row->lama_kerja,
                'nm_agency' => $row->nm_agency,
                'alamat_agency' => $row->alamat_agency,
                'f_nama_mjk' => $row->f_nama_mjk,
                'l_nama_mjk' => $row->l_nama_mjk,
                'jml_mjk' => $row->jml_mjk,
                'badan_usaha' => $row->badan_usaha,
                'alamat_mjk' => $row->alamat_mjk,
            );
        }

        $this->load->library('Pdf');
        $this->pdf->setPaper('F4', 'potrait');
        $this->pdf->filename = "pengaduan-".$row->no_passport.".pdf";
        $this->pdf->load_view('admin/pengaduan/cetak', $data);
        $this->pdf->render();
    }


    public function tambah_mobile(){

        $data = array(
            'id' => $this->Pengaduan_m->id_pengaduan(),
            'action' => site_url('pengaduan/simpan'),
            'nama_pengadu' => set_value('nama_pengadu'),
            'umur_pengadu' => set_value('umur_pengadu'),
            'pekerjaan_pengadu' => set_value('pekerjaan_pengadu'),
            'hubungan_pengadu' => set_value('hubungan_pengadu'),
            'telp_pengadu' => set_value('telp_pengadu'),
            'id_kecamatan' => set_value('id_kecamatan'),
            'id_kelurahan' => set_value('id_kelurahan'),
            'alamat' => set_value('alamat'),
            'status' => set_value('status'),
            'tuntutan' => set_value('tuntutan'),
            'masalah' => set_value('masalah'),
            'id_pmi' => set_value('id_pmi'),
            'kecamatan_data' => $this->Wilayah_m->get_wilayah_all(),
            'kelurahan_data' => $this->Wilayah_m->get_kelurahan_all(),
            'kecamatan_selected' => '',
            'kelurahan_selected' => '',
            'no_passport' => set_value('no_passport'),
            'pmi_data' => $this->Pmi_m->get_all()
        );

        $this->load->view('pengaduan/form_mobile', $data);
    }

}
