<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Pengguna extends MY_Controller
{

    function __construct(){
        parent::__construct();
        $this->load->model('Pengguna_m');
        if ($this->session->userdata('users_login') != 1)
                redirect(base_url() . 'login', 'refresh');
    }

    public function index(){

        $data = array(
            'title' => 'Pengguna',
            'sub' => 'Kelola Pengguna',
            'jabatan_data' => $this->Pengguna_m->get_akses_all(),
            'users_data' => $this->Pengguna_m->get_all_pengguna(),
        );


        if($this->session->userdata('login_type') == 'Superadmin' || $this->session->userdata('login_type') == 'Admin'){
          $this->admin('pengguna/index', $data);
        }else{
          $this->temp('pengguna/index', $data);
        }
    }

    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('nama') == ''){
            $data['inputerror'][] = 'nama';
            $data['error_string'][] = 'Nama Lengkap Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if($this->input->post('id_akses') == ''){
            $data['inputerror'][] = 'id_akses';
            $data['error_string'][] = 'Hak Akses Pengguna Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if($this->input->post('username') == ''){
            $data['inputerror'][] = 'username';
            $data['error_string'][] = 'Username Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }else{

            $row = $this->db->get_where('users', array('username' => $this->input->post('username')));
            if ($row->num_rows() > 0) {
                $data['inputerror'][] = 'username';
                $data['error_string'][] = 'Username Sudah Digunakan';
                $data['status'] = FALSE;
            }
        }

        if($this->input->post('email') == ''){
            $data['inputerror'][] = 'email';
            $data['error_string'][] = 'Alamat Email Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }else{

            $row = $this->db->get_where('users', array('email' => $this->input->post('email')));
            if ($row->num_rows() > 0) {
                $data['inputerror'][] = 'email';
                $data['error_string'][] = 'Alamat Email Sudah Digunakan';
                $data['status'] = FALSE;
            }
        }

        if($this->input->post('password') == ''){
            $data['inputerror'][] = 'password';
            $data['error_string'][] = 'Kata Sandi Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if($this->input->post('re_password') == ''){
            $data['inputerror'][] = 're_password';
            $data['error_string'][] = 'Konfirmasi Kata Sandi Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }else if($this->input->post('re_password') <> $this->input->post('password')){
            $data['inputerror'][] = 're_password';
            $data['error_string'][] = 'Konfirmasi Kata Sandi Tidak Sama Dengan Kata Sandi';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

    public function simpan(){

       $this->_validate();

        $data = array(
          'nama' => $this->input->post('nama',TRUE),
          'email' => $this->input->post('email',TRUE),
          'username' => $this->input->post('username',TRUE),
          'password' => $this->bcrypt->hash_password($this->input->post('password',TRUE)),
          'id_akses' => $this->input->post('id_akses',TRUE),
          'active' => '1',
        );

        $insert = $this->Pengguna_m->tambah($data);
        echo json_encode(array("status" => TRUE));
    }

    public function edit($id){
        $data = $this->Pengguna_m->get_by_id($id);
        echo json_encode($data);
    }

    public function update(){

        $data = array(
            'nama' => $this->input->post('nama',TRUE),
            'username' => $this->input->post('username',TRUE),
            'email' => $this->input->post('email',TRUE),
            'telp' => $this->input->post('telp',TRUE),
            'id_akses' => $this->input->post('id_akses',TRUE),
        );

        $this->Pengguna_m->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function trash(){

        $data = array(
            'title' => 'Pengguna',
            'sub' => 'Data Pengguna Dihapus',
            'users_data' => $this->Pengguna_m->get_trash(),
        );

        $this->temp('pengguna/trash', $data);
    }

    public function aktif($id){
        $data = array('active' => 1);
        $this->Pengguna_m->aktivasi($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function nonaktif($id){
        $data = array('active' => 0);
        $this->Pengguna_m->aktivasi($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function hapus($id){
        $data = array('dihapus' => '1');
        $this->Pengguna_m->hapus($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function restore($id){
        $data = array('dihapus' => '0');
        $this->Pengguna_m->hapus($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function permanen($id){
        $this->Pengguna_m->permanen($id);
        echo json_encode(array("status" => TRUE));
    }


        //Akses

    public function akses(){

        $data = array(
            'title' => 'Jabatan Pengguna',
            'sub' => 'Data Jabatan Pengguna',
            'akses_data' => $this->Pengguna_m->get_akses_all(),
            'nama' => set_value('nama'),
        );


        $this->temp('pengguna/akses', $data);
    }


    public function tambah_akses(){
        $this->form_validation->set_rules('nama', 'Nama Jabatan', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->jenis_surat();
        } else {
            $data = array(
                    'nama' => ucwords($this->input->post('nama')),
                );

            $this->Pengguna_m->simpan_akses($data);
            $this->session->set_flashdata('success_message', 'Akses Surat Baru Berhasil Ditambahkan');
            redirect(site_url('pengguna/akses'));
        }

    }

    public function akses_edit($id){

            $data = $this->Pengguna_m->get_akses_by_id($id);

            echo json_encode($data);
    }

    public function update_akses(){

        $this->form_validation->set_rules('nama', 'Nama Akses Surat', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->jenis_surat();
        } else {
            $data = array(
                    'nama' => ucwords($this->input->post('nama')),
                );

            $this->Pengguna_m->update_akses(array('id' => $this->input->post('id')), $data);
            $this->session->set_flashdata('success_message', 'Akses Surat Berhasil Diperbaharui');
            redirect(site_url('pengguna/akses'));
        }
    }

    public function hapus_akses($id){
        $this->Pengguna_m->hapus_akses($id);
        echo json_encode(array("status" => TRUE));
    }

    public function ganti_password(){

        $data = array(
            'title' => 'Pengguna',
            'sub' => 'Ganti Password',
        );

      $this->temp('pengguna/ganti_pw', $data);
    }

    public function ganti_pw(){
        $this->form_validation->set_rules('password_lama', 'Password Lama', 'trim|required');
        $this->form_validation->set_rules('password_baru', 'Password Baru', 'trim|required');
        $this->form_validation->set_rules('password_baru_knf', 'Konfirmasi Password Baru', 'required|matches[password_baru]');
        if ($this->form_validation->run() == FALSE) {
            $this->ganti_password();
        } else {

            $id = $this->session->userdata('id');
            $password = $this->input->post('password_lama',TRUE);

            if($this->session->userdata('login_type') == 'Operator'){
                $get_id = $this->Operator_m->get_by_id($id);
            }else{
                $get_id = $this->Pengguna_m->get_by_id($id);
            }

            $row = $get_id;

            if($this->bcrypt->check_password($password, $row->password)){
                // jika password pada db sama dengan password inputan lama

                $data = array(
                    'password' => $this->bcrypt->hash_password($this->input->post('password_baru',TRUE))
                );

                if($this->session->userdata('login_type') == 'Operator'){
                    $this->Operator_m->update(array('id' => $id), $data);
                }else{
                    $this->Pengguna_m->update(array('id' => $id), $data);
                }
                $this->session->set_flashdata('success_message', 'Password Berhasil Diperbaharui');
                redirect(site_url('admin/beranda'));

            }else{
                $this->session->set_flashdata('error_message', 'Password Lama Salah!');
                redirect(base_url() . 'admin/pengguna/ganti_password', 'refresh');
            }
        }
    }
}
