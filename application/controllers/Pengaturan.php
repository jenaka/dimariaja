<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengaturan extends MY_Controller {

  function __construct(){
    parent::__construct();
  }

    public function index(){

  	  $row = $this->Pelapor_m->get_by_session();
      $data = array(
          'page_title' => 'Home',
          'id_pelapor' => $row->id_pelapor,
          'nama' => $row->nama,
          'email' => $row->email,
          'phone' => $row->phone,
      );
      $this->pelapor('pengaturan', $data);
    } // end function index

    public function ubah_info(){

      // rules untuk dipenuhi dibawah
      $this->form_validation->set_rules('nama', 'Nama Lengkap', 'trim|required');
      $this->form_validation->set_rules('email', 'E-mail', 'trim|required');
      $this->form_validation->set_rules('phone', 'No. handphone', 'trim|required');
      $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

      if ($this->form_validation->run() == FALSE) {
        // jika rules di form_validation tidak terpenuhi
        redirect(base_url('pengaturan#info-umum'));

      }else {
        // jika rules di form_validation sudah terpenuhi
        // assign inputan nama, email, phone lama ke $nama, $email, $phone
        $nama  = $this->input->post('nama');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');

        // update nama, email, phone lama pada db dengan yg baru yg sudah di input
        $this->db->set('nama', $nama);
        $this->db->set('email', $email);
        $this->db->set('phone', $phone);
        $this->db->where('id_pelapor', $this->session->userdata('pelapor_id'));
        $this->db->update('pelapor');

        // perbarui session untuk nama, email, phone dengan data terbaru
        $this->session->set_userdata('pelapor_nama', $nama);
        $this->session->set_userdata('pelapor_mail', $email);
        $this->session->set_userdata('pelapor_phone', $phone);
        $this->session->set_flashdata('success_message', 'Data diri anda berhasil diubah');
        redirect(base_url('pengaturan#info-umum'), 'refresh');

      }
    }



    public function ubah_pw(){

        // rules untuk dipenuhi dibawah
        $this->form_validation->set_rules('ubah-lama', 'Kata Sandi Lama', 'trim|required');
        $this->form_validation->set_rules('ubah-baru', 'Kata Sandi Baru', 'trim|required');
        $this->form_validation->set_rules('ubah-knf', 'Konfirmasi Kata Sandi Baru', 'trim|required|matches[ubah-baru]');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
          // jika rules di form_validation tidak terpenuhi
          redirect(base_url('pengaturan#ubah-kata-sandi'));

        } else {
          // jika rules di form_validation sudah terpenuhi
          // fetch data dari db berdasar session.
          // assign inputan password lama ke $ubah_lama
          $row = $this->Pengguna_m->get_by_session();
          // assign inputan password lama ke var $ubah_lama
          $row = $this->Pelapor_m->get_by_session();
          $ubah_lama = $this->input->post('ubah-lama');

          if($this->bcrypt->check_password($ubah_lama, $row->password)){
            // cek apakah $ubah_lama cocok dengan data password di db
            // jika sudah cocok
            // hash lalu assign inputan var $ubah_baru
            $ubah_baru = $this->bcrypt->hash_password($this->input->post('ubah-baru',TRUE));
            // update password lama pada db dengan password baru yg sudah di hash
            $this->db->set('password', $ubah_baru);
            $this->db->where('id_pelapor', $this->session->userdata('pelapor_id'));
            $this->db->update('pelapor');
            $this->session->set_flashdata('success_message', 'Password berhasil diubah');
            redirect(base_url('pengaturan#ubah-kata-sandi'), 'refresh');

          }else{
            // jika tidak cocok
            $this->session->set_flashdata('error_message', 'Password lama anda tidak sesuai');
            redirect(base_url('pengaturan#ubah-kata-sandi'), 'refresh');
          }

        }
    } // end function ubah_pw

}
