<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda_m extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

  public function count_pelapor_all(){
    $this->db->from('pelapor');
    return $this->db->count_all_results();
  }

  public function count_kontak_pesan(){
    $this->db->where('is_read', '0');
    $this->db->from('kontak');
    return $this->db->count_all_results();
  }

	public function count_kasus_all(){
		$this->db->where('dihapus', '0');
		$this->db->from('pengaduan');
		return $this->db->count_all_results();
	}

	public function kasus_proses_all(){
		$this->db->where('dihapus', '0');
		$this->db->where('status_pengaduan', '0');
		$this->db->from('pengaduan');
		return $this->db->count_all_results();
	}

	public function count_kasus_terusan($id_akses){
		$this->db->where('dihapus', '0');
		$this->db->where('id_akses', $id_akses);
		$this->db->from('pengaduan');
		$this->db->join('terusan', 'id_pengaduan');
		return $this->db->count_all_results();
	}

	public function kasus_proses_terusan($id_akses){
		$this->db->where('id_akses', $id_akses);
		$this->db->where('dihapus', '0');
		$this->db->where('status_pengaduan', '0');
		$this->db->from('pengaduan');
		$this->db->join('terusan', 'id_pengaduan');
		return $this->db->count_all_results();
	}

	public function count_kasus_operator($id_kecamatan, $id_kelurahan){
		$this->db->where('dihapus', '0');
		$this->db->where('pengaduan.id_kecamatan', $id_kecamatan);
		$this->db->where('pengaduan.id_kelurahan', $id_kelurahan);
		$this->db->from('pengaduan');
		$this->db->order_by('tgl', 'DESC');
		return $this->db->count_all_results();
	}

	public function kasus_proses_operator($id_kecamatan, $id_kelurahan){
		$this->db->where('dihapus', '0');
		$this->db->where('status_pengaduan', '0');
		$this->db->where('pengaduan.id_kecamatan', $id_kecamatan);
		$this->db->where('pengaduan.id_kelurahan', $id_kelurahan);
		$this->db->from('pengaduan');
		$this->db->order_by('tgl', 'DESC');
		return $this->db->count_all_results();
	}

	public function get_chart_admin(){
        $br = $this->db->query("
            select
              ifnull((SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=1)AND (YEAR(tgl)=2019))),0) AS `Januari`,
              ifnull((SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=2)AND (YEAR(tgl)=2019))),0) AS `Februari`,
              ifnull((SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=3)AND (YEAR(tgl)=2019))),0) AS `Maret`,
              ifnull((SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=4)AND (YEAR(tgl)=2019))),0) AS `April`,
              ifnull((SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=5)AND (YEAR(tgl)=2019))),0) AS `Mei`,
              ifnull((SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=6)AND (YEAR(tgl)=2019))),0) AS `Juni`,
              ifnull((SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=7)AND (YEAR(tgl)=2019))),0) AS `Juli`,
              ifnull((SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=8)AND (YEAR(tgl)=2019))),0) AS `Agustus`,
              ifnull((SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=9)AND (YEAR(tgl)=2019))),0) AS `September`,
              ifnull((SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=10)AND (YEAR(tgl)=2019))),0) AS `Oktober`,
              ifnull((SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=11)AND (YEAR(tgl)=2019))),0) AS `November`,
              ifnull((SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=12)AND (YEAR(tgl)=2019))),0) AS `Desember`
             from pengaduan GROUP BY YEAR(tgl)
        ");

        return $br;
    }

    public function get_chart_atasan(){
        $br = $this->db->query("
            select
              ifnull((SELECT count(pengaduan.id_pengaduan) FROM (pengaduan join terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan)WHERE((Month(tgl)=1)AND (YEAR(tgl)=2019) AND (id_akses = ".$this->session->userdata('id_akses').") )),0) AS `Januari`,
              ifnull((SELECT count(pengaduan.id_pengaduan) FROM (pengaduan join terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan)WHERE((Month(tgl)=2)AND (YEAR(tgl)=2019) AND (id_akses = ".$this->session->userdata('id_akses').") )),0) AS `Februari`,
              ifnull((SELECT count(pengaduan.id_pengaduan) FROM (pengaduan join terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan)WHERE((Month(tgl)=3)AND (YEAR(tgl)=2019) AND (id_akses = ".$this->session->userdata('id_akses').") )),0) AS `Maret`,
              ifnull((SELECT count(pengaduan.id_pengaduan) FROM (pengaduan join terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan)WHERE((Month(tgl)=4)AND (YEAR(tgl)=2019) AND (id_akses = ".$this->session->userdata('id_akses').") )),0) AS `April`,
              ifnull((SELECT count(pengaduan.id_pengaduan) FROM (pengaduan join terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan)WHERE((Month(tgl)=5)AND (YEAR(tgl)=2019) AND (id_akses = ".$this->session->userdata('id_akses').") )),0) AS `Mei`,
              ifnull((SELECT count(pengaduan.id_pengaduan) FROM (pengaduan join terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan)WHERE((Month(tgl)=6)AND (YEAR(tgl)=2019) AND (id_akses = ".$this->session->userdata('id_akses').") )),0) AS `Juni`,
              ifnull((SELECT count(pengaduan.id_pengaduan) FROM (pengaduan join terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan)WHERE((Month(tgl)=7)AND (YEAR(tgl)=2019) AND (id_akses = ".$this->session->userdata('id_akses').") )),0) AS `Juli`,
              ifnull((SELECT count(pengaduan.id_pengaduan) FROM (pengaduan join terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan)WHERE((Month(tgl)=8)AND (YEAR(tgl)=2019) AND (id_akses = ".$this->session->userdata('id_akses').") )),0) AS `Agustus`,
              ifnull((SELECT count(pengaduan.id_pengaduan) FROM (pengaduan join terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan)WHERE((Month(tgl)=9)AND (YEAR(tgl)=2019) AND (id_akses = ".$this->session->userdata('id_akses').") )),0) AS `September`,
              ifnull((SELECT count(pengaduan.id_pengaduan) FROM (pengaduan join terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan)WHERE((Month(tgl)=10)AND (YEAR(tgl)=2019) AND (id_akses = ".$this->session->userdata('id_akses').") )),0) AS `Oktober`,
              ifnull((SELECT count(pengaduan.id_pengaduan) FROM (pengaduan join terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan)WHERE((Month(tgl)=11)AND (YEAR(tgl)=2019) AND (id_akses = ".$this->session->userdata('id_akses').") )),0) AS `November`,
              ifnull((SELECT count(pengaduan.id_pengaduan) FROM (pengaduan join terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan)WHERE((Month(tgl)=12)AND (YEAR(tgl)=2019) AND (id_akses = ".$this->session->userdata('id_akses').") )),0) AS `Desember`
             from pengaduan join terusan ON terusan.id_pengaduan=pengaduan.id_pengaduan GROUP BY YEAR(tgl)
        ");

        return $br;
    }

    public function get_Chart_operator(){
        $br = $this->db->query("
            select
              ifnull( (SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=1)AND (YEAR(tgl)=2019) AND (id_kelurahan = ".$this->session->userdata('kelurahan').") )),0) AS `Januari`,
              ifnull( (SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=2)AND (YEAR(tgl)=2019) AND (id_kelurahan = ".$this->session->userdata('kelurahan').") )),0) AS `Februari`,
              ifnull( (SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=3)AND (YEAR(tgl)=2019) AND (id_kelurahan = ".$this->session->userdata('kelurahan').") )),0) AS `Maret`,
              ifnull( (SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=4)AND (YEAR(tgl)=2019) AND (id_kelurahan = ".$this->session->userdata('kelurahan').") )),0) AS `April`,
              ifnull( (SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=5)AND (YEAR(tgl)=2019) AND (id_kelurahan = ".$this->session->userdata('kelurahan').") )),0) AS `Mei`,
              ifnull( (SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=6)AND (YEAR(tgl)=2019) AND (id_kelurahan = ".$this->session->userdata('kelurahan').") )),0) AS `Juni`,
              ifnull( (SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=7)AND (YEAR(tgl)=2019) AND (id_kelurahan = ".$this->session->userdata('kelurahan').") )),0) AS `Juli`,
              ifnull( (SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=8)AND (YEAR(tgl)=2019) AND (id_kelurahan = ".$this->session->userdata('kelurahan').") )),0) AS `Agustus`,
              ifnull( (SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=9)AND (YEAR(tgl)=2019) AND (id_kelurahan = ".$this->session->userdata('kelurahan').") )),0) AS `September`,
              ifnull( (SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=10)AND (YEAR(tgl)=2019) AND (id_kelurahan = ".$this->session->userdata('kelurahan').") )),0) AS `Oktober`,
              ifnull( (SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=11)AND (YEAR(tgl)=2019) AND (id_kelurahan = ".$this->session->userdata('kelurahan').") )),0) AS `November`,
              ifnull( (SELECT count(id_pengaduan) FROM (pengaduan)WHERE((Month(tgl)=12)AND (YEAR(tgl)=2019) AND (id_kelurahan = ".$this->session->userdata('kelurahan').") )),0) AS `Desember`
             from pengaduan GROUP BY YEAR(tgl)
        ");
        return $br;
    }

}
