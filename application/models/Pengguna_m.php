<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna_m extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_all_pengguna(){

		$this->db->from('users');
		$this->db->where('dihapus', '0');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_session(){
		$this->db->from('users');
		$this->db->where('id', $this->session->userdata('id'));
		return $this->db->get()->row();
	}

	public function get_by_id($id){
		
		$this->db->from('users');
		$this->db->where('id',$id);
		$query = $this->db->get(); 

		return $query->row();
	}

	public function tambah($data)
	{
		$this->db->insert('users', $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update('users', $data, $where);
		return $this->db->affected_rows();
	}

	// Auth

	public function check_email($email){
		$this->db->select('users.id as id');
		$this->db->select('users.nama as nama');
		$this->db->select('email');
		$this->db->select('password');
		$this->db->select('active');
		$this->db->select('paraf');
		$this->db->select('user_akses.nama as akses');
		$this->db->from('users');
		$this->db->join('user_akses', 'user_akses.id=users.id_akses');
		$this->db->where('email',$email);
		return $this->db->get(); 
	}

	public function get_trash(){
		$this->db->from('users');
		$this->db->where('dihapus', '1');
		$query = $this->db->get();
		return $query->result();
	}

	public function hapus($id, $data){
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function restore($id, $data){
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function permanen($id){
        $this->db->where('id', $id);
        $this->db->delete('users');
    }

    public function aktivasi($id, $data){
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }


	// Media

	public function get_akses_all(){

		$this->db->from('user_akses');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_akses_by_id($id){
		
		$this->db->from('user_akses');
		$this->db->where('id',$id);
		$query = $this->db->get(); 

		return $query->row();
	}

	public function simpan_akses($data)
	{
		$this->db->insert('user_akses', $data);
		return $this->db->insert_id();
	}

	public function update_akses($where, $data)
	{
		$this->db->update('user_akses', $data, $where);
		return $this->db->affected_rows();
	}

	public function hapus_akses($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('user_akses');
	}

}
