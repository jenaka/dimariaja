<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pmi_m extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function id_pmi(){
        $q = $this->db->query("SELECT MAX(id) AS kd_max FROM pmi");
        $urut = "";
        if($q->num_rows()>0){
            foreach($q->result() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $urut = sprintf("%01s", $tmp);
            }
        }else{
            $urut = "01";
        }
        return $urut;
    }

    // get all
    public function get_all()
    {
        $this->db->select('pmi.id as id_pmi, nama_kelurahan, nama_kecamatan, nama, alamat_asal, mulai_kerja, alamat_kerja, no_passport, jk, embarsi, debarsi');
        $this->db->where('dihapus', '0');
        $this->db->from('pmi');
        $this->db->join('kecamatan', 'id_kecamatan');
        $this->db->join('kelurahan', 'id_kelurahan');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_pmi_op($id_kecamatan, $id_kelurahan)
    {
        $this->db->select('pmi.id as id_pmi, nama_kelurahan, nama_kecamatan, nama, alamat_asal, mulai_kerja, alamat_kerja, no_passport, jk, embarsi, debarsi');
        $this->db->where('dihapus', '0');
        $this->db->where('pmi.id_kecamatan', $id_kecamatan);
        $this->db->where('pmi.id_kelurahan', $id_kelurahan);
        $this->db->from('pmi');
        $this->db->join('kecamatan', 'id_kecamatan');
        $this->db->join('kelurahan', 'id_kelurahan');
        $query = $this->db->get();
        return $query->result();
    }

    // get data by id
    public function get_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->from('pmi');
        $this->db->join('kecamatan', 'id_kecamatan');
        $this->db->join('kelurahan', 'id_kelurahan');
        return $this->db->get()->row();
    }

    // insert data
    public function insert($data, $uploadData){
        $this->db->insert('pmi', $data);

        $this->db->insert_batch('lampiran_pmi',$uploadData);
    }

    public function get_lampiran($id){

        $this->db->where('id_pmi', $id);
        $this->db->from('lampiran_pmi');
        $query = $this->db->get();
        return $query->result();
    }

    // update data
    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    public function hapus($id, $data){
        $this->db->where('id', $id);
        $this->db->update('pmi', $data);
    }

    public function restore($id, $data){
        $this->db->where('id', $id);
        $this->db->update('pmi', $data);
    }

    public function permanen($id){
        $this->db->where('id', $id);
        $this->db->delete('pmi');
    }

    public function get_trash()
    {
        $this->db->select('pmi.id as id_pmi, nama_kelurahan, nama_kecamatan, nama, alamat_asal, mulai_kerja, alamat_kerja, no_passport');
        $this->db->where('dihapus', '1');
        $this->db->from('pmi');
        $this->db->join('kecamatan', 'id_kecamatan');
        $this->db->join('kelurahan', 'id_kelurahan');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_trash_op($id_kecamatan, $id_kelurahan)
    {
        $this->db->select('pmi.id as id_pmi, nama_kelurahan, nama_kecamatan, nama, alamat_asal, mulai_kerja, alamat_kerja, no_passport');
        $this->db->where('dihapus', '1');
        $this->db->where('pmi.id_kecamatan', $id_kecamatan);
        $this->db->where('pmi.id_kelurahan', $id_kelurahan);
        $this->db->from('pmi');
        $this->db->join('kecamatan', 'id_kecamatan');
        $this->db->join('kelurahan', 'id_kelurahan');
        $query = $this->db->get();
        return $query->result();
    }

}
