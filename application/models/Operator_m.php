<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Operator_m extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_all_operator(){

		$this->db->from('operator');
		$this->db->where('dihapus', '0');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_id($id){
		
		$this->db->from('operator');
		$this->db->join('kecamatan', 'id_kecamatan');
		$this->db->join('kelurahan', 'id_kelurahan');
		$this->db->where('id',$id);
		$query = $this->db->get(); 

		return $query->row();
	}

	public function tambah($data){
		$this->db->insert('operator', $data);
		return $this->db->insert_id();
	}
	
	public function update($where, $data){
		$this->db->update('operator', $data, $where);
		return $this->db->affected_rows();
	}

	// Auth

	public function check_email($email){
		$this->db->select('id');
		$this->db->select('nama');
		$this->db->select('email');
		$this->db->select('username');
		$this->db->select('password');
		$this->db->select('id_kecamatan');
		$this->db->select('id_kelurahan');
		$this->db->select('active');
		$this->db->from('operator');
		$this->db->where('email',$email);
		$this->db->or_where('username',$email);
		return $this->db->get(); 
	}

	public function get_trash(){
		$this->db->from('operator');
		$this->db->where('dihapus', '1');
		$query = $this->db->get();
		return $query->result();
	}

	public function hapus($id, $data){
        $this->db->where('id', $id);
        $this->db->update('operator', $data);
    }

    public function restore($id, $data){
        $this->db->where('id', $id);
        $this->db->update('operator', $data);
    }

    public function permanen($id){
        $this->db->where('id', $id);
        $this->db->delete('operator');
    }

    public function aktivasi($id, $data){
        $this->db->where('id', $id);
        $this->db->update('operator', $data);
    }

}
