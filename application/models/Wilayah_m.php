<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wilayah_m extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_wilayah_all(){
        $this->db->select('kecamatan.id_kecamatan as id_kecamatan');
        $this->db->select('nama_kecamatan');
        $this->db->select('COUNT(id_kelurahan) as total');
        $this->db->group_by('kecamatan.id_kecamatan');
        $this->db->from('kecamatan');
        $this->db->join('kelurahan', 'kelurahan.id_kecamatan=kecamatan.id_kecamatan');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_kecamatan_by_id($id){
		
		$this->db->from('kecamatan');
		$this->db->where('id_kecamatan',$id);
		$query = $this->db->get(); 

		return $query->row();
	}

	public function simpan_kecamatan($data)
	{
		$this->db->insert('kecamatan', $data);
		return $this->db->insert_id();
	}

	public function update_kecamatan($where, $data)
	{
		$this->db->update('kecamatan', $data, $where);
		return $this->db->affected_rows();
	}

	public function hapus_jenis_surat($id)
	{
		$this->db->where('id_kecamatan', $id);
		$this->db->delete('kecamatan');
	}


	// Media
	public function get_kelurahan_all(){

		$this->db->from('kelurahan');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_desa_all($id){

		$this->db->from('kelurahan');
		$this->db->where('id_kecamatan', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_media_by_id($id){
		
		$this->db->from('media');
		$this->db->where('id',$id);
		$query = $this->db->get(); 

		return $query->row();
	}

	public function simpan_media($data)
	{
		$this->db->insert('media', $data);
		return $this->db->insert_id();
	}

	public function update_media($where, $data)
	{
		$this->db->update('media', $data, $where);
		return $this->db->affected_rows();
	}

	public function hapus_media($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('media');
	}

	// Bidang

	public function get_bidang_all(){

		$this->db->from('unit_bidang');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_bidang_by_id($id){
		
		$this->db->from('unit_bidang');
		$this->db->where('id',$id);
		$query = $this->db->get(); 

		return $query->row();
	}

	public function simpan_bidang($data)
	{
		$this->db->insert('unit_bidang', $data);
		return $this->db->insert_id();
	}

	public function update_bidang($where, $data)
	{
		$this->db->update('unit_bidang', $data, $where);
		return $this->db->affected_rows();
	}

	public function hapus_bidang($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('unit_bidang');
	}

}
