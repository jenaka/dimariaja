<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaduan_m extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function id_pengaduan(){
        $q = $this->db->query("SELECT  MAX(RIGHT(id_pengaduan,3)) AS kd_max FROM pengaduan");
        $urut = "";
        if($q->num_rows()>0){
            foreach($q->result() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $urut = sprintf("%03s", $tmp);
            }
        }else{
            $urut = "001";
        }
        date_default_timezone_set('Asia/Jakarta');
        return date('ymd').$urut;
	}

	public function get_all_pengaduan(){
		$this->db->select('id_pengaduan, nama_kelurahan, nama_kecamatan, pelapor.phone as telp_pengadu, pelapor.nama as nama_pengadu, tgl, status_pmi, status_pengaduan as status, pengaduan.nama as nama, no_passport, id_pelapor');
		$this->db->where('pengaduan.dihapus', '0');
		$this->db->from('pengaduan');
		$this->db->join('kecamatan', 'id_kecamatan');
		$this->db->join('kelurahan', 'id_kelurahan');
		$this->db->join('pelapor', 'id_pelapor');
		$this->db->order_by('tgl', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_pengaduan_operator($id_kecamatan, $id_kelurahan){
		$this->db->select('id_pengaduan, nama_kelurahan, nama_kecamatan, pelapor.nama as nama_pengadu, tgl, status_pmi, status_pengaduan as status, pelapor.phone as telp_pengadu, pengaduan.nama as nama, no_passport, id_pelapor');
		$this->db->where('pengaduan.dihapus', '0');
		$this->db->where('pengaduan.id_kecamatan', $id_kecamatan);
		$this->db->where('pengaduan.id_kelurahan', $id_kelurahan);
		$this->db->from('pengaduan');
		$this->db->join('kecamatan', 'id_kecamatan');
		$this->db->join('kelurahan', 'id_kelurahan');
		$this->db->join('pelapor', 'id_pelapor');
		$this->db->order_by('tgl', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_pengaduan_terusan($id_akses){
		$this->db->select('id_pengaduan, nama_kelurahan, nama_kecamatan, pelapor.nama as nama_pengadu, tgl, status, pelapor.phone as telp_pengadu, pengaduan.nama as nama, no_passport, id_pelapor, status_pmi');
		$this->db->where('id_akses', $id_akses);
        $this->db->where('pengaduan.dihapus', '0');
        $this->db->from('pengaduan');
        $this->db->join('kecamatan', 'id_kecamatan');
        $this->db->join('kelurahan', 'id_kelurahan');
        $this->db->join('terusan', 'id_pengaduan');
		$this->db->join('pelapor', 'id_pelapor');
        $this->db->order_by('tgl', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_id($id){
		$this->db->select('id_pengaduan, status_pengaduan, tuntutan, masalah, tgl');
		$this->db->select('nama, no_passport, tmp_lahir, tgl_lahir, alamat_asal, alamat_kerja, embarsi, debarsi, nm_pptkis, negara, mulai_kerja, akhir_kerja, lama_kerja, nm_agency, alamat_agency, f_nama_mjk, l_nama_mjk, jml_mjk, badan_usaha, alamat_mjk, a.nama_kecamatan as kec_pmi, b.nama_kelurahan as kel_pmi, jk, status_pmi');
		$this->db->from('pengaduan');
		$this->db->where('pengaduan.id_pengaduan',$id);
		$this->db->join('kecamatan a', 'id_kecamatan');
		$this->db->join('kelurahan b', 'id_kelurahan');
		$query = $this->db->get(); 

		return $query->row();
	}

	public function get_cetak_id($id){
		$this->db->select('id_pengaduan, status_pengaduan, tuntutan, masalah, tgl');
		$this->db->select('nama, no_passport, tmp_lahir, tgl_lahir, alamat_asal, alamat_kerja, embarsi, debarsi, nm_pptkis, negara, mulai_kerja, akhir_kerja, lama_kerja, nm_agency, alamat_agency, f_nama_mjk, l_nama_mjk, jml_mjk, badan_usaha, alamat_mjk, a.nama_kecamatan as kec_pmi, b.nama_kelurahan as kel_pmi, jk, status_pmi');
		$this->db->from('pengaduan');
		$this->db->where('pengaduan.id_pengaduan',$id);
		$this->db->join('kecamatan a', 'id_kecamatan');
		$this->db->join('kelurahan b', 'id_kelurahan');
		$query = $this->db->get(); 

		return $query->row();
	}

	public function total_pengaduan($search = '') {

        $this->db->select('count(*) as allcount');
        $this->db->where('pengaduan.id_pelapor', $this->session->userdata('pelapor_id'));
		$this->db->where('pengaduan.dihapus', '0');
		$this->db->from('pengaduan');
		$this->db->join('kecamatan', 'id_kecamatan');
		$this->db->join('kelurahan', 'id_kelurahan');

        if($search != ''){
          $this->db->like('title', $search);
          $this->db->or_like('content', $search);
        }


        $query = $this->db->get();
        $result = $query->result_array();
     
        return $result[0]['allcount'];
    }

	public function get_pengaduan($rowno,$rowperpage,$search=""){
 
        $this->db->where('pengaduan.dihapus', '0');
        $this->db->where('pengaduan.id_pelapor', $this->session->userdata('pelapor_id'));
		$this->db->select('id_pengaduan, nama_kelurahan, nama_kecamatan, tgl, status_pmi, status_pengaduan as status, pengaduan.nama as nama, no_passport, id_pelapor');
		$this->db->where('pengaduan.dihapus', '0');
		$this->db->from('pengaduan');
		$this->db->join('kecamatan', 'id_kecamatan');
		$this->db->join('kelurahan', 'id_kelurahan');
		$this->db->order_by('pengaduan.id_pengaduan', 'DESC');


        if($search != ''){
          $this->db->like('title', $search);
          $this->db->or_like('content', $search);
        }

        $this->db->limit($rowperpage, $rowno); 
        $query = $this->db->get();
     
        return $query->result_array();
    }

	public function simpan($data, $uploadData = array()){
		$this->db->insert('pengaduan', $data);
		$this->db->insert_batch('lampiran',$uploadData);
	}

	public function update($where, $data){
    	$this->db->update('pengaduan', $data, $where);
    }

    public function update_file($where, $data, $uploadData = array()){
    	$this->db->update('pengaduan', $data, $where);
    	$this->db->insert_batch('lampiran',$uploadData);
    }


	public function hapus($id, $data){
        $this->db->where('id_pengaduan', $id);
        $this->db->update('pengaduan', $data);
    }

    public function restore($id, $data){
        $this->db->where('id', $id);
        $this->db->update('pengaduan', $data);
    }

    public function permanen($id){
        $this->db->where('id', $id);
        $this->db->delete('pengaduan');
    }

    public function get_trash(){
		$this->db->select('pengaduan.id as id_pengaduan, nama_kelurahan, nama_kecamatan, nama_pengadu, tgl, status, telp_pengadu, nama, no_passport');
		$this->db->where('pengaduan.dihapus', '1');
		$this->db->from('pengaduan');
		$this->db->join('kecamatan', 'id_kecamatan');
		$this->db->join('kelurahan', 'id_kelurahan');
		$this->db->join('pmi', 'pmi.id=pengaduan.id_pmi');
		$this->db->order_by('tgl', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_trash_operator($id_kecamatan, $id_kelurahan){
		$this->db->select('pengaduan.id as id_pengaduan, nama_kelurahan, nama_kecamatan, nama_pengadu, tgl, status, telp_pengadu, nama, no_passport');
		$this->db->where('pengaduan.dihapus', '1');
		$this->db->where('pengaduan.id_kecamatan', $id_kecamatan);
		$this->db->where('pengaduan.id_kelurahan', $id_kelurahan);
		$this->db->from('pengaduan');
		$this->db->join('kecamatan', 'id_kecamatan');
		$this->db->join('kelurahan', 'id_kelurahan');
		$this->db->join('pmi', 'pmi.id=pengaduan.id_pmi');
		$this->db->order_by('tgl', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}


	public function get_bukti_all($id){

		$this->db->where('id_pengaduan', $id);
		$this->db->from('lampiran');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_file_id($id){
		$this->db->where('id', $id);
        return $this->db->get('lampiran')->row();
	}

	public function hapus_file($id){
		$this->db->where('id', $id);
		$this->db->delete('lampiran');
	}

	public function terusan_tambah($data = array()){

		$total_array = count($data);
         
        if($total_array != 0){

            $hasil = $this->db->insert_batch('terusan', $data);
        }

	}

	public function get_laporan($tgl_mulai, $tgl_akhir){
		$this->db->where('pengaduan.dihapus', '0');
		$this->db->where('pengaduan.tgl >=', $tgl_mulai);
		$this->db->where('pengaduan.tgl <=', $tgl_akhir);
		$this->db->select('pengaduan.id as id_pengaduan, nama_kelurahan, nama_kecamatan, nama_pengadu, tgl, status, telp_pengadu, nama, no_passport, nm_pptkis, masalah, negara');
		$this->db->where('pengaduan.dihapus', '0');
		$this->db->from('pengaduan');
		$this->db->join('kecamatan', 'id_kecamatan');
		$this->db->join('kelurahan', 'id_kelurahan');
		$this->db->join('pmi', 'pmi.id=pengaduan.id_pmi');
		$this->db->order_by('pengaduan.tgl', 'DESC');
		return $this->db->get()->result();
	}

	public function get_laporan_operator($tgl_mulai, $tgl_akhir, $id_kecamatan, $id_kelurahan){
		$this->db->select('pengaduan.id as id_pengaduan, nama_kelurahan, nama_kecamatan, nama, no_passport, NIK, tgl, status');
		$this->db->where('dihapus', '0');
		$this->db->where('tgl >=', $tgl_mulai);
		$this->db->where('tgl <=', $tgl_akhir);
		$this->db->where('pengaduan.id_kecamatan', $id_kecamatan);
		$this->db->where('pengaduan.id_kelurahan', $id_kelurahan);
		$this->db->from('pengaduan');
		$this->db->join('kecamatan', 'id_kecamatan');
		$this->db->join('kelurahan', 'id_kelurahan');
		$this->db->order_by('tgl', 'ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_laporan_terusan($tgl_mulai, $tgl_akhir){

		$this->db->where('terusan.id_akses', $this->session->userdata('id_akses'));
		$this->db->where('pengaduan.dihapus', '0');
		$this->db->where('pengaduan.tgl >=', $tgl_mulai);
		$this->db->where('pengaduan.tgl <=', $tgl_akhir);
		$this->db->select('pengaduan.id as id_pengaduan, nama_kelurahan, nama_kecamatan, nama_pengadu, tgl, status, telp_pengadu, nama, no_passport, nm_pptkis, masalah, negara');
		$this->db->from('pengaduan');
		$this->db->join('kecamatan', 'id_kecamatan');
		$this->db->join('kelurahan', 'id_kelurahan');
		$this->db->join('pmi', 'pmi.id=pengaduan.id_pmi');
		$this->db->join('terusan', 'terusan.id_pengaduan=pengaduan.id');
		$this->db->order_by('tgl', 'ASC');
		$query = $this->db->get();
		return $query->result();
	}

}
