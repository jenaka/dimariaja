<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pelapor_m extends CI_Model{

    var $table = 'pelapor';


    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function check_email($email){
        $this->db->where('email',$email);
        return $this->db->get($this->table);
    }

    public function get_all(){
        $this->db->order_by('id_pelapor', 'DESC');
        $this->db->from($this->table);
        return $this->db->get()->result();
    }

    public function simpan($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function get_by_id($id){
        $this->db->from($this->table);
        $this->db->where('id_pelapor',$id);
        return $this->db->get()->row();
    }

    public function get_by_session(){
        $this->db->from($this->table);
        $this->db->where('id_pelapor',$this->session->userdata('pelapor_id'));
        return $this->db->get()->row();
    }

    public function get_by_activation($activation_code){
        $this->db->from($this->table);
        $this->db->where('kd_aktivasi',$activation_code);
        return $this->db->get();
    }

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete($id){
        $this->db->where('id_pelapor', $id);
        $this->db->delete($this->table);
    }

     public function aktivasi($id, $data){
        $this->db->where('id_pelapor', $id);
        $this->db->update($this->table, $data);
    }

    public function cek_activation($activation_code){
        $this->db->from('guest');
        $this->db->where('activation_code',$activation_code);
        return $this->db->get();
    }

    public function get_invoice_data($rowno,$rowperpage,$search="") {

        $this->db->select('orders.id_villa as id_villa, villas.slug as slug_villa, title, description, villas.base_price as base_price, featured_img, total_bedroom, total_bathroom');
        $this->db->select('location.name as name_location, location.slug as slug_location');
        $this->db->select('orders.check_in as check_in, orders.check_out as check_out, payment_status, night, total_amount, no_order');
        $this->db->where('id_pelapor', $this->session->userdata('guest_id'));
        $this->db->from('villas');
        $this->db->join('orders', 'orders.id_villa=villas.id_villa');
        $this->db->join('location', 'id_location');

        if($search != ''){
          $this->db->like('title', $search);
          $this->db->or_like('content', $search);
        }

        $this->db->limit($rowperpage, $rowno);
        $query = $this->db->get();

        return $query->result_array();
    }

      // Select total records
    public function total_invoice_villa($search = '') {

        $this->db->select('count(*) as allcount');
        $this->db->where('id_pelapor', $this->session->userdata('guest_id'));
        $this->db->from('villas');
        $this->db->join('orders', 'orders.id_villa=villas.id_villa');
        $this->db->join('location', 'id_location');

        if($search != ''){
          $this->db->like('title', $search);
          $this->db->or_like('content', $search);
        }


        $query = $this->db->get();
        $result = $query->result_array();

        return $result[0]['allcount'];
    }



}
