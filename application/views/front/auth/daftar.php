
<div class="hero-static content content-full invisible" data-toggle="appear">
    <!-- Sign In Form -->
    <div class="row px-5 mt-15">
        <div class="col-lg-8 pt-150 d-none d-sm-block">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <center><img src="<?= base_url(); ?>assets/img/register.png" style="width: 40%;"></center>
                    <h4 class="h5 font-w400 text-center mb-30 mt-5">Anda memiliki permasalahan ketika bekerja menjadi imigran? Ayo segera laporkan! hanya dimariaja.com</h4>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <form class="js-validation-signup" action="<?= base_url('daftar'); ?>" method="post">
                <div class="block block-rounded block-shadow">
                    <div class="block-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <h3 class="text-center font-w500 ">DAFTAR SEKARANG</h3>
                                <h4 class="h5 font-w400 text-center">Sudah punya akun? Silahkan <a href="<?= base_url(); ?>">login Dimariaja</a></h4>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="daftar-nama">Nama Lengkap</label>
                                <input type="text" class="form-control <?= form_error('daftar-nama') ? 'is-invalid' : '' ?>" id="daftar-nama" name="daftar-nama" placeholder="cth: Udin Sumarna">
                                <?php echo form_error('daftar-nama') ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="daftar-email">Email</label>
                                <input type="email" class="form-control <?= form_error('daftar-email') ? 'is-invalid' : '' ?>" id="daftar-email" name="daftar-email" placeholder="cth: john@example.com">
                                <?php echo form_error('daftar-email') ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="daftar-phone">No. Handphone</label>
                                <input type="text" class="form-control <?= form_error('daftar-phone') ? 'is-invalid' : '' ?>" id="daftar-phone" name="daftar-phone" placeholder="cth: 089656123214">
                                <?php echo form_error('daftar-phone') ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="daftar-password">Kata Sandi</label>
                                <input type="password" class="form-control <?= form_error('daftar-password') ? 'is-invalid' : '' ?>" id="daftar-password" name="daftar-password" placeholder="********">
                                <?php echo form_error('daftar-password') ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="daftar-password-confirm">Konfirmasi Kata Sandi</label>
                                <input type="password" class="form-control <?= form_error('daftar-knf_password') ? 'is-invalid' : '' ?>" id="daftar-knf_password" name="daftar-knf_password" placeholder="********">
                                <?php echo form_error('daftar-knf_password') ?>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-sm-12 push">
                                <button type="submit" class="btn btn-alt-primary btn-block">
                                    <i class="fa fa-plus mr-10"></i> Daftar Sekarang
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Sign In Form -->
</div>
<script src="<?= base_url(); ?>assets/js/pages/op_auth_signin.js"></script>
