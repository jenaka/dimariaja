<div class="content content-full">
    <div class="row justify-content-center py-0">
        <div class="col-lg-8">
            <div class="block block-rounded block-shadow">
                <div class="block-content">
                    <div class="row pt-10 pb-30">
                        <div class="col-lg-12">
                            <h3 class="h2 font-w700 text-center mb-10"><span class="text-primary">Reset Kata Sandi</span></h3>
                            <hr>
                            <div class="row nice-copy py-10 justify-content-center">
                                <div class="col-md-6 py-5">
                                    <form class="validasi-reset" action="<?= base_url('lupa_password');?>" method="post">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="lupa_email">Alamat Email</label>
                                                <input type="email" class="form-control" id="lupa-email <?= form_error('lupa-email') ? 'is-invalid' : '' ?>" name="lupa-email" placeholder="Masukan Alamat Email">
                                                <?php echo form_error('daftar-nama') ?>
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-alt-primary btn-block">
                                                <i class="fa fa-asterisk mr-10"></i> Reset Kata Sandi
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
