<div class="content content-full">
    <div class="row justify-content-center py-0">
        <div class="col-lg-8">
            <div class="block block-rounded block-shadow">
                <div class="block-content">
                    <div class="row pt-10 pb-30">
                        <div class="col-lg-12">
                            <h3 class="h2 font-w700 text-center mb-10"><span class="text-primary">Reset Password</span></h3>
                            <hr>
                            <div class="row nice-copy py-10 justify-content-center">
                                <div class="col-md-6 py-5">
                                  <form class="validasi-reset" action="<?= base_url('lupa_password/reset/'.$kode);?>" method="post">
                                    <div class="form-group row">
                                      <div class="col-12 my-2">
                                        <label for="lupa_pw">Password baru</label>
                                        <input type="password" class="form-control" id="lupa-pw <?= form_error('lupa-pw') ? 'is-invalid' : '' ?>" name="lupa-pw" placeholder="Masukan password baru">
                                        <?php echo form_error('lupa-pw') ?>
                                      </div>
                                      <div class="col-12 my-2">
                                        <label for="knf_lupa_pw">Konfirmasi password baru</label>
                                        <input type="password" class="form-control" id="knf-lupa-pw <?= form_error('knf-lupa-pw') ? 'is-invalid' : '' ?>" name="knf-lupa-pw" placeholder="Konfirmasi password baru">
                                        <?php echo form_error('knf-lupa-pw') ?>
                                      </div>
                                    </div>
                                    <div class="form-group text-center">
                                      <button type="submit" class="btn btn-alt-primary btn-block">
                                          <i class="fa fa-asterisk mr-10"></i> Reset Password
                                      </button>
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
