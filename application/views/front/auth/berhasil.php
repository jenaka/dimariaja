<div class="content content-full">
    <div class="row justify-content-center py-0">
        <div class="col-lg-12">
            <div class="block block-rounded block-shadow">
                <div class="block-content">
                    <div class="row pt-10 pb-50">
                        <div class="col-lg-12">
                            <center>
                                <div class="sa">
                                    <div class="sa-success">
                                        <div class="sa-success-tip"></div>
                                        <div class="sa-success-long"></div>
                                        <div class="sa-success-placeholder"></div>
                                        <div class="sa-success-fix"></div>
                                    </div>
                                </div>
                            </center>
                            <h3 class="h2 font-w700 text-center mb-10"><span class="text-success">Pendaftaran Berhasil</span></h3>
                            <hr>
                            <div class="row nice-copy py-10 justify-content-center">
                                <div class="col-md-6 py-20">
                                    <p class="mb-0 text-center">Kami telah mengirimkan email ke <?= $email; ?>. Silahkan cek email untuk melakukan aktivasi akun anda.</p>
                                    <!-- <p class="mb-0 text-center">Apabila belum menerima email aktivasi, silahkan klik tombol dibawah ini untuk pengirimin ulang.</p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
