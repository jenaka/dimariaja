<div class="bg-gd-sea">
    <div class="bg-sea">
        <div class="content text-center">
            <div class="pt-50 pb-20">
                <h1 class="font-w700 mb-10 text-white"><?= $page_title; ?></h1>
                <h2 class="h4 font-w400 text-white"><?= $sub_title; ?></h2>
            </div>
        </div>
    </div>
</div>
<div class="content content-full">
    <div class="row justify-content-center py-30">
        <div class="col-lg-8 col-xl-6">
            <form class="js-validation-be-contact" action="<?php echo base_url('kontak/send') ?>" method="post">
                <div class="form-group row">
                    <div class="col-12">
                        <label for="be-contact-name">Nama Lengkap*</label>
                        <input type="text" class="form-control form-control-lg" name="nama" required placeholder="Masukkan Nama Lengkap..">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12" for="be-contact-email">Alamat Email*</label>
                    <div class="col-12">
                        <input type="email" class="form-control form-control-lg" name="email" required placeholder="Masukkan Alamat Email..">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12" for="be-contact-email">Subjek*</label>
                    <div class="col-12">
                        <input type="text" class="form-control form-control-lg" name="subjek" required placeholder="Masukkan Subjek..">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12" for="be-contact-message">Pesan*</label>
                    <div class="col-12">
                        <textarea class="form-control form-control-lg"  name="pesan" rows="5" required placeholder="Masukkan Pesan.."></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-hero btn-alt-primary min-width-175">
                            <i class="fa fa-send mr-5"></i> Kirim Pesan
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
