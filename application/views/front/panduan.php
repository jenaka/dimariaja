<div class="bg-gd-sea">
    <div class="bg-sea">
        <div class="content text-center">
            <div class="pt-50 pb-20">
                <h1 class="font-w700 mb-10 text-white"><?= $page_title; ?></h1>
                <h2 class="h4 font-w400 text-white"><?= $sub_title; ?></h2>
            </div>
        </div>
    </div>
</div>
<div class="content content-full">
    <div class="row row-deck items-push">
        <div class="col-md-6 col-xl-4">
            <a class="block block-link-shadow block-rounded ribbon ribbon-bookmark ribbon-left ribbon-success text-center" href="<?= base_url(); ?>">
                <div class="block-content block-content-full">
                    <div class="item item-circle bg-gd-sea text-pulse-lighter mx-auto my-20">
                        <i class="si si-user-follow text-white"></i>
                    </div>
                </div>
                <div class="block-content block-content-full block-content-sm bg-body-light">
                    <div class="font-size-xl text-muted">Panduan</div>
                </div>
                <div class="block-content block-content-full">
                    <div class="h4 text-muted font-w600">Mendaftar</div>
                </div>
            </a>
        </div>
        <div class="col-md-6 col-xl-4">
            <a class="block block-link-shadow block-rounded ribbon ribbon-bookmark ribbon-left ribbon-success text-center" href="<?= base_url(); ?>">
                <div class="block-content block-content-full">
                    <div class="item item-circle bg-gd-sea text-pulse-lighter mx-auto my-20">
                        <i class="si si-note text-white"></i>
                    </div>
                </div>
                <div class="block-content block-content-full block-content-sm bg-body-light">
                    <div class="font-size-xl text-muted">Panduan</div>
                </div>
                <div class="block-content block-content-full">
                    <div class="h4 text-muted font-w600">Membuat Laporan</div>
                </div>
            </a>
        </div>
        <div class="col-md-6 col-xl-4">
            <a class="block block-link-shadow block-rounded ribbon ribbon-bookmark ribbon-left ribbon-success text-center" href="<?= base_url(); ?>">
                <div class="block-content block-content-full">
                    <div class="item item-circle bg-gd-sea text-pulse-lighter mx-auto my-20">
                        <i class="si si-reload text-white"></i>
                    </div>
                </div>
                <div class="block-content block-content-full block-content-sm bg-body-light">
                    <div class="font-size-xl text-muted">Panduan</div>
                </div>
                <div class="block-content block-content-full">
                    <div class="h4 text-muted font-w600">Lupa Kata Sandi</div>
                </div>
            </a>
        </div>
    </div>
</div>
