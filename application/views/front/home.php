<div class="content content-full">
    <div class="row justify-content-center py-0">
    	<div class="col-lg-8">
    		<div class="js-slider slick-dotted-inner slick-dotted-white" data-dots="true" data-autoplay="true" data-autoplay-speed="3000">
                <div>
                    <img class="img-fluid" src="<?= base_url(); ?>assets/img/slides/slide_1.png" alt="">
                </div>
                <div>
                    <img class="img-fluid" src="<?= base_url(); ?>assets/img/slides/slide_2.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="col-lg-4">
        	<form class="js-validation-signin" action="<?= base_url('home'); ?>" method="post">
                <div class="block block-rounded block-shadow">
                    <!-- <div class="block-header">

                    </div> -->
                    <div class="block-content">
                        <div class="form-group row mb-0">
                            <div class="col-12">
                                <h3 class="block-title text-center"><b>MASUK SEKARANG</b></h3>
                                <p class="nice-copy text-center mb-10">Punya masalah ketika bekerja diluar negeri? Bingung lapor kemana? <br>Lapor dimari aja!<br>
                                    Belum punya akun? Silahkan <a href="<?= base_url('daftar'); ?>"><b>daftar dimari</b></a>
                                </p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="login-email">Alamat Email</label>
                                <input type="email" class="form-control <?= form_error('login-email') ? 'is-invalid' : '' ?>" id="login-email" name="login-email" placeholder="Masukan Alamat Email">
                                <?php echo form_error('login-email') ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="login-password">Kata Sandi</label>
                                <input type="password" class="form-control <?= form_error('login-password') ? 'is-invalid' : '' ?>" id="login-password" name="login-password" placeholder="Masukan Kata Sandi">
                                <?php echo form_error('login-password') ?>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-alt-primary btn-block">
                                    <i class="si si-login mr-10"></i> Login
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="block-content bg-body-light">
                        <div class="form-group text-center">
                            <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="<?= base_url('lupa_password'); ?>">
                                <i class="fa fa-warning mr-5"></i> Lupa Kata Sandi ?
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="block block-rounded block-shadow">
                <div class="block-content">
                    <div class="row pt-10 pb-15">
                        <div class="col-lg-12">
                            <h3 class="h3 font-w700 text-center">
                                Alur Pengaduan <span class="text-primary">Permasalahan</span>
                            </h3>
                            <hr>
                            <div class="row text-center">
                                <div class="col-4 col-md-2 py-30 mx-15 invisible" data-toggle="appear" data-class="animated bounceIn">
                                    <div class="item item-circle mx-auto my-15">
                                        <img src="<?= base_url(); ?>assets/img/daftar.png" width="100px">
                                    </div>
                                    <div class="font-w600">1. Buat Akun</div>
                                </div>
                                <div class="col-4 col-md-2 py-30 mx-15 invisible" data-toggle="appear" data-class="animated bounceIn" data-timeout="150">
                                    <div class="item item-circle mx-auto my-15">
                                        <img src="<?= base_url(); ?>assets/img/confirm.png" width="100px">
                                    </div>
                                    <div class="font-w600">2. Aktivasi</div>
                                </div>
                                <div class="col-4 col-md-2 py-30 mx-15 invisible" data-toggle="appear" data-class="animated bounceIn" data-timeout="300">
                                    <div class="item item-circle mx-auto my-15">
                                        <img src="<?= base_url(); ?>assets/img/login.png" width="100px">
                                    </div>
                                    <div class="font-w600">3. Login</div>
                                </div>
                                <div class="col-4 col-md-2 py-30 mx-15 invisible" data-toggle="appear" data-class="animated bounceIn" data-timeout="450">
                                    <div class="item item-circle mx-auto my-15">
                                        <img src="<?= base_url(); ?>assets/img/pengaduan.png" width="100px">
                                    </div>
                                    <div class="font-w600">4. Pengaduan</div>
                                </div>
                                <div class="col-4 col-md-2 py-30 mx-15 invisible" data-toggle="appear" data-class="animated bounceIn" data-timeout="600">
                                    <div class="item item-circle mx-auto my-15">
                                        <img src="<?= base_url(); ?>assets/img/status.png" width="70px">
                                    </div>
                                    <div class="font-w600">5. Status Pengaduan</div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
