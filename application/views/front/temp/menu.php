<!-- Side Header -->
    <div class="content-header content-header-fullrow">
        <div class="content-header-section text-center align-parent">
            <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                <i class="fa fa-times text-danger"></i>
            </button>
            <!-- END Close Sidebar -->

            <!-- Logo -->
            <div class="content-header-item">
                <a class="font-w700" href="<?= base_url(); ?>">
                    <img src="<?= base_url('assets/img/logo_ver.png'); ?>" width="130px">
                </a>
            </div>
            <!-- END Logo -->
        </div>
    </div>
    <!-- END Side Header -->

    <!-- Side Main Navigation -->
    <div class="content-side content-side-full">
        <ul class="nav-main">
            <li>
                <a href="<?= base_url() ?>"><i class="si si-home"></i>Home</a>
            </li>
            <li>
                <a href="<?= base_url('panduan') ?>"><i class="si si-note"></i>Panduan</a>
            </li>
            <li>
                <a href="<?= base_url('kontak'); ?>"><i class="si si-envelope"></i></i>Kontak</a>
            </li>
            
        </ul>
    </div>
    <!-- END Side Main Navigation -->