<!-- Left Section -->
<div class="content-header-section">
    <!-- Logo -->
    <div class="content-header-item">
        <a class="font-w700" href="<?= base_url(); ?>">
            <img src="<?= base_url('assets/img/logo_ver.png'); ?>" width="200px">
        </a>
    </div>
    <!-- END Logo -->
</div>
<!-- END Left Section -->

<!-- Right Section -->
<div class="content-header-section">
    <ul class="nav-main-header mr-15">
        <li>
            <a href="<?= base_url() ?>"><i class="si si-home"></i>Home</a>
        </li>
        <li>
            <a href="<?= base_url('panduan') ?>"><i class="si si-note"></i>Panduan</a>
        </li>
        <li>
            <a href="<?= base_url('kontak'); ?>"><i class="si si-envelope"></i></i>Kontak</a>
        </li>
        <?php if($this->session->userdata('pelapor_login') !== 1){ ?>
        <li>
            <a href="<?= base_url('daftar'); ?>" class="btn btn-alt-primary"><i class="si si-user-follow mr-10"></i>Daftar</a>
        </li>
        <?php } ?>
    </ul>
    <?php if($this->session->userdata('pelapor_login') == 1){ ?>
        <div class="btn-group" role="group">
            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo $this->session->userdata('pelapor_nama'); ?><i class="fa fa-angle-down ml-5"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right min-width-150" aria-labelledby="page-header-user-dropdown">
                <a class="dropdown-item" href="<?= base_url('pengaduan/tambah'); ?>" data-toggle="layout" data-action="side_overlay_toggle">
                    <i class="si si-note mr-5"></i> Buat Laporan Pengaduan
                </a>
                <a class="dropdown-item" href="<?= base_url('pengaduan'); ?>" data-toggle="layout" data-action="side_overlay_toggle">
                    <i class="si si-list mr-5"></i> Laporan Pengaduan Saya
                </a>
                <a class="dropdown-item" href="<?= base_url('pengaturan'); ?>" data-toggle="layout" data-action="side_overlay_toggle">
                    <i class="si si-wrench mr-5"></i> Pengaturan
                </a>
                <a class="dropdown-item" href="<?= base_url('ubah-kata-sandi'); ?>" data-toggle="layout" data-action="side_overlay_toggle">
                    <i class="si si-lock mr-5"></i> Ganti Password
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?= base_url('logout'); ?>">
                    <i class="si si-logout mr-5"></i> Keluar
                </a>
            </div>
        </div>
    <?php } ?>
    <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none" data-toggle="layout" data-action="sidebar_toggle">
        <i class="fa fa-navicon"></i>
    </button>
</div>