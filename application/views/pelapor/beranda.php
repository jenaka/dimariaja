<div class="js-slider slick-dotted-inner slick-dotted-white" data-dots="true" data-autoplay="true" data-autoplay-speed="3000">
    <div>
        <img class="img-fluid" src="<?= base_url(); ?>assets/img/slides/slide_1.png" alt="">
    </div>
    <div>
        <img class="img-fluid" src="<?= base_url(); ?>assets/img/slides/slide_2.jpg" alt="">
    </div>
</div>
