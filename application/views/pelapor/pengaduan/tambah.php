<div class="js-wizard-validation-classic block">
    <!-- Step Tabs -->
    <ul class="nav nav-tabs nav-tabs-block nav-fill" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" href="#wizard-validation-classic-step1" data-toggle="tab">1. Identitas PMI</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#wizard-validation-classic-step2" data-toggle="tab">2. PPTKIS & Agency</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#wizard-validation-classic-step3" data-toggle="tab">3. Pengguna Jasa</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#wizard-validation-classic-step4" data-toggle="tab">3. Tuntutan & Permasalahan</a>
        </li>
    </ul>
    <!-- END Step Tabs -->

    <!-- Form -->
    <form class="js-wizard-validation-classic-form" action="<?= base_url('pengaduan/simpan'); ?>" method="post" enctype="multipart/form-data">
        <!-- Steps Content -->
        <div class="block-content block-content-full tab-content" style="min-height: 265px;">
            <!-- Step 1 -->
            <div class="tab-pane active" id="wizard-validation-classic-step1" role="tabpanel">
                <!-- <input type="hidden" name="id_pengaduan" value="<?= $id_pengaduan; ?>"> -->
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="nama_lengkap">Nama Lengkap</label>
                    <div class="col-lg-8">
                    <input type="text" id="nama" class="form-control <?php if(form_error('nama') !== ''){ echo 'is-invalid'; } ?>" id="nama" name="nama" value="<?php echo $nama; ?>"  placeholder="Masukan Nama Lengkap">
                        <?php echo form_error('nama') ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Tempat Lahir</label>
                    <div class="col-lg-8">
                    <input type="text" id="tmp_lahir" class="form-control <?php if(form_error('tmp_lahir') !== ''){ echo 'is-invalid'; } ?>" id="tmp_lahir" name="tmp_lahir" value="<?php echo $tmp_lahir; ?>"  placeholder="Masukan Tempat Lahir">
                        <div class="form-text text-danger"><?php echo form_error('tmp_lahir') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Tanggal Lahir</label>
                    <div class="col-lg-8">
                    <input type="text" id="tgl_lahir" class="js-datepicker form-control <?php if(form_error('tgl_lahir') !== ''){ echo 'is-invalid'; } ?>" name="tgl_lahir" value="<?php echo $tgl_lahir; ?>" placeholder="Masukan Umur (cth. 20-10-1992)" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy">
                        <div class="form-text text-danger"><?php echo form_error('tgl_lahir') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Jenis Kelamin</label>
                    <div class="col-lg-8">
                    <select class="form-control <?php if(form_error('jk') !== ''){ echo 'is-invalid'; } ?>" name="jk" id="jk">
                        <option value="">Pilih</option>
                        <option value="L">Laki-Laki</option>
                        <option value="P">Perempuan</option>
                    </select>
                        <div class="form-text text-danger"><?php echo form_error('jk') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Nomor Passport</label>
                    <div class="col-lg-8">
                       <input type="text" id="no_passport" class="form-control <?php if(form_error('no_passport') !== ''){ echo 'is-invalid'; } ?>" name="no_passport" value="<?php echo $no_passport; ?>"  placeholder="Masukan Nomor Passport">
                       <div class="form-text text-danger"><?php echo form_error('no_passport') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Kecamatan</label>
                    <div class="col-lg-8">
                    <select class="form-control <?php if(form_error('id_kecamatan') !== ''){ echo 'is-invalid'; } ?>" name="id_kecamatan" id="id_kecamatan">  
                        <option value="">Pilih</option>
                            <?php
                            foreach ($Kecamatan_data as $kec) {
                                ?>
                                <!--di sini kita tambahkan class berisi id provinsi-->
                                <option <?php echo $id_kecamatan == $kec->id_kecamatan ? 'selected="selected"' : '' ?> 
                                    class="<?php echo $kec->id_kecamatan ?>" value="<?php echo $kec->id_kecamatan ?>"><?php echo $kec->nama_kecamatan ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <div class="form-text text-danger"><?php echo form_error('id_kecamatan') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Kelurahan</label>
                    <div class="col-lg-8">
                        <select class="form-control <?php if(form_error('id_kelurahan') !== ''){ echo 'is-invalid'; } ?>" name="id_kelurahan" id="id_kelurahan">
                            <option value="">Pilih</option>
                                <?php
                                foreach ($kelurahan_data as $kel) {
                                    ?>
                                    <option <?php echo $id_kelurahan == $kel->id_kelurahan ? 'selected="selected"' : '' ?><?php echo $kelurahan_selected == $kel->id_kecamatan ? 'selected="selected"' : '' ?> 
                                        class="<?php echo $kel->id_kecamatan ?>" value="<?php echo $kel->id_kelurahan ?>"><?php echo $kel->nama_kelurahan ?></option>
                                    <?php
                                }
                                ?>
                        </select>
                        <div class="form-text text-danger"><?php echo form_error('id_kelurahan') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Alamat Asal</label>
                    <div class="col-lg-8">
                        <input type="text" id="alamat_asal" class="form-control <?php if(form_error('alamat_asal') !== ''){ echo 'is-invalid'; } ?>" name="alamat_asal" value="<?php echo $alamat_asal; ?>"  placeholder="Alamat Jalan / Dusun">
                        <div class="form-text text-danger"><?php echo form_error('alamat_asal') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Alamat Kerja Lengkap</label>
                    <div class="col-lg-8">
                    <textarea class="form-control <?php if(form_error('alamat_kerja') !== ''){ echo 'is-invalid'; } ?>" name="alamat_kerja" id="alamat_kerja" rows="2" placeholder="Alamat Kerja Lengkap"><?php $alamat_kerja;?></textarea>
                    <div class="form-text text-danger"><?php echo form_error('alamat_kerja') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Embarkasi</label>
                    <div class="col-lg-8">
                        <input type="text" id="embarsi" class="form-control <?php if(form_error('embarsi') !== ''){ echo 'is-invalid'; } ?>" name="embarsi" value="<?php echo $embarsi; ?>"  placeholder="Embarsi">
                        <div class="form-text text-danger"><?php echo form_error('embarsi') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Debarkasi</label>
                    <div class="col-lg-8">
                        <input type="text" id="debarsi" class="form-control <?php if(form_error('debarsi') !== ''){ echo 'is-invalid'; } ?>" name="debarsi" value="<?php echo $debarsi; ?>"  placeholder="Debarsi">
                        <div class="form-text text-danger"><?php echo form_error('debarsi') ?></div>
                    </div>
                </div>
            </div>
            <!-- END Step 1 -->

            <!-- Step 2 -->
            <div class="tab-pane" id="wizard-validation-classic-step2" role="tabpanel">
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Nama PPTKIS</label>
                    <div class="col-lg-8">
                    <input type="text" id="nm_pptkis" class="form-control <?php if(form_error('nm_pptkis') !== ''){ echo 'is-invalid'; } ?>" name="nm_pptkis" value="<?php echo $nm_pptkis; ?>"  placeholder="Masukan Nama PPTKIS">
                        <div class="form-text text-danger"><?php echo form_error('nm_pptkis') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Negara Penempatan</label>
                    <div class="col-lg-8">
                       <input type="text" id="negara" class="form-control <?php if(form_error('negara') !== ''){ echo 'is-invalid'; } ?>" name="negara" value="<?php echo $negara; ?>"  placeholder="Masukan Negara Penempatan">
                       <div class="form-text text-danger"><?php echo form_error('negara') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Mulai Bekerja Di Luar Negeri</label>
                    <div class="col-lg-8">
                       <input type="text" class="js-datepicker form-control <?php if(form_error('mulai_kerja') !== ''){ echo 'is-invalid'; } ?>" id="mulai_kerja" name="mulai_kerja" placeholder="Tanggal Mulai Bekerja Di Luar Negeri" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy">
                       <div class="form-text text-danger"><?php echo form_error('mulai_kerja') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Tanggal Kembali Ke Daerah Asal</label>
                    <div class="col-lg-8">
                       <input type="text" class="js-datepicker form-control <?php if(form_error('akhir_kerja') !== ''){ echo 'is-invalid'; } ?>" id="akhir_kerja" name="akhir_kerja"  placeholder="Tanggal Kepulangan Ke Daerah Asal" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy">
                       <div class="form-text text-danger"><?php echo form_error('akhir_kerja') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Lama Bekerja</label>
                    <div class="col-lg-8">
                       <input type="text" id="lama_kerja" class="form-control <?php if(form_error('lama_kerja') !== ''){ echo 'is-invalid'; } ?>" name="lama_kerja" value="<?php echo $lama_kerja; ?>"  placeholder="Masukan Lama Bekerja">
                       <div class="form-text text-danger"><?php echo form_error('lama_kerja') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Nama Agency</label>
                    <div class="col-lg-8">
                    <input type="text" id="nm_agency" class="form-control <?php if(form_error('nm_agency') !== ''){ echo 'is-invalid'; } ?>" name="nm_agency" value="<?php echo $nm_agency; ?>"  placeholder="Masukan Nama Agency">
                        <div class="form-text text-danger"><?php echo form_error('nm_agency') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Alamat Agency</label>
                    <div class="col-lg-8">
                    <textarea class="form-control <?php if(form_error('alamat_agency') !== ''){ echo 'is-invalid'; } ?>" name="alamat_agency" rows="2" placeholder="Alamat Agency"><?php $alamat_agency;?></textarea>
                    <div class="form-text text-danger"><?php echo form_error('alamat_agency') ?></div>
                    </div>
                </div>
            </div>
            <!-- END Step 2 -->

            <!-- Step 3 -->
            <div class="tab-pane" id="wizard-validation-classic-step3" role="tabpanel">
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Nama Awal Majikan</label>
                    <div class="col-lg-8">
                    <input type="text" id="f_nama_mjk" class="form-control <?php if(form_error('f_nama_mjk') !== ''){ echo 'is-invalid'; } ?>" name="f_nama_mjk" value="<?php echo $f_nama_mjk; ?>"  placeholder="Masukan Nama Awal Majikan">
                        <div class="form-text text-danger"><?php echo form_error('f_nama_mjk') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Nama Akhir Majikan</label>
                    <div class="col-lg-8">
                    <input type="text" id="l_nama_mjk" class="form-control <?php if(form_error('l_nama_mjk') !== ''){ echo 'is-invalid'; } ?>" name="l_nama_mjk" value="<?php echo $l_nama_mjk; ?>"  placeholder="Masukan Nama Akhir Majikan">
                        <div class="form-text text-danger"><?php echo form_error('l_nama_mjk') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Jumlah Majikan</label>
                    <div class="col-lg-8">
                    <input type="text" id="jml_mjk" class="form-control <?php if(form_error('jml_mjk') !== ''){ echo 'is-invalid'; } ?>" name="jml_mjk" value="<?php echo $jml_mjk; ?>"  placeholder="Masukan Jumlah Majikan">
                        <div class="form-text text-danger"><?php echo form_error('jml_mjk') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Nama Badan Usaha</label>
                    <div class="col-lg-8">
                    <input type="text" id="badan_usaha" class="form-control <?php if(form_error('badan_usaha') !== ''){ echo 'is-invalid'; } ?>" name="badan_usaha" value="<?php echo $badan_usaha; ?>"  placeholder="Masukan Nama Badan Usaha">
                        <div class="form-text text-danger"><?php echo form_error('badan_usaha') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Alamat</label>
                    <div class="col-lg-8">
                    <input type="text" id="alamat_mjk" class="form-control <?php if(form_error('alamat_mjk') !== ''){ echo 'is-invalid'; } ?>" name="alamat_mjk" value="<?php echo $alamat_mjk; ?>"  placeholder="Masukan Alamat Majikan">
                        <div class="form-text text-danger"><?php echo form_error('alamat_mjk') ?></div>
                    </div>
                </div>
            </div>
            <!-- END Step 3 -->

            <!-- Step 4 -->
            <div class="tab-pane" id="wizard-validation-classic-step4" role="tabpanel">
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Masalah Yang Dilaporkan</label>
                    <div class="col-lg-8">
                       <textarea class="form-control  <?php if(form_error('alamat_mjk') !== ''){ echo 'is-invalid'; } ?>" name="masalah" rows="10" id="masalah" placeholder="Masukan Masalah Yang Dilaporkan"><?php $masalah;?></textarea>
                       <div class="form-text text-danger"><?php echo form_error('masalah') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Tuntutan</label>
                    <div class="col-lg-8">
                       <textarea class="form-control <?php if(form_error('alamat_mjk') !== ''){ echo 'is-invalid'; } ?>" name="tuntutan" rows="10" id="tuntutan" placeholder="Tuntutan"><?php $keterangan;?></textarea>
                        <div class="form-text text-danger"><?php echo form_error('tuntutan') ?></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" >Upload Berkas Lampiran</label>
                    <div class="col-lg-8">
                    <input type="file" id="lampiran" class="form-control <?php if(form_error('lampiran') !== ''){ echo 'is-invalid'; } ?>" name="lampiran[]" multiple>
                    <div class="form-text text-danger"><?php echo form_error('lampiran') ?></div>
                    </div>
                </div>
            </div>
            <!-- END Step 4 -->
        </div>
        <!-- END Steps Content -->

        <!-- Steps Navigation -->
        <div class="block-content block-content-sm block-content-full bg-body-light">
            <div class="row">
                <div class="col-6">
                    <button type="button" class="btn btn-alt-secondary" data-wizard="prev">
                        <i class="fa fa-angle-left mr-5"></i> Sebelumnya
                    </button>
                </div>
                <div class="col-6 text-right">
                    <button type="button" class="btn btn-alt-secondary" data-wizard="next">
                        Selanjutnya <i class="fa fa-angle-right ml-5"></i>
                    </button>
                    <button type="submit" class="btn btn-alt-primary d-none" data-wizard="finish">
                        <i class="fa fa-check mr-5"></i> Simpan
                    </button>
                </div>
            </div>
        </div>
        <!-- END Steps Navigation -->
    </form>
    <!-- END Form -->
</div>