<div class="listings-container compact-list-layout margin-top-35">
	<?php if(!empty($pengaduan_data)): foreach($pengaduan_data as $data): ?>
	<a href="<?= base_url('pengaduan/detail/'.$data['id_pengaduan']); ?>" class="job-listing">
		<!-- Job Listing Details -->
		<div class="job-listing-details">
			<!-- Details -->
			<div class="job-listing-description">
				<h3 class="job-listing-title">#<?= $data['id_pengaduan']; ?></h3>

				<!-- Job Listing Footer -->
				<div class="job-listing-footer">
					<ul>
						<?php if($data['status'] == '0'){
                        	echo '<li class="mr-4"><span class="badge badge-danger" href="javascript:void(0)">Proses</span></li>';
                        }else{
                            $this->db->where('id_pengaduan', $data['id_pengaduan']);
                            $this->db->from('terusan');
                            $this->db->join('user_akses', 'user_akses.id=terusan.id_akses');
                            $query = $this->db->get()->result();
                            foreach ($query as $t){
                                echo '<li class="mr-5"><span class="badge badge-success">'.$t->nama.' </span></li>';
                            }
                        }?>
					</ul>
				</div>
			</div>

			<!-- Bookmark -->
			<span class="bookmark-icon"><i class="si si-calendar"></i> <?= date("d-m-Y", strtotime($data['tgl'])) ?></span>
		</div>
	</a>
	<?php endforeach; else: ?>
		<div class="px-15 py-15">
	        <center><img src="<?= base_url(); ?>assets/img/no_report.png" width="250px">
	        	<h3 class="text-muted">Anda Belum Membuat Laporan Pengaduan</h3>
	        	<a class="btn btn-primary text-white" href="<?= base_url('pengaduan/tambah'); ?>"><i class="si si-note mr-5"></i> Buat Laporan Pengaduan Sekarang!</a>
	        </center>
	    </div>
    <?php endif; ?>
</div>