<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title font-w700">#<?= $id; ?></h3>
    </div>
    <div class="block block-rounded">
        <div class="block-content">
        	<table class="table table-striped table-vcenter" style="width: 100%;">
                <thead>
                    <tr>
                        <th colspan="3">IDENTITAS PMI
                            <?php if($status_pmi == '0'){ ?>
                                <span class="badge badge-danger" href="javascript:void(0)">Belum Terverifikasi</span>
                            </button>
                            <?php }else{ ?>
                               <span class="badge badge-success" href="javascript:void(0)">Sudah Terverifikasi</span>
                            <?php } ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    	<td width="40%">Nama Lengkap</td>
                    	<td>: <?= $nama; ?></td>
                    </tr>
                    <tr>
                    	<td>Tempat / Tanggal Lahir</td>
                    	<td>: <?= $tmp_lahir.' / '.$tgl_lahir; ?> Tahun</td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>: <?= $jk; ?></td>
                    </tr>
                    <tr>
                    	<td>No. Passport</td>
                    	<td>: <?= $no_passport; ?></td>
                    </tr>
                    <tr>
                    	<td>Alamat Asal</td>
                    	<td>: <?= $alamat_asal; ?> <br> Kec.<?= $kec_pmi; ?> Kel. <?= $kel_pmi; ?> Kabupaten Bandung Barat</td>
                    </tr>
                    <tr>
                    	<td>Alamat Kerja</td>
                    	<td>: <?= $alamat_kerja; ?></td>
                    </tr>
                    <tr>
                    	<td>Embarkasi / Debarkasi</td>
                    	<td>: <?= $embarsi.' / '.$debarsi; ?></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-vcenter" style="width: 100%;">
                <thead>
                    <tr>
                        <th colspan="3">PPTKIS Yang Menempatkan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    	<td width="40%">Nama</td>
                    	<td>: <?= $nm_pptkis; ?></td>
                    </tr>
                    <tr>
                    	<td>Negara Penempatan</td>
                    	<td>: <?= $negara; ?></td>
                    </tr>
                    <tr>
                    	<td>Mulai Bekerka di Luar Negeri</td>
                    	<td>: <?= $mulai_kerja; ?></td>
                    </tr>
                    <tr>
                    	<td>Tanggal Kembali Ke Daerah Asal</td>
                    	<td>: <?= $akhir_kerja; ?></td>
                    </tr>
                    <tr>
                    	<td>Lama Bekerja</td>
                    	<td>: <?= $lama_kerja; ?></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-vcenter" style="width: 100%;">
                <thead>
                    <tr>
                        <th colspan="3">Agency</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    	<td width="40%">Nama Agency</td>
                    	<td>: <?= $nm_agency; ?></td>
                    </tr>
                    <tr>
                    	<td>Alamat</td>
                    	<td>: <?= $alamat_agency; ?></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-vcenter" style="width: 100%;">
                <thead>
                    <tr>
                        <th colspan="3">Pengguna Jasa TKI</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    	<td width="15%">Nama Majikan</td>
                    	<td width="20%">: a. Awal</td>
                    	<td>: <?= $f_nama_mjk; ?></td>
                    </tr>
                    <tr>
                    	<td></td>
                    	<td width="20%">: b. Akhir</td>
                        <td>: <?= $l_nama_mjk; ?></td>
                    </tr>
                    <tr>
                    	<td></td>
                    	<td width="20%">: c. Jumlah Majikan</td>
                        <td>: <?= $jml_mjk; ?> Orang</td>
                    </tr>
                    <tr>
                    	<td width="40%">Nama Badan usaha</td>
                    	<td colspan="2">: <?= $badan_usaha;?></td>
                    </tr>
                    <tr>
                    	<td width="40%">Alamat</td>
                    	<td colspan="2">: <?= $alamat_mjk; ?></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-vcenter" style="width: 100%;">
                <thead>
                    <tr>
                        <th>MASALAH YANG DILAPORKAN</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $masalah; ?></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-vcenter" style="width: 100%;">
                <thead>
                    <tr>
                        <th>TUNTUTAN</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $tuntutan; ?></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-vcenter" style="width: 100%;">
                <thead>
                    <tr>
                        <th colspan="3">Berkas Yang Dilampiran</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($bukti_pengaduan as $u) { ?>
                    <tr>
                        <td class="text-center" scope="row" width="10%"><?= $no++; ?></td>
                        <td><?= $u->path; ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="<?= base_url('uploads/pengaduan/'.$u->path); ?>" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Download">
                                    <i class="fa fa-download"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Default Elements -->
</div>
