<ul class="nav nav-pills flex-column push">
    <li class="nav-item">
        <a class="nav-link d-flex align-items-center justify-content-between" href="<?= base_url('pengaduan/tambah');?>">
            <span><i class="si si-fw si-note mr-5"></i> Buat Laporan Pengaduan</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link d-flex align-items-center justify-content-between" href="<?= base_url('pengaduan');?>">
            <span><i class="si si-fw si-list mr-5"></i> Laporan Pengaduan Saya</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link d-flex align-items-center justify-content-between" href="<?= base_url('pengaturan');?>">
            <span><i class="si si-fw si-wrench mr-5"></i> Pengaturan</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link d-flex align-items-center justify-content-between" href="<?= base_url('pengaturan#ubah-kata-sandi');?>">
            <span><i class="fa fa-fw fa-lock mr-5"></i> Ubah Kata Sandi</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link d-flex align-items-center justify-content-between" href="<?= base_url('logout');?>">
            <span><i class="si si-fw si-logout mr-5"></i> Keluar</span>
        </a>
    </li>
</ul>
