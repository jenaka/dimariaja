<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Dimariaja - Tempat Pengaduan Kasus Imigran Indonesia</title>

        <meta name="description" content="Dimariaja - Tempat Pengaduan Kasus Imigran Indonesia">
        <meta name="author" content="dimariaja">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Dimariaja - Tempat Pengaduan Kasus Imigran Indonesia">
        <meta property="og:site_name" content="Dimariaja">
        <meta property="og:description" content="Dimariaja - Tempat Pengaduan Kasus Imigran Indonesia">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <link rel="shortcut icon" href="<?= base_url(); ?>assets/img/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url(); ?>assets/img/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <link rel="stylesheet" id="css-main" href="<?= base_url(); ?>assets/css/codebase.css">
        <link rel="stylesheet" id="css-main" href="<?= base_url(); ?>assets/css/success_icon.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/js/plugins/select2/select2.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/js/plugins/slick/slick.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/js/plugins/slick/slick-theme.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">

        <!-- Codebase Core JS -->
        <script src="<?= base_url(); ?>assets/js/core/jquery.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/core/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    </head>
    <body>
        <div id="page-loader" class="show"></div>
        <div id="page-container" class="sidebar side-scroll page-header-fixed page-header main-content-boxed">
            <!-- Sidebar -->
            <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <div class="sidebar-content">
                        <?= $menu; ?>
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="page-header">
                <!-- Header Content -->
                <div class="content-header">
                    <?= $menu_header; ?>
                </div>
                <!-- END Header Content -->
                
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                 <div class="content content-full">
                    <div class="row">
                        <div class="col-md-5 col-xl-3">
                            <div class="block">
                                <div class="block-content">
                                    <?= $menu_pelapor; ?>
                                </div>
                            </div>
                            <!-- END Collapsible Inbox Navigation -->
                        </div>
                        <div class="col-md-7 col-xl-9">
                            <?= $content; ?>
                        </div>
                    </div>
                 </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

        </div>
        <!-- END Page Container -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-102902098-4"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-102902098-4');
		</script>
		 <!-- Histats.com  START  (aync)-->
		<script type="text/javascript">var _Hasync= _Hasync|| [];
		_Hasync.push(['Histats.start', '1,4213126,4,0,0,0,00010000']);
		_Hasync.push(['Histats.fasi', '1']);
		_Hasync.push(['Histats.track_hits', '']);
		(function() {
		var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
		hs.src = ('//s10.histats.com/js15_as.js');
		(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
		})();</script>
		<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4213126&101" alt="invisible hit counter" border="0"></a></noscript>
		<!-- Histats.com  END  -->
        <script src="<?= base_url(); ?>assets/js/core/bootstrap.bundle.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/core/jquery.appear.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/core/jquery.countTo.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/core/js.cookie.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/codebase.js"></script>

        <!-- Page JS Plugins -->
        <script src="<?= base_url(); ?>assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/plugins/chartjs/Chart.bundle.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/plugins/slick/slick.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/plugins/select2/select2.full.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/plugins/slick/slick.min.js"></script>


        <script src="<?= base_url(); ?>assets/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="<?= base_url(); ?>assets/js/plugins/jquery-validation/additional-methods.min.js"></script>
        
        <!-- Page JS Code -->
        <script src="<?= base_url(); ?>assets/js/pages/be_pages_dashboard.js"></script>
        <script src="<?= base_url(); ?>assets/js/pages/auth_ubah_pw.js"></script>
        <script src="<?= base_url(); ?>assets/js/pages/form_pengaduan.js"></script>
        <!-- Another -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-chained/1.0.1/jquery.chained.js"></script>
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5c4bbc91ab5284048d0ea306/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
        </script>
        <script>

            $("#id_kelurahan").chained("#id_kecamatan");
            jQuery(function () {
                // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
                Codebase.helpers(['datepicker', 'select2', 'slick']);
            });
        </script>
         <?php if ($this->session->userdata('success_message')): ?>
            <script>
                swal({
                    title: "Berhasil",
                    text: "<?php echo $this->session->userdata('success_message'); ?>",
                    timer: 3000,
                    button: false,
                    icon: 'success'
                });
            </script>
        <?php endif; ?>
        <?php if ($this->session->userdata('error_message')): ?>
            <script>
                swal({
                    title: "Oops...",
                    text: "<?php echo $this->session->userdata('error_message'); ?>",
                    timer: 3000,
                    button: false,
                    icon: 'error'
                });
            </script>
        <?php endif; ?>
    </body>
</html>