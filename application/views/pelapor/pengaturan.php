<div class="row">
    <div class="col-lg-12">
        <div class="block">
            <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#info-umum">Informasi Umum</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#ubah-kata-sandi">Ubah Kata Sandi</a>
                </li>
            </ul>
            <div class="block-content tab-content pb-30">
                <div class="tab-pane active" id="info-umum" role="tabpanel">
                    <div class="row justify-content-center">
                        <div class="col-lg-10">
                            <form action="<?= base_url('pengaturan/ubah_info'); ?>" method="post">
                                <input type="hidden" name="id" value="<?= $id_pelapor;?>">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" >Nama Lengkap</label>
                                    <div class="col-lg-8">
                                        <input type="text" id="nama" class="form-control <?php if(form_error('nama') !== ''){ echo 'is-invalid'; } ?>" name="nama" value="<?php echo $nama; ?>"  placeholder="Full Name">
                                        <div class="form-text text-danger"><?php echo form_error('nama') ?></div>
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" >Email</label>
                                    <div class="col-lg-8">
                                        <input type="email" id="email" class="form-control <?php if(form_error('email') !== ''){ echo 'is-invalid'; } ?>" name="email" value="<?php echo $email; ?>"  placeholder="Email">
                                        <div class="form-text text-danger"><?php echo form_error('email') ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" >No. Handphone</label>
                                    <div class="col-lg-8">
                                        <input type="text" id="phone" class="form-control <?php if(form_error('phone') !== ''){ echo 'is-invalid'; } ?>" name="phone" value="<?php echo $phone; ?>"  placeholder="No. Handphone">
                                        <div class="form-text text-danger"><?php echo form_error('phone') ?></div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn bg-gd-sea text-white btn-lg btn-block">Simpan Perubahan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="ubah-kata-sandi" role="tabpanel">
                    <div class="row justify-content-center">
                        <div class="col-lg-10">
                            <form class="validasi-ubah-sandi" action="<?= base_url('pengaturan/ubah_pw');?>" method="post">
                              <input hidden type="email" name="email" value="<?= $email;?>">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" >Kata Sandi Lama</label>
                                    <div class="col-lg-8">
                                        <input type="password" id="ubah-lama" class="form-control <?php if(form_error('ubah-lama') !== ''){ echo 'is-invalid'; } ?>" name="ubah-lama"  placeholder="Kata Sandi Lama">
                                        <div class="form-text text-danger"><?php echo form_error('ubah-lama') ?></div>
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" >Kata Sandi Baru</label>
                                    <div class="col-lg-8">
                                        <input type="password" id="ubah-baru" class="form-control <?php if(form_error('ubah-baru') !== ''){ echo 'is-invalid'; } ?>" name="ubah-baru" placeholder="Kata Sandi Baru">
                                        <div class="form-text text-danger"><?php echo form_error('ubah-baru') ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" >Konfirmasi Kata Sandi Baru</label>
                                    <div class="col-lg-8">
                                        <input type="password" id="ubah-knf" class="form-control <?php if(form_error('ubah-knf') !== ''){ echo 'is-invalid'; } ?>" name="ubah-knf" placeholder="Konfirmasi Kata Sandi Baru">
                                        <div class="form-text text-danger"><?php echo form_error('ubah-knf') ?></div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn bg-gd-sea text-white btn-lg btn-block">Simpan Perubahan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    })
</script>
