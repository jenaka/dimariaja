<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <?= $header; ?>
    </head>
    <body>
        <div id="page-loader" class="show"></div>
        <div id="page-container" class="sidebar side-scroll page-header-fixed page-header main-content-boxed">

            <?= $menu; ?>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="page-header">
                <!-- Header Content -->
                <div class="content-header">
                    <!-- Left Section -->
                    <div class="content-header-section">
                        <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none mr-20" data-toggle="layout" data-action="sidebar_toggle">
                            <i class="fa fa-navicon"></i>
                        </button>
                        <!-- Logo -->
                        <div class="content-header-item d-none d-sm-block d-sm-none d-md-block d-md-none d-lg-block">
                            <a class="font-w700" href="<?= base_url('admin'); ?>">
                                <img src="<?= base_url('assets/img/logo_ver.png'); ?>" width="200px">
                            </a>
                        </div>
                        <!-- END Logo -->
                    </div>
                    <!-- END Left Section -->

                    <!-- Right Section -->
                    <div class="content-header-section">
                        <?= $menu_top; ?>

                        <?= $notif; ?>

                        <!-- User Dropdown -->
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo $this->session->userdata('nama'); ?><i class="fa fa-angle-down ml-5"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right min-width-150" aria-labelledby="page-header-user-dropdown">
                                <a class="dropdown-item" href="<?= base_url('admin/pengaturan/'); ?>" data-toggle="layout" data-action="side_overlay_toggle">
                                    <i class="si si-wrench mr-5"></i> Pengaturan
                                </a>
                                <!-- END Side Overlay -->

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?= base_url('admin/login/logout'); ?>">
                                    <i class="si si-logout mr-5"></i> Keluar
                                </a>
                            </div>
                        </div>

                    </div>
                    <!-- END Right Section -->
                </div>
                <!-- END Header Content -->
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container" style="padding-top: 0px">
                <!-- Page Content -->
                <?= $content; ?>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->
            
        </div>
        <!-- END Page Container -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-102902098-4"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-102902098-4');
        </script>
         <!-- Histats.com  START  (aync)-->
        <script type="text/javascript">var _Hasync= _Hasync|| [];
        _Hasync.push(['Histats.start', '1,4213126,4,0,0,0,00010000']);
        _Hasync.push(['Histats.fasi', '1']);
        _Hasync.push(['Histats.track_hits', '']);
        (function() {
        var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
        hs.src = ('//s10.histats.com/js15_as.js');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
        })();</script>
        <noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4213126&101" alt="invisible hit counter" border="0"></a></noscript>
        <!-- Histats.com  END  -->
        <?= $footer; ?>
    </body>
</html>