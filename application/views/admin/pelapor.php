<div class="content">
    <div class="block block-rounded block-transparent bg-gd-sea">
        <div class="block-content">
            <div class="py-20 text-center">
                <h1 class="font-w700 text-white mb-10"><?php echo $title; ?></h1>
                <h2 class="h4 font-w400 text-white-op"><?php echo $sub; ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <!-- Default Elements -->
            <div class="block block-rounded">
                <div class="block-content pb-15">
                   <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>Nama Lengkap</th>
                                <th>Email</th>
                                <th>No. HP</th>
                                <th class="text-center">Status</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($guest_data as $data){
                            ?>
                            <tr>
                                <td class="text-center"><?= $no++; ?></td>
                                <td class="font-w600"><?= htmlspecialchars($data->nama,ENT_QUOTES,'UTF-8');?> </td>
                                <td class="font-w600"><?= htmlspecialchars($data->email,ENT_QUOTES,'UTF-8');?> </td>
                                <td class="font-w600"><?= htmlspecialchars($data->phone,ENT_QUOTES,'UTF-8');?> </td>
                                <td><center>
                                    <?php if($data->status == 1){ ?>
                                        <button class="btn btn-sm btn-success" onclick="deaktivasi(<?= $data->id_pelapor; ?>)">Aktif</button>
                                    <?php }else{ ?>
                                        <button class="btn btn-sm btn-danger" onclick="aktivasi(<?= $data->id_pelapor; ?>)">Tidak Aktif</button>
                                    <?php } ?>
                                </center></td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Detail Pelapor" onclick="detail(<?= $data->id_pelapor; ?>)"><i class="si si-user"></i></a>
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit Pelapor" onclick="edit(<?= $data->id_pelapor; ?>)"><i class="si si-note"></i></a>
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Hapus Pelapor"  onclick="hapus(<?= $data->id_pelapor; ?>)">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END Default Elements -->
        </div>
    </div>
</div>
<script type="text/javascript">

    function tambah(){
        save_method = 'add';
        $('#form_guest')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_form').modal('show')
        $('#modal_title').text('<?= lang('add_new_guest'); ?>');
    }

    function detail(id){
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('admin/pelapor/edit/')?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id_pelapor"]').val(data.id_pelapor);
                $('[name="full_name"]').val(data.nama);
                $('[name="email_address"]').val(data.email);
                $('[name="phone"]').val(data.phone);
                $('#detail_pelapor').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    }

    function edit(id){
        save_method = 'update';
        $('#form_guest')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('admin/pelapor/edit/')?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id_pelapor"]').val(data.id_pelapor);
                $('[name="full_name"]').val(data.nama);
                $('[name="email_address"]').val(data.email);
                $('[name="phone"]').val(data.phone);
                $('#password_field').hide();
                $('#re_password_field').hide();
                $('#modal_form').modal('show');
                $('#modal_title').text('Perbaharui Pelapor');
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    }

    function simpan(){
        $('#btnSave').text('Simpan');
        $('#btnSave').attr('disabled',true);
        var url;

        if(save_method == 'add') {
            url = "<?php echo site_url('admin/pelapor/simpan')?>";
        } else {
            url = "<?php echo site_url('admin/pelapor/update')?>";
        }

        var formData = new FormData($('#form_guest')[0]);
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data){

                if(data.status){
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil disimpan",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){
                        location.reload();
                    } ,1500);
                }else{
                    for (var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
                $('#btnSave').text('Simpan');
                $('#btnSave').attr('disabled',false);
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error adding / update data');
                $('#btnSave').text('Simpan');
                $('#btnSave').attr('disabled',false);

            }
        });
    }

    function hapus(id) {
    swal({
      title: "Anda Yakin?",
      text: "Pelapor Yang Dihapus Tidak Akan Bisa Dikembalikan",
      icon: "warning",
      buttons: ["Batal", "Hapus"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>admin/pelapor/delete/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Success",
                        text: "Data berhasil dihapus",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
            }
        });
      } else {
        window.setTimeout(function(){
            location.reload();
        } ,1500);
      }
    });
    }

    function aktivasi(id) {
    swal({
      title: "Aktifkan Pengguna?",
      text: "Pengguna Akan Bisa Digunakan Kembali",
      icon: "warning",
      buttons: ["Tidak!", "Ya!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>admin/pelapor/activate/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Success",
                        text: "Pengguna Berhasil Diaktifkan",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error activate data');
            }
        });
      } else {
        window.setTimeout(function(){
            location.reload();
        } ,1500);
      }
    });
    }

    function deaktivasi(id) {
    swal({
      title: "Non Aktifkan Pengguna?",
      text: "Pengguna Tidak Akan Digunakan Kembali",
      icon: "warning",
      buttons: ["Tidak!", "Ya!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>admin/pelapor/deactive/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Success",
                        text: "Pelapor Berhasil Di Non-Aktifkan",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deactivate data');
            }
        });
      } else {
        window.setTimeout(function(){
            location.reload();
        } ,1500);
      }
    });
    }

</script>

<!-- edit pelapor -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-gd-sea">
                    <h3 class="block-title" id="modal_title">Form Guest</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                   <form id="form_guest" class="form-horizontal">
                        <input type="hidden" value="" name="id_pelapor"/>
                        <div class="form-group">
                            <label class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" name="full_name" placeholder="Nama Lengkap">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Email</label>
                            <input type="email" class="form-control" name="email_address" placeholder="Alamat Email">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">No. Hanphone</label>
                            <input type="text" class="form-control" name="phone" placeholder="No. Handphone">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group" id="password_field">
                            <label class="col-form-label">Kata Sandi</label>
                            <input type="password" class="form-control" name="password" placeholder="Kata Sandi">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group" id="re_password_field">
                            <label class="col-form-label">Konfirmasi Kata Sandi</label>
                            <input type="password" class="form-control" name="re_password" placeholder="Konfirmasi Kata Sandi">
                            <span class="text-danger"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" class="btn btn-alt-primary" data-dismiss="modal" onclick="simpan()"></i>Simpan
                </button>
                <button type="button" class="btn btn-alt-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->

<!-- detail pelapor -->
<div class="modal fade" id="detail_pelapor" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-gd-sea">
                    <h3 class="block-title">Detail Pelapor</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                   <form id="form_guest" class="form-horizontal">
                        <input type="hidden" value="" name="id_pelapor"/>
                        <div class="form-group">
                            <label class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" name="full_name" placeholder="Nama Lengkap" readonly="readonly">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Email</label>
                            <input type="email" class="form-control" name="email_address" placeholder="Alamat Email" readonly="readonly">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">No. Hanphone</label>
                            <input type="text" class="form-control" name="phone" placeholder="No. Handphone" readonly="readonly">
                            <span class="text-danger"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->
