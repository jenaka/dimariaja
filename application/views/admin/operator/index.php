<div class="content">
    <div class="block block-rounded block-transparent bg-gd-sea">
        <div class="block-content">
            <div class="py-20 text-center">
                <h1 class="font-w700 text-white mb-10"><?php echo $title; ?></h1>
                <h2 class="h4 font-w400 text-white-op"><?php echo $sub; ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
    	<div class="col-lg-12">
	        <!-- Default Elements -->
	        <div class="block block-themed block-rounded">
	            <div class="block-content pb-15">
                    <div class="push">
                        <a class="btn btn-rounded btn-alt-secondary" href="<?php echo base_url('admin/operator/trash');?>">
                            <i class="fa fa-archive text-danger mx-5"></i>
                            <span class="d-none d-sm-inline"> Operator Dihapus</span>
                        </a>
                        <button class="btn btn-rounded btn-alt-secondary float-right" onclick="tambah()">
                            <i class="si si-plus text-primary mx-5"></i>
                            <span class="d-none d-sm-inline"> Tambah Operator</span>
                        </button>
                    </div>
	               <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="text-center"></th>
                                <th>Nama</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>No Telepon</th>
                                <th>Status</th>
                                <th class="text-center" style="width: 15%;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        	$no = 1;
					        foreach ($operator_data as $data){
                                if($data->active == 1){
                                    $active = '<button class="btn btn-sm btn-success mr-5 mb-5" style="curson:pointer;" onclick="deaktivasi('.$data->id.')">Aktif</button>';
                                }else{
                                    $active = '<button class="btn btn-sm btn-danger mr-5 mb-5" style="curson:pointer;" onclick="aktivasi('.$data->id.')">Tidak Aktif</button>';
                                }
					        ?>
                            <tr>
                                <td class="text-center"><?php echo $no++; ?></td>
                                <td class="font-w600"><?php echo $data->nama; ?></td>
                                <td class="font-w600"><?php echo $data->username; ?></td>
                                <td class="font-w600"><?php echo $data->email; ?></td>
                                <td class="font-w600"><?php echo $data->telp; ?></td>
                                <td class="font-w600 text-center"><?= $active; ?></td>
                                <td class="text-center">
                                   <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit Operator" onclick="edit(<?php echo $data->id;?>)">
                                        <i class="si si-note"></i>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Hapus Operator" onclick="hapus(<?php echo $data->id;?>)">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
	            </div>
	        </div>
	        <!-- END Default Elements -->
	    </div>
    </div>
</div>

<script type="text/javascript">
    function tambah(){
        save_method = 'add';
        $('#form_guest')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_form').modal('show')
        $('#modal_title').text('Tambah Operator Baru');
    }
 
    function edit(id){
        save_method = 'update';
        $('#form_guest')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('admin/operator/edit/')?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="nama"]').val(data.nama);
                $('[name="email"]').val(data.email);
                $('[name="username"]').val(data.username);
                $('[name="telp"]').val(data.telp);
                $('[name="id_kecamatan"]').val(data.id_kecamatan).change();;
                $('[name="id_kelurahan"]').val(data.id_kelurahan).change();;
                $('#password_field').hide();
                $('#re_password_field').hide();
                $('#modal_form').modal('show');
                $('#modal_title').text('Perbaharui Operator');
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    }

    function simpan(){
        $('#btnSave').text('Menyimpan');
        $('#btnSave').attr('disabled',true);
        var url;
     
        if(save_method == 'add') {
            url = "<?php echo site_url('admin/operator/simpan')?>";
        } else {
            url = "<?php echo site_url('admin/operator/update')?>";
        }

        var formData = new FormData($('#form_guest')[0]);
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data){
     
                if(data.status){
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil disimpan",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                }else{
                    for (var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
                $('#btnSave').text('Simpan');
                $('#btnSave').attr('disabled',false);
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error adding / update data');
                $('#btnSave').text('Simpan');
                $('#btnSave').attr('disabled',false);
     
            }
        });
    }

    function hapus(id) {
    swal({
      title: "Anda Yakin?",
      text: "Operator Yang Dihapus Di Simpan Di Keranjang Sampah",
      icon: "warning",
      buttons: ["Batal!", "Hapus!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>admin/operator/hapus/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil dihapus",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
            }
        });
      } else {
        window.setTimeout(function(){ 
            location.reload();
        } ,1500);
      }
    });
    }

    function aktivasi(id) {
    swal({
      title: "Aktifkan operator?",
      text: false,
      icon: "warning",
      buttons: ["Tidak!", "Ya!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>admin/operator/aktif/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil diaktifkan",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
            }
        });
      } else {
        window.setTimeout(function(){ 
            location.reload();
        } ,1500);
      }
    });
    }

    function deaktivasi(id) {
    swal({
      title: "Non Aktifkan operator?",
      text: false,
      icon: "warning",
      buttons: ["Tidak!", "Ya!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>admin/operator/nonaktif/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil di non-aktifkan",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
            }
        });
      } else {
        window.setTimeout(function(){ 
            location.reload();
        } ,1500);
      }
    });
    }
</script>
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-gd-sea">
                    <h3 class="block-title" id="modal_title">Form Operator</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                   <form id="form_guest" class="form-horizontal">
                        <input type="hidden" value="" name="id"/> 
                        <div class="form-group">
                            <label class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Username</label>
                            <input type="text" class="form-control" name="username" placeholder="Username">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Alamat Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Alamat Email">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">No. Handphone</label>
                            <input type="text" class="form-control" name="telp" placeholder="No. Handphone">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Kecamatan</label>
                            <select class="form-control <?php if(form_error('id_kecamatan') !== ''){ echo 'is-invalid'; } ?>" name="id_kecamatan" id="kecamatan">  
                            <option value="">Pilih</option>
                                <?php
                                foreach ($kecamatan_data as $kec) {
                                    ?>
                                    <!--di sini kita tambahkan class berisi id provinsi-->
                                    <option <?php echo $id_kecamatan == $kec->id_kecamatan ? 'selected="selected"' : '' ?> 
                                        class="<?php echo $kec->id_kecamatan ?>" value="<?php echo $kec->id_kecamatan ?>"><?php echo $kec->nama_kecamatan ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Kelurahan</label>
                            <select class="form-control <?php if(form_error('id_kelurahan') !== ''){ echo 'is-invalid'; } ?>" name="id_kelurahan" <?php if($this->session->userdata('login_type') ==  'Operator'){ echo'disabled="disabled"'; }else{ echo 'id="kelurahan"'; } ?>>
                                <option value="">Pilih</option>
                                    <?php
                                    foreach ($kelurahan_data as $kel) {
                                        ?>
                                        <option <?php echo $kelurahan_selected == $kel->id_kecamatan ? 'selected="selected"' : '' ?> 
                                            class="<?php echo $kel->id_kecamatan ?>" value="<?php echo $kel->id_kelurahan ?>"><?php echo $kel->nama_kelurahan ?></option>
                                        <?php
                                    }
                                    ?>
                            </select>
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group" id="password_field">
                            <label class="col-form-label">Kata Sandi</label>
                            <input type="password" class="form-control" name="password" placeholder="Kata Sandi">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group" id="re_password_field">
                            <label class="col-form-label">Konfirmasi Kata Sandi</label>
                            <input type="password" class="form-control" name="re_password" placeholder="Konfirmasi Kata Sandi">
                            <span class="text-danger"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" class="btn btn-alt-primary" data-dismiss="modal" onclick="simpan()"></i>Simpan
                </button>
                <button type="button" class="btn btn-alt-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->