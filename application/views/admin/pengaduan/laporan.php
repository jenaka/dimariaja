<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <!-- Default Elements -->
            <div class="block block-themed block-rounded">
                <div class="block-content">
                    <?php if($kasus_data == ''){ ?>
                    <form action="<?= $action; ?>" method="POST">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" >Dari Tanggal</label>
                                <div class="col-lg-8">
                                    <input type="text" class="js-datepicker form-control <?php if(form_error('tgl_mulai') !== ''){ echo 'is-invalid'; } ?>" id="tgl_mulai" name="tgl_mulai" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="Dari Tanggal">
                                    <div class="form-text text-danger"><?php echo form_error('tgl_mulai') ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" >Sampai Tanggal</label>
                                <div class="col-lg-8">
                                    <input type="text" class="js-datepicker form-control <?php if(form_error('tgl_akhir') !== ''){ echo 'is-invalid'; } ?>" id="tgl_akhir" name="tgl_akhir" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="Sampai Tanggal">
                                    <div class="form-text text-danger"><?php echo form_error('tgl_akhir') ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn bg-gd-emerald text-white btn-lg btn-block">Cari</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                    <?php }else{ ?>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <center><h3>Data Pengaduan Kasus Dari Tanggal <?= $tgl_mulai.' s/d '.$tgl_akhir; ?> </h3></center>
                            <div class="row" style="margin-bottom: 15px;">
                                <div class="col-lg-12">               
                                    <a class="btn btn-rounded btn-alt-secondary float-right" href="#" style="margin-left: 15px;">
                                        <i class="si si-doc text-primary mx-5"></i>
                                        <span class="d-none d-sm-inline"> PDF</span>
                                    </a>
                                    <a class="btn btn-rounded btn-alt-secondary float-right" href="#">
                                        <i class="fa fa-list text-primary mx-5"></i>
                                        <span class="d-none d-sm-inline"> Excel</span>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-bordered table-striped table-vcenter datatable">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="width: 5%">#</th>
                                                <th width="15%">NIK</th>
                                                <th width="15%">No. Passport</th>
                                                <th width="15%">Nama Lengkap</th>
                                                <th width="10%">Kecamatan</th>
                                                <th width="10%">Kelurahan</th>
                                                <th class="text-center" style="width: 10%;">Tanggal</th>
                                                <th class="text-center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 1;
                                            foreach ($kasus_data as $data){

                                            ?>
                                            <tr>
                                                <td class="text-center"><?php echo $no++; ?></td>
                                                <td class="font-w600"><?php echo $data->NIK; ?></td>
                                                <td class="font-w600"><?php echo $data->no_passport; ?></td>
                                                <td class="font-w600"><?php echo $data->nama; ?></td>
                                                <td class="font-w600"><?php echo $data->nama_kecamatan; ?></td>
                                                <td class="font-w600"><?php echo $data->nama_kelurahan; ?></td>
                                                <td class="font-w600"><?php echo  date("d-m-Y", strtotime($data->tgl)); ?></td>
                                                <td class="font-w600">
                                                    <?php if($data->status == '0'){
                                                    echo '<a class="badge badge-danger" href="javascript:void(0)">Proses</a>';
                                                    }else{
                                                        $this->db->where('id_kasus', $data->id_kasus);
                                                        $this->db->from('terusan');
                                                        $this->db->join('user_akses', 'user_akses.id=terusan.id_akses');
                                                        $query = $this->db->get()->result();
                                                        foreach ($query as $t){
                                                            echo '<a class="badge badge-success" href="javascript:void(0)" style="margin-right:4px;">'.$t->nama.' </a>';
                                                        }
                                                    }?>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!-- END Default Elements -->
        </div>
    </div>
</div>