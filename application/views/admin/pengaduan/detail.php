<div class="content">
    <div class="row">
    	<div class="col-lg-12">
            <div class="block-header block-header-default">
                <a class="btn btn-primary btn-rounded float-right" href="#">
                    <i class="si si-printer"></i> Download
                </a>
                <?php if($this->session->userdata('login_type') == 'Admin' && $status == '0'){
                    if($status_pmi == '0'){ ?>
                        <button type="button" class="btn btn-primary btn-rounded" data-toggle="tooltip" title="Teruskan Laporan" onclick="error()">
                        <i class="si si-action-redo"></i> Teruskan Pengaduan
                    </button>
                    <?php }else{ ?>
                       <button type="button" class="btn btn-primary btn-rounded" data-toggle="tooltip" title="Teruskan Laporan" onclick="teruskan()">
                        <i class="si si-action-redo"></i> Teruskan Pengaduan
                    </button>
                    <?php }
                } ?>
                <?php if($this->session->userdata('login_type') == 'Operator' && $status_pmi == '0'){ ?>
                        <button type="button" class="btn btn-primary btn-rounded" data-toggle="tooltip" title="Teruskan Laporan" onclick="verifikasi(<?= $id; ?>)">
                        <i class="si si-action-redo"></i> Verifikasi PMI
                    </button>
                <?php } ?>
            </div>
	        <div class="block block-rounded">
                <div class="block-content">
                	<table class="table table-striped table-vcenter" style="width: 100%;">
                        <thead>
                            <tr>
                                <th colspan="3">IDENTITAS PMI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            	<td width="40%">Nama Lengkap</td>
                            	<td>: <?= $nama; ?></td>
                            </tr>
                            <tr>
                            	<td>Tempat / Tanggal Lahir</td>
                            	<td>: <?= $tmp_lahir.' / '.date("d-m-Y", strtotime($tgl_lahir)); ?></td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td>: <?= $jk; ?></td>
                            </tr>
                            <tr>
                            	<td>No. Passport</td>
                            	<td>: <?= $no_passport; ?></td>
                            </tr>
                            <tr>
                            	<td>Alamat Asal</td>
                            	<td>: <?= $alamat_asal; ?> <br> Kec.<?= $kec_pmi; ?> Kel. <?= $kel_pmi; ?> Kabupaten Bandung Barat</td>
                            </tr>
                            <tr>
                            	<td>Alamat Kerja</td>
                            	<td>: <?= $alamat_kerja; ?></td>
                            </tr>
                            <tr>
                            	<td>Embarkasi / Debarkasi</td>
                            	<td>: <?= $embarsi.' / '.$debarsi; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-striped table-vcenter" style="width: 100%;">
                        <thead>
                            <tr>
                                <th colspan="3">PPTKIS Yang Menempatkan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            	<td width="40%">Nama</td>
                            	<td>: <?= $nm_pptkis; ?></td>
                            </tr>
                            <tr>
                            	<td>Negara Penempatan</td>
                            	<td>: <?= $negara; ?></td>
                            </tr>
                            <tr>
                            	<td>Mulai Bekerka di Luar Negeri</td>
                            	<td>: <?= date("d-m-Y", strtotime($mulai_kerja)); ?></td>
                            </tr>
                            <tr>
                            	<td>Tanggal Kembali Ke Daerah Asal</td>
                            	<td>: <?= date("d-m-Y", strtotime($akhir_kerja)); ?></td>
                            </tr>
                            <tr>
                            	<td>Lama Bekerja</td>
                            	<td>: <?= $lama_kerja; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-striped table-vcenter" style="width: 100%;">
                        <thead>
                            <tr>
                                <th colspan="3">Agency</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            	<td width="40%">Nama Agency</td>
                            	<td>: <?= $nm_agency; ?></td>
                            </tr>
                            <tr>
                            	<td>Alamat</td>
                            	<td>: <?= $alamat_agency; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-striped table-vcenter" style="width: 100%;">
                        <thead>
                            <tr>
                                <th colspan="3">Pengguna Jasa TKI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            	<td width="15%">Nama Majikan</td>
                            	<td width="20%">: a. Awal</td>
                            	<td>: <?= $f_nama_mjk; ?></td>
                            </tr>
                            <tr>
                            	<td></td>
                            	<td width="20%">: b. Akhir</td>
                                <td>: <?= $l_nama_mjk; ?></td>
                            </tr>
                            <tr>
                            	<td></td>
                            	<td width="20%">: c. Jumlah Majikan</td>
                                <td>: <?= $jml_mjk; ?> Orang</td>
                            </tr>
                            <tr>
                            	<td width="40%">Nama Badan usaha</td>
                            	<td colspan="2">: <?= $badan_usaha;?></td>
                            </tr>
                            <tr>
                            	<td width="40%">Alamat</td>
                            	<td colspan="2">: <?= $alamat_mjk; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-striped table-vcenter" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>MASALAH YANG DILAPORKAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?= $masalah; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-striped table-vcenter" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>TUNTUTAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?= $tuntutan; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-striped table-vcenter" style="width: 100%;">
                        <thead>
                            <tr>
                                <th colspan="3">Berkas Yang Dilampiran</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($bukti_pengaduan as $u) { ?>
                            <tr>
                                <td class="text-center" scope="row" width="10%"><?= $no++; ?></td>
                                <td><?= $u->path; ?></td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="<?= base_url('uploads/pengaduan/'.$u->path); ?>" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Download">
                                            <i class="fa fa-download"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
	            </div>
	        </div>
	        <!-- END Default Elements -->
	    </div>
    </div>
</div>
<script type="text/javascript">

var save_method;
var table;
var base_url = '<?php echo base_url();?>';

    function verifikasi(id) {
    swal({
      title: "Verifikasi PMI ?",
      text: false,
      icon: "warning",
      buttons: ["Batal!", "Verifikasi!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>admin/pengaduan/verifikasi_pmi/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "PMI Berhasil Terverifikasi",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error verifikasi');
            }
        });
      } else {
        window.setTimeout(function(){ 
            location.reload();
        } ,1500);
      }
    });
    }


    function error() {
        swal({
            title: "PMI Belum Terverifikasi",
            text: "Hubungi Pihak Operator Desa Untuk Segera Verifikasi PMI",
            timer: 3500,
            buttons: false,
            icon: 'error'
        });
    }

    function teruskan(){
        save_method = 'ditambahkan';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_form').modal('show');
        $('.modal-title').text('Teruskan Kasus');
    }

</script>

<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-rounded mb-0">
                <form action="<?php echo site_url('admin/pengaduan/teruskan/')?>" id="form" class="form-horizontal" method="POST">
                <div class="modal-header block-header bg-gd-sea">
                    <h3 class="modal-title block-title text-white">Teruskan Pengaduan</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option text-white" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="modal-body form">
                        <input type="hidden" value="<?= $id; ?>" name="id_pengaduan"/> 
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" >Diteruskan Kepada</label>
                            <div class="col-lg-8">
                                <select class="js-select2 form-control" id="terusan" name="terusan[]" style="width: 100%;" data-placeholder="Pilih.." multiple>
                                 <option></option>
                                 <option value="6">BNP2TKI</option>
                                 <option value="7">BNP3TKI</option>
                                 <option value="8">KEMENLU</option>
                                 <option value="9">KEMNAKER</option>
                                 <option value="10">DISNAKERTRANS JABAR</option>
                                </select>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnSave" class="btn bg-gd-sea btn-block text-white">Simpan</button>
                </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>