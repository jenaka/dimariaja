<div class="content">
    <div class="row">
    	<div class="col-lg-12">
	        <!-- Default Elements -->
	        <div class="block block-themed block-rounded">
	            <div class="block-content">
	               <table class="table table-bordered table-striped table-vcenter datatable">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 7%">#</th>
                                <th>Nama Pengadu</th>
                                <th>No. Handphone</th>
                                <th>No. Passport - Nama PMI</th>
                                <th>Tanngal</th>
                                <th class="text-center">Status</th>
                                <th class="text-center" style="width: 15%;">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        	$no = 1;
					        foreach ($pengaduan_data as $data){
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $no++; ?></td>
                                <td class="font-w600"><?php echo $data->nama_pengadu; ?></td>
                                <td class="font-w600"><?php echo $data->telp_pengadu; ?></td>
                                <td class="font-w600"><?php echo $data->no_passport.' - '.$data->nama; ?></td>
                                <td class="font-w600"><?php echo  date("d-m-Y", strtotime($data->tgl)); ?></td>
                                <td class="font-w600">
                                    <?php if($data->status == '0'){
                                    echo '<center><a class="badge badge-danger" href="javascript:void(0)">Proses</a></center>';
                                    }else{
                                        $this->db->where('id_pengaduan', $data->id_pengaduan);
                                        $this->db->from('terusan');
                                        $this->db->join('user_akses', 'user_akses.id=terusan.id_akses');
                                        $query = $this->db->get()->result();
                                        foreach ($query as $t){
                                            echo '<center><a class="badge badge-success" href="javascript:void(0)" style="margin-right:4px;">'.$t->nama.' </a></center>';
                                        }
                                    }?>
                                </td>
                                <td class="text-center">
                                    <?php if($this->session->userdata('operator')){?>
                                        <button type="button" class="btn btn-success btn-block" data-toggle="tooltip" title="Restore Pengaduan" onclick="restore(<?php echo $data->id_pengaduan;?>)">
                                        <i class="si si-refresh"></i>
                                    </button>
                                    <?php }else{ ?>
                                    <button type="button" class="btn btn-success " data-toggle="tooltip" title="Restore Pengaduan" onclick="restore(<?php echo $data->id_pengaduan;?>)">
                                        <i class="si si-refresh"></i>
                                    </button>
                                    <button type="button" class="btn btn-danger " data-toggle="tooltip" title="Hapus Permananen Pengaduan" onclick="permanen(<?php echo $data->id_pengaduan;?>)">
                                        <i class="si si-trash"></i>
                                    </button>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
	            </div>
	        </div>
	        <!-- END Default Elements -->
	    </div>
    </div>
</div>

<script type="text/javascript">
    function restore(id) {
    swal({
      title: "Anda Yakin?",
      text: "Kasus Yang Di Restore Di Kembalikan Seperti Semula",
      icon: "warning",
      buttons: ["Batal!", "Restore!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>pengaduan/restore/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil Direstore",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
            }
        });
      } else {
        window.setTimeout(function(){ 
            location.reload();
        } ,1500);
      }
    });
    }
</script>
<script type="text/javascript">
    function permanen(id) {
    swal({
      title: "Anda Yakin?",
      text: "Pengaduan akan dihapus secara permanen!",
      icon: "warning",
      buttons: ["Batal!", "Hapus!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>pengaduan/permanen/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil dihapus permanen",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
            }
        });
      } else {
        window.setTimeout(function(){ 
            location.reload();
        } ,1500);
      }
    });
    }
</script>