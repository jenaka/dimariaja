<div class="content">
    <div class="block block-rounded block-transparent bg-gd-sea">
        <div class="block-content">
            <div class="py-20 text-center">
                <h1 class="font-w700 text-white mb-10"><?php echo $title; ?></h1>
                <h2 class="h4 font-w400 text-white-op"><?php echo $sub; ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
    	<div class="col-lg-12">
	        <!-- Default Elements -->
	        <div class="block block-rounded">
	            <div class="block-content pb-15">
                    <!-- <?php if($this->session->userdata('login_type') == 'Operator' || $this->session->userdata('login_type') == 'Admin' || $this->session->userdata('login_type') == 'Superadmin'){ ?>
                    <div class="push">
                        <a class="btn btn-rounded btn-alt-secondary" href="<?php echo base_url('admin/pengaduan/trash');?>">
                            <i class="fa fa-archive text-danger mx-5"></i>
                            <span class="d-none d-sm-inline"> Pengaduan Dihapus</span>
                        </a>
                        <a class="btn btn-rounded btn-alt-secondary float-right" href="<?php echo base_url('admin/pengaduan/tambah');?>">
                            <i class="si si-plus text-primary mx-5"></i>
                            <span class="d-none d-sm-inline"> Tambah Pengaduan</span>
                        </a>
                    </div>
                    <?php } ?> -->
	                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 7%">#</th>
                                <th>Nama Pengadu</th>
                                <th>No. Passport - Nama PMI</th>
                                <th>Tanngal</th>
                                <th class="text-center">Status PMI</th>
                                <th class="text-center">Status Pengaduan</th>
                                <th class="text-center" style="width: 15%;">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        	$no = 1;
					        foreach ($pengaduan_data as $data){
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $no++; ?></td>
                                <td class="font-w600"><?php echo $data->nama_pengadu; ?></td>
                                <td class="font-w600"><?php echo $data->no_passport.' - '.$data->nama; ?></td>
                                <td class="font-w600"><?php echo  date("d-m-Y", strtotime($data->tgl)); ?></td>
                                <td class="font-w600">
                                    <?php if($data->status_pmi == '0'){
                                    echo '<center><a class="badge badge-danger" href="javascript:void(0)">Belum</a></center>';
                                    }else{
                                       echo '<center><a class="badge badge-success" href="javascript:void(0)">Terverifikasi</a></center>';
                                    }?>
                                </td>
                                <td class="font-w600">
                                    <?php if($data->status == '0'){
                                    echo '<center><a class="badge badge-danger" href="javascript:void(0)">Proses</a></center>';
                                    }else{
                                        $this->db->where('id_pengaduan', $data->id_pengaduan);
                                        $this->db->from('terusan');
                                        $this->db->join('user_akses', 'user_akses.id=terusan.id_akses');
                                        $query = $this->db->get()->result();
                                        foreach ($query as $t){
                                            echo '<center><a class="badge badge-success" href="javascript:void(0)" style="margin-right:4px;">'.$t->nama.' </a></center>';
                                        }
                                    }?>
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Detail Pengaduan" href="<?php echo base_url('admin/pengaduan/detail/'.$data->id_pengaduan); ?>">
                                        <i class="si si-eye"></i>
                                    </a>
                                    <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Detail Pelapor" onclick="detail_pelapor(<?php echo $data->id_pelapor;?>)">
                                        <i class="si si-user"></i>
                                    </button>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
	            </div>
	        </div>
	        <!-- END Default Elements -->
	    </div>
    </div>
</div>

<script type="text/javascript">

    function detail_pelapor(id){
        save_method = 'update';
        $('#form_guest')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('admin/pelapor/edit/')?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id_pelapor"]').val(data.id_pelapor);
                $('[name="full_name"]').val(data.nama).attr("disabled", true);;
                $('[name="email_address"]').val(data.email).attr("disabled", true);;
                $('[name="phone"]').val(data.phone).attr("disabled", true);;
                $('#password_field').hide();
                $('#re_password_field').hide();
                $('.modal-footer').hide();
                $('#modal_form').modal('show');
                $('#modal_title').text('Detail Pelapor');
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    }

    function hapus(id) {
    swal({
      title: "Anda Yakin?",
      text: "Pengaduan Yang Di Hapus Di Simpan Di Pengaduan Dihapus",
      icon: "warning",
      buttons: ["Batal!", "Hapus!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>pengaduan/hapus/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil dihapus",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
            }
        });
      } else {
        window.setTimeout(function(){ 
            location.reload();
        } ,1500);
      }
    });
    }
</script>
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title" id="modal_title">Form Guest</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                   <form id="form_guest" class="form-horizontal">
                        <input type="hidden" value="" name="id_pelapor"/> 
                        <div class="form-group">
                            <label class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" name="full_name" placeholder="Nama Lengkap">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Email</label>
                            <input type="email" class="form-control" name="email_address" placeholder="Alamat Email">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">No. Hanphone</label>
                            <input type="text" class="form-control" name="phone" placeholder="No. Handphone">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group" id="password_field">
                            <label class="col-form-label">Kata Sandi</label>
                            <input type="password" class="form-control" name="password" placeholder="Kata Sandi">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group" id="re_password_field">
                            <label class="col-form-label">Konfirmasi Kata Sandi</label>
                            <input type="password" class="form-control" name="re_password" placeholder="Konfirmasi Kata Sandi">
                            <span class="text-danger"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" class="btn btn-alt-primary" data-dismiss="modal" onclick="simpan()"></i>Simpan
                </button>
                <button type="button" class="btn btn-alt-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->