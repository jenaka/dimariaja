
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Pengaduan <?= $nama_pengadu; ?></title>

<style type="text/css">
    body {
  max-width: 100%;
  margin: 0 auto;
  font-size: 12px;
}


.table td {
    border-bottom: 1px solid black;
}

.table {
  border-collapse: separate;
  border-spacing: 0px;
/*  border-width: 1px;*/
  margin-bottom: 30px;
  font-size: 14px;
  width: 100%;
}

.table td, .table th {
  font-weight: normal;
  text-align: left;
}

.table th {
  border-width: 0 1px 1px 0;
  font-weight: bold;
  line-height: 24px;
  display: table-cell;
  vertical-align: middle;
  text-align: left;
  padding: 10px
}

.table td {
  border-width: 0.5px;
  padding: 3px;
}
</style>
</head>
<body>
	<div class="content" style="width: 700px;">
		<div class="header" style="width: 90%;margin-left:  3%;">
			<h2 style="font-size: 15px; text-align: center; margin-bottom: 0px">PENGADUAN PERMASALAHAN</h2>
			<h2 style="font-size: 15px; text-align: center; margin-bottom: 0px; margin-top: 3px">PMI PRA/MASA/PURNA PENEMPATAN </h2>
		</div>
		<p style="font-size: 14px;">Pada hari ini <?= hari($tgl); ?> tanggal <?= date("d", strtotime($tgl)); ?> Bulan <?= bulan(date("m", strtotime($tgl))); ?> Tahun <?= date("Y", strtotime($tgl)); ?>  <br>Saya yang bertanda tangan dibawah ini:</p>
		<table class="table" style="width: 100%;">
            <tbody>
                <tr><td width="5%">1</td>
                    <td width="30%">Nama</td>
                    <td width="2%">:</td>
                    <td><?= $nama_pengadu; ?></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Umur</td>
                    <td>:</td>
                    <td><?= $umur_pengadu; ?> Tahun</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td><?= $jk_pengadu; ?></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Pekerjaan</td>
                    <td>:</td>
                    <td><?= $pekerjaan_pengadu; ?></td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Alamat Asal</td>
                    <td>:</td>
                    <td><?= $alamat; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>:</td>
                    <td>Kec.<?= $nama_kecamatan; ?> Kel. <?= $nama_kelurahan; ?> Kabupaten Bandung Barat</td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>No. Handphone</td>
                    <td>:</td>
                    <td><?= $telp_pengadu; ?></td>
                </tr>
            </tbody>
        </table>
		<table style="width: 100%;"  class="table">
            <tbody>
                <tr>
                    <td colspan="4"><strong>I. IDENTITAS TKI</strong></td>
                </tr>
                <tr>
                    <td width="5%">1</td>
                	<td width="30%">Nama Lengkap</td>
                    <td width="2%">:</td>
                	<td><?= $nama; ?></td>
                </tr>
                <tr>
                    <td>2</td>
                	<td>Umur</td>
                    <td>:</td>
                	<td><?= $umur; ?> Tahun</td>
                </tr>
                <tr>
                    <td>3</td>
                	<td>Jenis Kelamin</td>
                    <td>:</td>
                	<td><?= $jk_pmi; ?></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>No. Passport</td>
                    <td>:</td>
                    <td><?= $no_passport; ?></td>
                </tr>
                <tr>
                    <td>5</td>
                	<td>Alamat Asal</td>
                    <td>:</td>
                	<td><?= $alamat_asal; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>:</td>
                    <td>Kec. <?= $nama_kecamatan; ?> Kel. <?= $nama_kelurahan; ?> - Kabupaten Bandung Barat</td>
                </tr>
                <tr>
                    <td>6</td>
                	<td>Alamat Kerja</td>
                    <td>:</td>
                	<td><?= $alamat_kerja; ?></td>
                </tr>
                <tr>
                    <td></td>
                	<td>Embarsi / Debarsi</td>
                    <td>:</td>
                	<td><?= $embarsi.' / '.$debarsi; ?></td>
                </tr>
            </tbody>
        </table>
		<table class="table" style="width: 100%;">
            <tbody>
                <tr>
                    <td colspan="4">II. PPTKIS Yang Menempatkan</td>
                </tr>
                <tr>
                    <td width="5%">1</td>
                	<td width="30%">Nama</td>
                    <td width="2%">:</td>
                	<td width="63%"><?= $nm_pptkis; ?></td>
                </tr>
                <tr>
                    <td>2</td>
                	<td>Negara Penempatan</td>
                    <td>:</td>
                	<td><?= $negara; ?></td>
                </tr>
                <tr>
                    <td>3</td>
                	<td>Mulai Bekerka di Luar Negeri</td>
                	<td>:</td>
                    <td><?= date("d-m-Y", strtotime($mulai_kerja));; ?></td>
                </tr>
                <tr>
                    <td>4</td>
                	<td>Tanggal Kembali Ke Daerah Asal</td>
                    <td>:</td>
                	<td><?= date("d-m-Y", strtotime($akhir_kerja)); ?></td>
                </tr>
                <tr>
                    <td>5</td>
                	<td>Lama Bekerja</td>
                	<td>:</td>
                    <td><?= $lama_kerja; ?></td>
                </tr>
            </tbody>
        </table>
        <table class="table" style="width: 100%;">
            <tbody>
                <tr>
                    <td colspan="4">III. Agency</td>
                </tr>
                <tr>
                    <td width="5%">1</td>
                	<td width="30%">Nama Agency</td>
                    <td width="2%">:</td>
                	<td><?= $nm_agency; ?></td>
                </tr>
                <tr>
                    <td>2</td>
                	<td>Alamat</td>
                    <td>:</td>
                	<td><?= $alamat_agency; ?></td>
                </tr>
            </tbody>
        </table>
        <table class="table" style="width: 100%;">
            <tbody>
                <tr>
                    <td colspan="4">IV. Pengguna Jasa TKI</td>
                </tr>
                <tr>
                    <td width="5%">1</td>
                	<td width="30%">Nama Majikan</td>
                	<td width="20%">: a. Awal</td>
                	<td width="50%">: <?= $f_nama_mjk; ?></td>
                </tr>
                <tr>
                    <Td></Td>
                	<td></td>
                	<td>  b. Akhir</td>
                    <td>: <?= $l_nama_mjk; ?></td>
                </tr>
                <tr>
                    <td></td>
                	<td></td>
                	<td>  c. Jumlah Majikan</td>
                    <td>: <?= $jml_mjk; ?> Orang</td>
                </tr>
                <tr>
                    <td>2</td>
                	<td>Nama Badan usaha</td>
                	<td></td>
                    <td>: <?= $badan_usaha; ?></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Alamat</td>
                    <td></td>
                    <td>: <?= $alamat_mjk; ?></td>
                </tr>
                <tr>
                    <td>4</td>
                	<td>Nomor ID</td>
                	<td></td>
                    <td width="63%">:<?= $no_id; ?></td>
                </tr>
            </tbody>
        </table>
        <br>
        <table class="table" style="width: 100%;">
            <tbody>
                <tr>
                    <td>V. MASALAH YANG DILAPORKAN</td>
                </tr>
                <tr>
                    <td><?= $masalah; ?></td>
                </tr>
            </tbody>
        </table>
        <table class="table" style="width: 100%;">
            <tbody>
                <tr>
                    <td>VI. TUNTUTAN</td>
                </tr>
                <tr>
                    <td><?= $tuntutan; ?></td>
                </tr>
            </tbody>
        </table>
        <div>
            <table>
                <tr>
                    <td colspan="2"><p style="padding: 15px; font-size: 14px">Demikian Surat Pengaduan ini saya buat dengan sesungguhnya secara sadar, untuk dipergunakan sebagaimana mestinya.</p></td>
                </tr>
                <tr>
                    <td><center>Petugas Yang Menangani</center></td>
                    <td><center>Yang Mengadu</center></td>
                </tr>
                <tr>
                    <td style="padding: 10px;"><center><img width="150px" src="<?= base_url('uploads/ttd/'.$paraf); ?>"></center></td>
                    <td style="padding: 10px;"><center></td>
                </tr>
                <tr>
                    <td><center><?= $nama_op; ?></center></td>
                    <td><center><?= $nama_pengadu; ?></center></td>
                </tr>
            </table>
        </div>
        
        <table class="table" style="width: 100%;">
            <thead>
                <tr>
                    <th colspan="3">Berkas Yang Dilampiran</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($bukti_pengaduan as $u) { ?>
                <tr>
                    <td class="text-center" scope="row" width="10%"><?= $no++; ?></td>
                    <td><?= $u->path; ?></td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a href="<?= base_url('uploads/bukti/'.$u->path); ?>" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Download">
                                <i class="fa fa-download"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
	</div>
</body>