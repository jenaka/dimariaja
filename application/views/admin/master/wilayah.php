<div class="content">
    <div class="row">
        <div class="col-lg-4">
            <div class="block block-themed block-rounded">
                <div class="block-header bg-gd-emerald">
                    <h3 class="block-title">Tambah Wilayah</h3>
                </div>
                <div class="block-content">
                    <form action="<?php echo base_url('master/wilayah/tambah_kecamatan'); ?>" method="post" id="form">
                        <input type="hidden" value="" name="id_kecamatan" />
                        <div class="form-group">
                            <label class="col-form-label">Nama Kecamatan</label>
                            <input type="text" class="form-control" name="nama_kecamatan" placeholder="Nama Kecamatan" value="<?php echo $nama_kecamatan; ?>">
                            <div class="form-text text-danger"><?php echo form_error('nama_kecamatan') ?></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn bg-gd-emerald text-white btn-block">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    	<div class="col-lg-8">
	        <!-- Default Elements -->
	        <div class="block block-themed block-rounded">
	            <div class="block-header bg-gd-emerald">
                    <h3 class="block-title">List Wilayah</h3>
                    <div class="">
	                   
                    </div>
	            </div>
	            <div class="block-content">
	               <table class="table table-bordered table-striped table-vcenter wilayah">
                        <thead>
                            <tr>
                                <th class="text-center"></th>
                                <th>Nama Kecamatan</th>
                                <th>Jumlah Desa</th>
                                <th class="text-center" style="width: 25%;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        	$no = 1;
					        foreach ($wilayah_data as $data){
					        ?>
                            <tr>
                                <td class="text-center"><?php echo $no++; ?></td>
                                <td class="font-w600"><?php echo htmlspecialchars($data->nama_kecamatan,ENT_QUOTES,'UTF-8');?></td>
                                <td class="font-w600"><?php echo htmlspecialchars($data->total,ENT_QUOTES,'UTF-8');?></td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-secondary" href="<?= base_url('wilayah/desa/'.$data->id_kecamatan); ?>" data-toggle="tooltip" title="Lihat Data Desa/Kelurahan"><i class="si si-eye"></i></a>
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit Data" onclick="edit(<?php echo $data->id_kecamatan;?>)"><i class="si si-note"></i></a>
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Hapus Data"  onclick="hapus(<?php echo $data->id_kecamatan;?>)">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
	            </div>
	        </div>
	        <!-- END Default Elements -->
	    </div>
    </div>
</div>

<script type="text/javascript">
    var save_method;
    var table;

    function edit(id) {
        save_method = 'update';
        $('#form')[0].reset();
        $('#form').attr('action', "<?php echo base_url('master/wilayah/update_kecamatan');?>");
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url: "<?php echo base_url(); ?>master/wilayah/edit_kecamatan/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {

                $('[name="id_kecamatan"]').val(data.id_kecamatan);
                $('[name="nama_kecamatan"]').val(data.nama_kecamatan);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function hapus(id) {
    swal({
      title: "Anda Yakin?",
      text: false,
      icon: "warning",
      buttons: ["Batal!", "Hapus!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>master/wilayah/hapus_kecamatan/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil dihapus",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
            }
        });
      } else {
        window.setTimeout(function(){ 
            location.reload();
        } ,1500);
      }
    });
    }

</script>