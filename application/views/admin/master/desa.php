<div class="content">
    <div class="row">
        <div class="col-lg-4">
            <div class="block block-themed block-rounded">
                <div class="block-header bg-gd-emerald">
                    <h3 class="block-title">Tambah Wilayah</h3>
                </div>
                <div class="block-content">
                    <form action="<?php echo base_url('master/wilayah/tambah_kelurahan'); ?>" method="post" id="form">
                        <input type="hidden" value="" name="id_kelurahan" />
                        <div class="form-group">
                            <label class="col-form-label">Nama kelurahan</label>
                            <input type="text" class="form-control" name="nama_kelurahan" placeholder="Nama kelurahan" value="<?php echo $nama_kelurahan; ?>">
                            <div class="form-text text-danger"><?php echo form_error('nama_kelurahan') ?></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-alt-primary btn-block">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    	<div class="col-lg-8">
	        <!-- Default Elements -->
	        <div class="block block-themed block-rounded">
	            <div class="block-header bg-gd-emerald">
                    <h3 class="block-title">List Wilayah</h3>
                    <div class="">
	                   
                    </div>
	            </div>
	            <div class="block-content">
	               <table class="table table-bordered table-striped table-vcenter wilayah">
                        <thead>
                            <tr>
                                <th class="text-center"></th>
                                <th>Nama kelurahan</th>
                                <th class="text-center" style="width: 25%;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        	$no = 1;
					        foreach ($desa_data as $data){
					        ?>
                            <tr>
                                <td class="text-center"><?php echo $no++; ?></td>
                                <td class="font-w600"><?php echo htmlspecialchars($data->nama_kelurahan,ENT_QUOTES,'UTF-8');?></td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit Data" onclick="edit(<?php echo $data->id_kelurahan;?>)"><i class="si si-note"></i></a>
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Hapus Data"  onclick="hapus(<?php echo $data->id_kelurahan;?>)">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
	            </div>
	        </div>
	        <!-- END Default Elements -->
	    </div>
    </div>
</div>

<script type="text/javascript">
    var save_method;
    var table;

    function edit(id) {
        save_method = 'update';
        $('#form')[0].reset();
        $('#form').attr('action', "<?php echo base_url('master/wilayah/update_kelurahan');?>");
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url: "<?php echo base_url(); ?>master/wilayah/edit_kelurahan/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {

                $('[name="id_kelurahan"]').val(data.id_kelurahan);
                $('[name="nama_kelurahan"]').val(data.nama_kelurahan);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function hapus(id) {
    swal({
      title: "Anda Yakin?",
      text: false,
      icon: "warning",
      buttons: ["Batal!", "Hapus!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>master/wilayah/hapus_kelurahan/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil dihapus",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
            }
        });
      } else {
        window.setTimeout(function(){ 
            location.reload();
        } ,1500);
      }
    });
    }

</script>