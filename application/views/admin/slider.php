<div class="content">
    <div class="block block-rounded block-transparent bg-gd-sea">
        <div class="block-content">
            <div class="py-20 text-center">
                <h1 class="font-w700 text-white mb-10"><?php echo $title; ?></h1>
                <h2 class="h4 font-w400 text-white-op"><?php echo $sub; ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <!-- Default Elements -->
            <div class="block block-rounded">
                <div class="block-content">
                   
                </div>
            </div>
            <!-- END Default Elements -->
        </div>
    </div>
</div>
<script type="text/javascript">

    function detail(id){
        $.ajax({
            url : "<?php echo site_url('admin/kontak/detail/')?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="nama"]').val(data.nama);
                $('[name="email"]').val(data.email);
                $('[name="subjek"]').val(data.subjek);
                $('[name="pesan"]').val(data.pesan);
                $('#detail_pesan').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    }

    function edit(id){
        save_method = 'update';
        $('#form_guest')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('admin/pelapor/edit/')?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id_pelapor"]').val(data.id_pelapor);
                $('[name="full_name"]').val(data.nama);
                $('[name="email_address"]').val(data.email);
                $('[name="phone"]').val(data.phone);
                $('#password_field').hide();
                $('#re_password_field').hide();
                $('#modal_form').modal('show');
                $('#modal_title').text('Perbaharui Pelapor');
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    }

    function simpan(){
        $('#btnSave').text('<?= lang('save_btn'); ?>');
        $('#btnSave').attr('disabled',true);
        var url;

        if(save_method == 'add') {
            url = "<?php echo site_url('admin/guest/simpan')?>";
        } else {
            url = "<?php echo site_url('admin/guest/update')?>";
        }

        var formData = new FormData($('#form_guest')[0]);
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data){

                if(data.status){
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil disimpan",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){
                        location.reload();
                    } ,1500);
                }else{
                    for (var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
                $('#btnSave').text('<?= lang('save_btn'); ?>');
                $('#btnSave').attr('disabled',false);
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error adding / update data');
                $('#btnSave').text('save');
                $('#btnSave').attr('disabled',false);

            }
        });
    }

    function hapus(id) {
      swal({
        title: "Anda Yakin?",
        text: "Pesan Yang Dihapus Tidak Bisa Dikembalikan",
        icon: "warning",
        buttons: ["Batal", "Hapus!"],
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajax({
              url: "<?php echo base_url('admin/kontak/delete/'); ?>" + id,
                  type: "POST",
                  dataType: "JSON",
                  success: function(data) {
                      //if success reload ajax table
                      $('#modal_form').modal('hide');
                      swal({
                          title: "Berhasil",
                          text: "Data berhasil dihapus",
                          timer: 3000,
                          buttons: false,
                          icon: 'success'
                      });
                      window.setTimeout(function(){
                          location.reload();
                      } ,1500);
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                      alert('Error deleting data');
              }
          });
        } else {
          window.setTimeout(function(){
              location.reload();
          } ,1500);
        }
      });
    }

</script>

<div class="modal fade" id="detail_pesan" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-gd-sea">
                    <h3 class="block-title">Detail Kontak Pesan</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <form id="form_guest" class="form-horizontal">
                        <input type="hidden" value="" name="id"/>
                        <div class="form-group">
                            <label class="col-form-label">Dari</label>
                            <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap" readonly="readonly">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Alamat Email" readonly="readonly">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Subjek</label>
                            <input type="text" class="form-control" name="subjek" placeholder="No. Handphone" readonly="readonly">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Pesan</label>
                            <textarea class="form-control" name="pesan" readonly="readonly"></textarea>
                            <span class="text-danger"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->


<div class="modal fade" id="detail_pelapor" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-gd-sea">
                    <h3 class="block-title">Detail Pelapor</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                   <form id="form_guest" class="form-horizontal">
                        <input type="hidden" value="" name="id_pelapor"/>
                        <div class="form-group">
                            <label class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" name="full_name" placeholder="Nama Lengkap" readonly="readonly">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Email</label>
                            <input type="email" class="form-control" name="email_address" placeholder="Alamat Email" readonly="readonly">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">No. Hanphone</label>
                            <input type="text" class="form-control" name="phone" placeholder="No. Handphone" readonly="readonly">
                            <span class="text-danger"></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><!-- /.modal -->
