<div class="content">
    <div class="block block-rounded block-transparent bg-gd-sea">
        <div class="block-content">
            <div class="py-20 text-center">
                <h1 class="font-w700 text-white mb-10"><?php echo $title; ?></h1>
                <h2 class="h4 font-w400 text-white-op"><?php echo $sub; ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
    	<div class="col-lg-12">
	        <div class="block">
                <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#info-umum">Informasi Umum</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#ubah-kata-sandi">Ubah Kata Sandi</a>
                    </li>
                </ul>
                <div class="block-content tab-content pb-30">
                    <div class="tab-pane active" id="info-umum" role="tabpanel">
                        <div class="row justify-content-center">
                            <div class="col-lg-10">
                                <form action="<?= base_url('admin/pengaturan'); ?>" method="post">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" >Nama Lengkap</label>
                                        <div class="col-lg-8">
                                            <input type="text" id="nama" class="form-control <?php if(form_error('nama') !== ''){ echo 'is-invalid'; } ?>" name="nama" value="<?php echo $nama; ?>"  placeholder="Full Name">
                                            <div class="form-text text-danger"><?php echo form_error('nama') ?></div>
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" >Email</label>
                                        <div class="col-lg-8">
                                            <input type="email" id="email" class="form-control <?php if(form_error('email') !== ''){ echo 'is-invalid'; } ?>" name="email" value="<?php echo $email; ?>"  placeholder="Email">
                                            <div class="form-text text-danger"><?php echo form_error('email') ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" >Username</label>
                                        <div class="col-lg-8">
                                            <input type="text" id="username" class="form-control <?php if(form_error('username') !== ''){ echo 'is-invalid'; } ?>" name="username" value="<?php echo $username; ?>"  placeholder="username">
                                            <div class="form-text text-danger"><?php echo form_error('username') ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" >No. Handphone</label>
                                        <div class="col-lg-8">
                                            <input type="text" id="phone" class="form-control <?php if(form_error('phone') !== ''){ echo 'is-invalid'; } ?>" name="phone" value="<?php echo $phone; ?>"  placeholder="No. Handphone">
                                            <div class="form-text text-danger"><?php echo form_error('phone') ?></div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-12">
                                            <button type="submit" class="btn bg-gd-sea text-white btn-lg btn-block">Simpan Perubahan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="ubah-kata-sandi" role="tabpanel">
                        <div class="row justify-content-center">
                            <div class="col-lg-10">
                                <form class="validasi-ubah-sandi" action="<?= base_url('admin/pengaturan/ubah_pw');?>" method="post">
                                  <input hidden type="email" name="email" value="<?= $email;?>">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" >Kata Sandi Lama</label>
                                        <div class="col-lg-8">
                                            <input type="password" id="ubah-lama" class="form-control <?php if(form_error('ubah-lama') !== ''){ echo 'is-invalid'; } ?>" name="ubah-lama"  placeholder="Kata Sandi Lama">
                                            <div class="form-text text-danger"><?php echo form_error('ubah-lama') ?></div>
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" >Kata Sandi Baru</label>
                                        <div class="col-lg-8">
                                            <input type="password" id="ubah-baru" class="form-control <?php if(form_error('ubah-baru') !== ''){ echo 'is-invalid'; } ?>" name="ubah-baru" placeholder="Kata Sandi Baru">
                                            <div class="form-text text-danger"><?php echo form_error('ubah-baru') ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" >Konfirmasi Kata Sandi Baru</label>
                                        <div class="col-lg-8">
                                            <input type="password" id="ubah-knf" class="form-control <?php if(form_error('ubah-knf') !== ''){ echo 'is-invalid'; } ?>" name="ubah-knf" placeholder="Konfirmasi Kata Sandi Baru">
                                            <div class="form-text text-danger"><?php echo form_error('ubah-knf') ?></div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-12">
                                            <button type="submit" class="btn bg-gd-sea text-white btn-lg btn-block">Simpan Perubahan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
    </div>
</div>