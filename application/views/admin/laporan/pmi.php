<div class="content" style="max-width: 90%">
    <div class="row">
        <div class="col-lg-12">
            <!-- Default Elements -->
            <div class="block block-themed block-rounded">
                <div class="block-header bg-gd-emerald">
                    <h3 class="block-title"><?= $title; ?></h3>
                </div>
                <div class="block-content">
                    <?php if($pengaduan_data == ''){ ?>
                    <form action="<?= $action; ?>" method="POST">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" >Dari Tanggal</label>
                                <div class="col-lg-8">
                                    <input type="text" class="js-datepicker form-control <?php if(form_error('tgl_mulai') !== ''){ echo 'is-invalid'; } ?>" id="tgl_mulai" name="tgl_mulai" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="Dari Tanggal">
                                    <div class="form-text text-danger"><?php echo form_error('tgl_mulai') ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" >Sampai Tanggal</label>
                                <div class="col-lg-8">
                                    <input type="text" class="js-datepicker form-control <?php if(form_error('tgl_akhir') !== ''){ echo 'is-invalid'; } ?>" id="tgl_akhir" name="tgl_akhir" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="Sampai Tanggal">
                                    <div class="form-text text-danger"><?php echo form_error('tgl_akhir') ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn bg-gd-emerald text-white btn-lg btn-block">Cari</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                    <?php }else{ ?>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <center><h3>Data Pengaduan Kasus Dari Tanggal <?= $tgl_mulai.' s/d '.$tgl_akhir; ?> </h3></center>
                            <div class="row" style="margin-bottom: 15px;">
                                <div class="col-lg-12">               
                                    <a class="btn btn-rounded btn-alt-secondary float-right" href="#" style="margin-left: 15px;">
                                        <i class="si si-doc text-primary mx-5"></i>
                                        <span class="d-none d-sm-inline"> PDF</span>
                                    </a>
                                    <a class="btn btn-rounded btn-alt-secondary float-right" href="#">
                                        <i class="fa fa-list text-primary mx-5"></i>
                                        <span class="d-none d-sm-inline"> Excel</span>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-bordered table-vcenter">
                                        <thead>
                                            <tr>
                                                <th rowspan="3" class="text-center" style="width: 4%">NO</th>
                                                <th rowspan="3" class="text-center"><center>NAMA PPTKIS</center></th>
                                                <th><center>DASAR SURAT DARI/</center></th>
                                                <th><center>NAMA TKI &</center></th>
                                                <th width="40%"><center>PERMASALAHAN TKI</center></th>
                                                <th colspan="2"><center>TINDAK LANJUT DI BP3TKI BANDUNG</center></th>
                                                <th colspan="2"><center>MONITORING STATUS PENGADUAN</center></th>
                                                <th rowspan="3"><center>KET</center></th>
                                            </tr>
                                            <tr style="border-top:1px solid #eaecee; line-height: 5px">
                                                <th><center>NOMOR & TGL SURAT</center></th>
                                                <th><center>DAERAH ASAL</center></th>
                                                <th style="line-height: 30px;" rowspan="2"><center>PRA / MASAPENEMPATAN / PURNA</center></th>
                                                <th style="line-height: 30px;" rowspan="2"><center>1</center></th>
                                                <th style="line-height: 30px;" rowspan="2"><center>2</center></th>
                                                <th style="line-height: 30px;" rowspan="2"><center>1</center></th>
                                                <th style="line-height: 30px;" rowspan="2"><center>2</center></th>
                                            </tr>
                                            <tr style="border-top:1px solid #eaecee; line-height: 5px">
                                                <th><center>NMR & TGL SURAT MASUK</center></th>
                                                <th><center>NEGARA TUJUAN</center></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 1;
                                            foreach ($pengaduan_data as $data){

                                            ?>
                                            <tr>
                                                <td class="text-center"><?php echo $no++; ?></td>
                                                <td class="font-w600"><?php echo $data->nm_pptkis; ?></td>
                                                <td class="font-w600"></td>
                                                <td class="font-w600"><?php echo $data->nama; ?><br>Kab. Bandung Barat<br><?= $data->negara;?></td>
                                                <td class="font-w600"><?php echo $data->masalah; ?></td>
                                                <td class="font-w600"></td>
                                                <td class="font-w600"></td>
                                                <td class="font-w600"></td>
                                                <td class="font-w600"></td>
                                                <td class="font-w600"></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!-- END Default Elements -->
        </div>
    </div>
</div>