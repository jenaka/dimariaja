<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Dimariaja - Tempat Pengaduan Kasus Imigran Indonesia</title>

        <meta name="description" content="SIDUKA-PMI KBB - Sistem Informasi Pengaduan Purna Pekerja Migran Indonesia">
        <meta name="author" content="Disnaker">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="SIDUKA-PMI KBB - Sistem Informasi Pengaduan Purna Pekerja Migran Indonesia">
        <meta property="og:site_name" content="SIDUKA-PMI KBB">
        <meta property="og:description" content="SIDUKA-PMI KBB - Sistem Informasi Pengaduan Purna Pekerja Migran Indonesia">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?= base_url('assets/img/favicons/favicon.png'); ?>">
        <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url('assets/img/favicons/favicon-192x192.png'); ?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('assets/img/favicons/apple-touch-icon-180x180.png'); ?>">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <link rel="stylesheet" id="css-main" href="<?= base_url('assets/css/codebase.min.css'); ?>">
    </head>
    <body>

        <div id="page-container" class="main-content-boxed">
            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="bg-body-dark">
                    <div class="row mx-0 justify-content-center">
                        <div class="hero-static col-lg-6 col-xl-4">
                            <div class="content content-full overflow-hidden">
                                <!-- Header -->
                                <div class="py-60 text-center" style="margin-top: 10%; margin-bottom: 10%">
                                    <img src="<?php echo base_url('assets/img/logo.png'); ?>" width="45%">
                                </div>
                                <!-- END Header -->

                                <!-- Sign In Form -->
                                <?php echo form_open('admin/login/validate_login'); ?>
                                    <div class="block block-themed block-rounded block-shadow">
                                        <div class="block-header bg-gd-sea">
                                            <h3 class="block-title">Silahkan Login</h3>
                                            <div class="block-options">
                                                <button type="button" class="btn-block-option">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="block-content">
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <label for="login-username">Email / Username</label>
                                                    <input type="text" class="form-control" id="login-username" name="identity">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <label for="login-password">Password</label>
                                                    <input type="password" class="form-control" id="login-password" name="password">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col-sm-12 text-sm-right push">
                                                    <button type="submit" class="btn bg-gd-sea btn-block text-white">
                                                        <i class="si si-login mr-10 text-white"></i> Login
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END Sign In Form -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="<?= base_url('assets/js/core/jquery.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/core/popper.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/core/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/core/jquery.slimscroll.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/core/jquery.scrollLock.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/core/jquery.appear.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/core/jquery.countTo.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/core/js.cookie.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/codebase.js'); ?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
        <!-- Page JS Plugins -->
        <script src="<?= base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js'); ?>"></script>

        <!-- Page JS Code -->
        <script src="<?= base_url('assets/js/pages/op_auth_signin.js'); ?>"></script>
        <?php if ($this->session->userdata('success_message')): ?>
            <script>
                swal({
                    title: "Berhasil",
                    text: "<?php echo $this->session->userdata('success_message'); ?>",
                    timer: 3000,
                    button: false,
                    icon: 'success'
                });
            </script>
        <?php endif; ?>
        <?php if ($this->session->userdata('error_message')): ?>
            <script>
                swal({
                    title: "Oops...",
                    text: "<?php echo $this->session->userdata('error_message'); ?>",
                    timer: 3000,
                    button: false,
                    icon: 'error'
                });
            </script>
        <?php endif; ?>
    </body>
</html>
