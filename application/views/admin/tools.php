<div class="content">
    <div class="row">
        <div class="col-sm-6">
            <a class="block block-rounded block-transparent bg-gray-lighter" href="<?= base_url('tools/backup'); ?>">
                <div class="block-content block-content-full">
                    <div class="py-30 text-center">
                        <div class="item item-2x item-circle bg-primary text-white mx-auto">
                            <i class="fa fa-download"></i>
                        </div>
                        <div class="h4 pt-20 mb-0">Cadangkan Database</div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6">
            <a class="block block-rounded block-transparent bg-gray-lighter" href="<?= base_url('tools/optimize'); ?>">
                <div class="block-content block-content-full">
                    <div class="py-30 text-center">
                        <div class="item item-2x item-circle bg-success text-white mx-auto">
                            <i class="fa fa-download"></i>
                        </div>
                        <div class="h4 pt-20 mb-0">Optimasi Database</div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
