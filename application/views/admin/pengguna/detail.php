<div class="content">
    <div class="row">
    	<div class="col-lg-12">
	        <!-- Default Elements -->
	        <div class="block block-themed block-rounded">
	            <div class="block-header bg-gd-emerald">
                    <h3 class="block-title"><?= $title; ?></h3>
                </div>
	            <div class="block-content">
	                <form action="<?= $action; ?>" method="post">
	                	<div class="row justify-content-center">
	                		<div class="col-md-6">
	                			<input type="hidden" name="id" value="<?= $id;?>">
	                			<div class="form-group row">
		                            <label class="col-lg-4 col-form-label" >Username</label>
		                            <div class="col-lg-8">
		                                <input type="text" id="username" class="form-control <?php if(form_error('username') !== ''){ echo 'is-invalid'; } ?>" name="username" value="<?php echo $username; ?>"  placeholder="username" disabled="disabled">
		                                <div class="form-text text-danger"><?php echo form_error('username') ?></div>
		                            </div>
		                        </div>
	                			<div class="form-group row">
		                            <label class="col-lg-4 col-form-label" >Nama Lengkap</label>
		                            <div class="col-lg-8">
		                                <input type="text" id="nama" class="form-control <?php if(form_error('nama') !== ''){ echo 'is-invalid'; } ?>" name="nama" value="<?php echo $nama; ?>"  placeholder="Full Name" disabled="disabled">
		                                <div class="form-text text-danger"><?php echo form_error('nama') ?></div>
		                            </div>
		                        </div>
		                         <div class="form-group row">
		                            <label class="col-lg-4 col-form-label" >Email</label>
		                            <div class="col-lg-8">
		                                <input type="email" id="email" class="form-control <?php if(form_error('email') !== ''){ echo 'is-invalid'; } ?>" name="email" value="<?php echo $email; ?>"  placeholder="Email" disabled="disabled">
		                                <div class="form-text text-danger"><?php echo form_error('email') ?></div>
		                            </div>
		                        </div>
		                        <div class="form-group row">
		                            <label class="col-lg-4 col-form-label" >No. Handphone</label>
		                            <div class="col-lg-8">
	                                    <div class="input-group">
	                                        <span class="input-group-addon">+62</span>
	                                        <input type="text" class="form-control" id="telp" name="telp" placeholder="No. Handphone" value="<?php echo $telp; ?>" disabled="disabled">
	                                    </div>
	                                    <div class="form-text text-danger"><?php echo form_error('telp') ?></div>
	                                </div>
		                        </div>
		                        <div class="form-group row">
		                            <label class="col-lg-4 col-form-label" >Jabatan</label>
		                            <div class="col-lg-8">
	                                    <select class="form-control <?php if(form_error('id_akses') !== ''){ echo 'is-invalid'; } ?>" name="id_akses" disabled="disabled">
		                                	<option value="">Pilih</option>
					                            <?php
					                            foreach ($jabatan_data as $data) {
					                                ?>
					                                <option <?php if($id_akses == $data->id){ echo 'selected="selected"'; } ?> value="<?php echo $data->id ?>"><?php echo $data->nama ?></option>
					                                <?php
					                            }
					                            ?>
		                                </select>
	                                </div>
		                        </div>
		                        <?php if($this->uri->segment(2)=="tambah"){ ?>

		                        <div class="form-group row">
		                            <label class="col-lg-4 col-form-label" >Password</label>
		                            <div class="col-lg-8">
		                                <input type="password" id="password" class="form-control <?php if(form_error('password') !== ''){ echo 'is-invalid'; } ?>" name="password" value="<?php echo $password; ?>"  placeholder="password">
		                                <div class="form-text text-danger"><?php echo form_error('password') ?></div>
		                            </div>
		                        </div>
		                        <div class="form-group row">
		                            <label class="col-lg-4 col-form-label" >Konfirmasi Password</label>
		                            <div class="col-lg-8">
		                                <input type="password" id="password_knf" class="form-control <?php if(form_error('password_knf') !== ''){ echo 'is-invalid'; } ?>" name="password_knf" value="<?php echo $password_knf; ?>"  placeholder="Password Confirmation">
		                                <div class="form-text text-danger"><?php echo form_error('password_knf') ?></div>
		                            </div>
		                        </div>
		                    <?php } ?>
	                		</div>
	                    </div>
	                    <div class="row justify-content-center" style="padding-top: 30px;padding-bottom: 25px;">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <a class="btn btn-danger btn-lg btn-block" href="<?php echo base_url('pengguna');?>">Kembali</a>
                                    </div>
		                        </div>
                            </div>
	                    </div>
                    </form>
	            </div>
	        </div>
	        <!-- END Default Elements -->
	    </div>
    </div>
</div>