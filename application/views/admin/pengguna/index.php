<div class="content">
    <div class="block block-rounded block-transparent bg-gd-sea">
        <div class="block-content">
            <div class="py-20 text-center">
                <h1 class="font-w700 text-white mb-10"><?php echo $title; ?></h1>
                <h2 class="h4 font-w400 text-white-op"><?php echo $sub; ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
    	<div class="col-lg-12">
	        <!-- Default Elements -->
	        <div class="block block-rounded">
	            <div class="block-content pb-15">
                    <div class="push">
                        <a class="btn btn-rounded btn-alt-secondary" href="<?php echo base_url('admin/pengguna/trash');?>">
                            <i class="fa fa-archive text-danger mx-5"></i>
                            <span class="d-none d-sm-inline"> Pengguna Dihapus</span>
                        </a>
                        <button class="btn btn-rounded btn-alt-secondary float-right" onclick="tambah()">
                            <i class="si si-plus text-primary mx-5"></i>
                            <span class="d-none d-sm-inline"> Tambah Pengguna</span>
                        </button>
                    </div>
	                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="text-center"></th>
                                <th>Nama</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>No Telepon</th>
                                <th>Status</th>
                                <th class="text-center" style="width: 15%;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        	$no = 1;
					        foreach ($users_data as $data){
                                if($data->active == 1){
                                    $active = '<button class="btn btn-sm btn-success mr-5 mb-5" style="curson:pointer;" onclick="deaktivasi('.$data->id.')">Aktif</button>';
                                }else{
                                    $active = '<button class="btn btn-sm btn-danger mr-5 mb-5" style="curson:pointer;" onclick="aktivasi('.$data->id.')">Tidak Aktif</button>';
                                }
					        ?>
                            <tr>
                                <td class="text-center"><?php echo $no++; ?></td>
                                <td class="font-w600"><?php echo $data->nama; ?></td>
                                <td class="font-w600"><?php echo $data->username; ?></td>
                                <td class="font-w600"><?php echo $data->email; ?></td>
                                <td class="font-w600"><?php echo $data->telp; ?></td>
                                <td class="font-w600"><center><?= $active; ?></center></td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit Pengguna" onclick="edit(<?php echo $data->id;?>)">
                                        <i class="si si-note"></i>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Hapus Pengguna" onclick="hapus(<?php echo $data->id;?>)">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
	            </div>
	        </div>
	        <!-- END Default Elements -->
	    </div>
    </div>
</div>

<script type="text/javascript">
    function tambah(){
        save_method = 'add';
        $('#form_guest')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_form').modal('show')
        $('#modal_title').text('Tambah Pengguna Baru');
    }
 
    function edit(id){
        save_method = 'update';
        $('#form_guest')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('admin/pengguna/edit/')?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="nama"]').val(data.nama);
                $('[name="email"]').val(data.email);
                $('[name="username"]').val(data.username);
                $('[name="telp"]').val(data.telp);
                $('[name="id_akses"]').val(data.id_akses);
                $('#password_field').hide();
                $('#re_password_field').hide();
                $('#modal_form').modal('show');
                $('#modal_title').text('Perbaharui Pengguna');
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    }

    function simpan(){
        $('#btnSave').text('Menyimpan');
        $('#btnSave').attr('disabled',true);
        var url;
     
        if(save_method == 'add') {
            url = "<?php echo site_url('admin/pengguna/simpan')?>";
        } else {
            url = "<?php echo site_url('admin/pengguna/update')?>";
        }

        var formData = new FormData($('#form_guest')[0]);
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data){
     
                if(data.status){
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil disimpan",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                }else{
                    for (var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
                $('#btnSave').text('<?= lang('save_btn'); ?>');
                $('#btnSave').attr('disabled',false);
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error adding / update data');
                $('#btnSave').text('Simpan');
                $('#btnSave').attr('disabled',false);
     
            }
        });
    }


    function hapus(id) {
    swal({
      title: "Anda Yakin?",
      text: "Pengguna Yang Dihapus Di Simpan Di Keranjang Sampah",
      icon: "warning",
      buttons: ["Batal!", "Hapus!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>admin/pengguna/hapus/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil dihapus",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
            }
        });
      } else {
        window.setTimeout(function(){ 
            location.reload();
        } ,1500);
      }
    });
    }

    function aktivasi(id) {
    swal({
      title: "Aktifkan Pengguna?",
      text: "Pengguna Akan Bisa Digunakan Kembali",
      icon: "warning",
      buttons: ["Tidak!", "Ya!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>admin/pengguna/aktif/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Pengguna Berhasil Diaktifkan",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
            }
        });
      } else {
        window.setTimeout(function(){ 
            location.reload();
        } ,1500);
      }
    });
    }

    function deaktivasi(id) {
    swal({
      title: "Non Aktifkan Pengguna?",
      text: "Pengguna Tidak Akan Digunakan Kembali",
      icon: "warning",
      buttons: ["Tidak!", "Ya!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>admin/pengguna/nonaktif/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Pengguna Berhasil Di Non-Aktifkan",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
                }
        });
      } else {
        window.setTimeout(function(){ 
            location.reload();
        } ,1500);
      }
    });
    }
</script>

<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-gd-sea">
                    <h3 class="block-title" id="modal_title">Form Pengguna</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                   <form id="form_guest" class="form-horizontal">
                        <input type="hidden" value="" name="id"/> 
                        <div class="form-group">
                            <label class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Username</label>
                            <input type="text" class="form-control" name="username" placeholder="Username">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Alamat Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Alamat Email">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">No. Handphone</label>
                            <input type="text" class="form-control" name="telp" placeholder="No. Handphone">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Hak Akses</label>
                            <select name="id_akses" class="form-control">
                                <option value="">Pilih</option>
                                <?php
                                foreach ($jabatan_data as $data) {
                                    ?>
                                    <option value="<?php echo $data->id ?>"><?php echo $data->nama ?></option>
                                <?php } ?>
                            </select>
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group" id="password_field">
                            <label class="col-form-label">Kata Sandi</label>
                            <input type="password" class="form-control" name="password" placeholder="Kata Sandi">
                            <span class="text-danger"></span>
                        </div>
                        <div class="form-group" id="re_password_field">
                            <label class="col-form-label">Konfirmasi Kata Sandi</label>
                            <input type="password" class="form-control" name="re_password" placeholder="Konfirmasi Kata Sandi">
                            <span class="text-danger"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" class="btn btn-alt-primary" data-dismiss="modal" onclick="simpan()"></i>Simpan
                </button>
                <button type="button" class="btn btn-alt-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->