<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

<title>Dimariaja - Tempat Pengaduan Kasus Imigran Indonesia</title>

<meta name="description" content="Dimariaja - Tempat Pengaduan Kasus Imigran Indonesia">
<meta name="author" content="dimariaja">
<meta name="robots" content="noindex, nofollow">

<!-- Open Graph Meta -->
<meta property="og:title" content="Dimariaja - Tempat Pengaduan Kasus Imigran Indonesia">
<meta property="og:site_name" content="Dimariaja">
<meta property="og:description" content="Dimariaja - Tempat Pengaduan Kasus Imigran Indonesia">
<meta property="og:type" content="website">
<meta property="og:url" content="">
<meta property="og:image" content="">

<!-- Icons -->
<link rel="shortcut icon" href="<?= base_url('assets/img/favicons/favicon.png'); ?>">
<link rel="icon" type="image/png" sizes="192x192" href="<?= base_url('assets/img/favicons/favicon-192x192.png'); ?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('assets/img/favicons/apple-touch-icon-180x180.png'); ?>">
<!-- END Icons -->

<!-- Stylesheets -->
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" id="css-main" href="<?= base_url(); ?>assets/css/codebase.css">
<link rel="stylesheet" id="css-main" href="<?= base_url(); ?>assets/css/success_icon.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/js/plugins/datatables/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/js/plugins/slick/slick-theme.min.css">
<!-- Codebase Core JS -->
<script src="<?= base_url(); ?>assets/js/core/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/core/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

<style type="text/css">
.notif {
    position: absolute;
    top: 0px;
    right: -7px;
    color: white;
    border-radius: 50%;
    text-align: center;
    font-size: 11px;
    padding: 3px 5px;
    line-height: 0.9;
}

.scrollable-menu {
    height: auto;
    max-height: 300px;
    overflow-x: hidden;
}

</style>