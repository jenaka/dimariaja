<?php if($this->session->userdata('login_type') == 'Superadmin'){ ?>
<ul class="nav-main-header">
    <li>
        <a href="<?php echo base_url('admin/beranda');?>" class="<?php if($this->uri->segment(2)=="beranda"){echo 'active';}?>"><i class="si si-compass"></i><span class="sidebar-mini-hide">Beranda</span></a>
    </li>
    <li>
        <a href="<?php echo base_url('admin/pengaduan');?>" class="<?php if($this->uri->segment(2)=="pengaduan"){echo 'active';}?>"><i class="si si-note"></i><span class="sidebar-mini-hide">Pengaduan</span></a>
    </li>
    <li>
        <a href="<?php echo base_url('admin/pelapor');?>" class="<?php if($this->uri->segment(2)=="pelapor"){echo 'active';}?>"><i class="si si-user"></i><span class="sidebar-mini-hide">Pelapor</span></a>
    </li>
    <li>
        <a href="<?php echo base_url('admin/operator');?>" class="<?php if($this->uri->segment(2)=="operator"){echo 'active';}?>"><i class="si si-people"></i><span class="sidebar-mini-hide">Operator</span></a>
    </li>
    <li>
        <a href="<?php echo base_url('admin/pengguna');?>" class="<?php if($this->uri->segment(2)=="pengguna"){echo 'active';}?>"><i class="si si-people"></i><span class="sidebar-mini-hide">Pengguna</span></a>
    </li>
    <li>
        <a href="<?php echo base_url('admin/kontak');?>" class="<?php if($this->uri->segment(2)=="kontak"){echo 'active';}?>"><i class="si si-envelope"></i><span class="sidebar-mini-hide">Kontak</span></a>
    </li>
</ul>
<?php } ?>

<?php if($this->session->userdata('login_type') == 'BNP2TKI' || $this->session->userdata('login_type') == 'BP3TKI' || $this->session->userdata('login_type') == 'KEMENLU' || $this->session->userdata('login_type') == 'KEMNAKER' || $this->session->userdata('login_type') == 'DISNAKERTRANS-JABAR' || $this->session->userdata('login_type') == 'DISNAKERTRANS' || $this->session->userdata('login_type') == 'Operator'){ ?>
     <ul class="nav-main-header">
        <li>
            <a href="<?php echo base_url('admin/beranda');?>" class="<?php if($this->uri->segment(2)=="beranda"){echo 'active';}?>"><i class="si si-compass"></i><span class="sidebar-mini-hide">Beranda</span></a>
        </li>
        <li>
            <a href="<?php echo base_url('admin/pengaduan');?>" class="<?php if(uri_string()=="admin/pengaduan"){echo 'active';}?>"><i class="si si-note"></i><span class="sidebar-mini-hide">Pengaduan</span></a>
        </li>
    </ul>
<?php } ?>