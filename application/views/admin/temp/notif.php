<div class="btn-group" role="group">
    <button type="button" class="btn btn-circle btn-dual-secondary" id="page-header-themes-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-bell"></i>
        <span class="notif label bg-danger" id="total">0</span>
    </button>
    <div class="dropdown-menu min-width-300  dropdown-menu-right" aria-labelledby="page-header-themes-dropdown">
        <ul class="nav-users push scrollable-menu" id="notifnya" style="margin-bottom: 0px;">

        </ul>
    </div>
</div>
<?php if($this->session->userdata('login_type') == 'Superadmin' || $this->session->userdata('login_type') == 'Admin'){?>
<div class="btn-group mr-10" role="group">
    <button type="button" class="btn btn-circle btn-dual-secondary" id="page-header-themes-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-envelope"></i>
        <span class="notif label bg-danger" id="total_inbox">0</span>
    </button>
    <div class="dropdown-menu min-width-300  dropdown-menu-right" aria-labelledby="page-header-themes-dropdown">
        <ul class="nav-users push scrollable-menu" id="notif_inbox" style="margin-bottom: 0px;">

        </ul>
    </div>
</div>
<?php } ?>