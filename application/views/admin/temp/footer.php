<script src="<?php echo base_url('assets/js/core/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/core/jquery.scrollLock.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/core/jquery.appear.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/core/jquery.countTo.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/core/js.cookie.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/codebase.js');?>"></script>

<!-- Page JS Plugins -->
<script src="<?php echo base_url('assets/js/plugins/chartjs/Chart.bundle.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/plugins/datatables/dataTables.bootstrap4.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js');?>"></script>
<script src="<?= base_url(); ?>assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/js/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-chained/1.0.1/jquery.chained.js"></script>
<!-- Page JS Code -->
<script src="<?= base_url(); ?>assets/js/pages/be_tables_datatables.js"></script>
 <script>
    jQuery(function () {
        // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        Codebase.helpers(['datepicker', 'select2']);
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-chained/1.0.1/jquery.chained.js"></script>
<script>
    $("#kelurahan").chained("#kecamatan");

    function get_inbox(){
     $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>admin/kontak/get_inbox",
        dataType:'json',
        success: function(data){
         $("#total_inbox").text(""+data.total+"");
         $('#notif_inbox').html(data.notification);
         timer = setTimeout("get_inbox()",1000);
        }

       });  
    }

    $(document).ready(function(){
     get_inbox();
    });

    function ambilKomentar(){
     $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>admin/notifikasi/",
        dataType:'json',
        success: function(data){
         $("#total").text(""+data.total+"");
         $('#notifnya').html(data.notification);
         timer = setTimeout("ambilKomentar()",1000);
        }

       });  
    }

    $(document).ready(function(){
     ambilKomentar();
    });
</script>
 <?php if ($this->session->userdata('success_message')): ?>
    <script>
        swal({
            title: "Berhasil",
            text: "<?php echo $this->session->userdata('success_message'); ?>",
            timer: 3000,
            button: false,
            icon: 'success'
        });
    </script>
<?php endif; ?>
<?php if ($this->session->userdata('error_message')): ?>
    <script>
        swal({
            title: "Oops...",
            text: "<?php echo $this->session->userdata('error_message'); ?>",
            timer: 3000,
            button: false,
            icon: 'error'
        });
    </script>
<?php endif; ?>