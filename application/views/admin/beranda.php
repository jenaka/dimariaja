<div class="content">
    <div class="row justify-content-center">
        <div class="col-lg-10">
            <?php if($this->session->userdata('login_type') == 'Operator' || $this->session->userdata('login_type') == 'Admin' || $this->session->userdata('login_type') == 'Superadmin'){ ?>
            <div class="row gutters-tiny invisible" data-toggle="appear">
                <!-- Row #1 -->
                <div class="col-6 col-xl-6">
                    <a class="block block-link-shadow text-right" href="<?= base_url('pengaduan'); ?>">
                        <div class="block-content block-content-full clearfix">
                            <div class="float-left mt-10 d-none d-sm-block">
                                <i class="si si-note fa-3x text-body-bg-dark"></i>
                            </div>
                            <div class="font-size-h3 font-w600" data-toggle="countTo" data-speed="1000" data-to="<?= $total_kasus; ?>">0</div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">Pengaduan</div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-xl-6">
                    <a class="block block-link-shadow text-right" href="<?= base_url('pmi'); ?>">
                        <div class="block-content block-content-full clearfix">
                            <div class="float-left mt-10 d-none d-sm-block">
                                <i class="si si-refresh fa-3x text-body-bg-dark"></i>
                            </div>
                            <div class="font-size-h3 font-w600"><span data-toggle="countTo" data-speed="1000" data-to="<?= $kasus_proses; ?>">0</span></div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">Pengaduan Belum Di Proses</div>
                        </div>
                    </a>
                </div>
                <!-- END Row #1 -->
            </div>
        <?php } ?>
            <div class="row gutters-tiny invisible" data-toggle="appear">
                <!-- Row #2 -->
                <div class="col-lg-12">
                    <div class="block">
                        <div class="block-header">
                            <h3 class="block-title">
                                Statistik Pengaduan PMI <small>Tahun Ini</small>
                            </h3>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="pull-all">
                                <!-- Lines Chart Container -->
                                <canvas class="pengaduan_Graph"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Row #2 -->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var BePagesDashboard = function() {
    var initDashboardChartJS = function () {
        Chart.defaults.global.defaultFontColor              = '#555555';
        Chart.defaults.scale.gridLines.color                = "transparent";
        Chart.defaults.scale.gridLines.zeroLineColor        = "transparent";
        Chart.defaults.scale.display                        = false;
        Chart.defaults.scale.ticks.beginAtZero              = true;
        Chart.defaults.global.elements.line.borderWidth     = 2;
        Chart.defaults.global.elements.point.radius         = 5;
        Chart.defaults.global.elements.point.hoverRadius    = 7;
        Chart.defaults.global.tooltips.cornerRadius         = 3;
        Chart.defaults.global.legend.display                = false;

        // Chart Containers
        var chartDashboardLinesCon  = jQuery('.pengaduan_Graph');

        // Chart Variables
        var chartDashboardLines;

        // Lines Charts Data
        var chartDashboardLinesData = {
            labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            datasets: [
                {
                    label: 'Tahun ini',
                    fill: true,
                    backgroundColor: 'rgba(66,165,245,.25)',
                    borderColor: 'rgba(66,165,245,1)',
                    pointBackgroundColor: 'rgba(66,165,245,1)',
                    pointBorderColor: '#fff',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: 'rgba(66,165,245,1)',
                    data: <?php echo json_encode($total); ?>
                }
            ]
        };

        var chartDashboardLinesOptions = {
            scales: {
                yAxes: [{
                    ticks: {
                        suggestedMax: 50
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return ' ' + tooltipItems.yLabel + ' Pengaduan';
                    }
                }
            }
        };

        // Init Charts
        if ( chartDashboardLinesCon.length ) {
            chartDashboardLines  = new Chart(chartDashboardLinesCon, { type: 'line', data: chartDashboardLinesData, options: chartDashboardLinesOptions });
        }

    };

    return {
        init: function () {
            // Init Chart.js Charts
            initDashboardChartJS();
        }
    };
}();
jQuery(function(){ BePagesDashboard.init(); });
</script>