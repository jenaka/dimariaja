<?php 

class MY_Controller extends CI_Controller 
{

    function __construct(){
        parent::__construct();

  }

   function temp($content, $data = NULL){

        $data['header'] = $this->load->view('admin/temp/header', $data, TRUE);
        $data['menu'] = $this->load->view('admin/temp/menu', $data, TRUE);
        $data['menu_top'] = $this->load->view('admin/temp/menu_top', $data, TRUE);
        $data['notif'] = $this->load->view('admin/temp/notif', $data, TRUE);
        $data['content'] = $this->load->view('admin/'.$content, $data, TRUE);
        $data['footer'] = $this->load->view('admin/temp/footer', $data, TRUE);
        
        $this->load->view('admin/index', $data);
    }

     function admin($content, $data = NULL){

        $data['header'] = $this->load->view('admin/temp/header', $data, TRUE);
        $data['menu'] = $this->load->view('admin/temp/menu', $data, TRUE);
        $data['menu_top'] = $this->load->view('admin/temp/menu_top', $data, TRUE);
        $data['notif'] = $this->load->view('admin/temp/notif', $data, TRUE);
        $data['content'] = $this->load->view('admin/'.$content, $data, TRUE);
        $data['footer'] = $this->load->view('admin/temp/footer', $data, TRUE);
        
        $this->load->view('admin/index_admin', $data);
    }


    function front($content, $data = NULL){

        $data['header'] = $this->load->view('front/temp/header', $data, TRUE);
        $data['menu'] = $this->load->view('front/temp/menu', $data, TRUE);
        $data['menu_header'] = $this->load->view('front/temp/menu_header', $data, TRUE);
        $data['content'] = $this->load->view('front/'.$content, $data, TRUE);
        $data['footer'] = $this->load->view('front/temp/footer', $data, TRUE);
        
        $this->load->view('front/index', $data);
    }

    function pelapor($content, $data = NULL){

        $data['header'] = $this->load->view('front/temp/header', $data, TRUE);
        $data['menu'] = $this->load->view('front/temp/menu', $data, TRUE);
        $data['menu_header'] = $this->load->view('front/temp/menu_header', $data, TRUE);
        $data['menu_pelapor'] = $this->load->view('pelapor/menu_pelapor', $data, TRUE);
        $data['content'] = $this->load->view('pelapor/'.$content, $data, TRUE);
        $data['footer'] = $this->load->view('front/temp/footer', $data, TRUE);
        
        $this->load->view('pelapor/index', $data);
    }

}