-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 18, 2019 at 04:21 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_dimariaja`
--

-- --------------------------------------------------------

--
-- Table structure for table `baca_operator`
--

CREATE TABLE `baca_operator` (
  `id` int(11) NOT NULL,
  `id_pengaduan` int(11) NOT NULL,
  `id_operator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `baca_user`
--

CREATE TABLE `baca_user` (
  `id` int(11) NOT NULL,
  `id_pengaduan` int(11) NOT NULL,
  `id_users` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id_kecamatan` int(11) NOT NULL,
  `nama_kecamatan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id_kecamatan`, `nama_kecamatan`) VALUES
(1, 'Batujajar'),
(2, 'Cikalongwetan'),
(3, 'Cihampelas'),
(4, 'Cililin '),
(5, 'Cipatat'),
(6, 'Cipeundeuy'),
(7, 'Cipongkor'),
(8, 'Cisarua'),
(9, 'Gununghalu'),
(10, 'Ngamprah'),
(11, 'Padalarang'),
(12, 'Rongga'),
(13, 'Sindangkerta'),
(14, 'Lembang'),
(15, 'Saguling'),
(16, 'Parongpong');

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id_kelurahan` int(11) NOT NULL,
  `nama_kelurahan` varchar(150) NOT NULL,
  `id_kecamatan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelurahan`
--

INSERT INTO `kelurahan` (`id_kelurahan`, `nama_kelurahan`, `id_kecamatan`) VALUES
(1, 'Batujajar Barat', 1),
(2, 'Batujajar Timur', 1),
(3, 'Cangkorah', 1),
(4, 'Galanggang', 1),
(5, 'Giriasih', 1),
(6, 'Pangauban', 1),
(7, 'Selacau', 1),
(8, 'Cikalong', 2),
(9, 'Cipada', 2),
(10, 'Ciptagumati', 2),
(11, 'Cisomang Barat', 2),
(12, 'Ganjarsari', 2),
(13, 'Kanangasari', 2),
(14, 'Mandalamukti', 2),
(15, 'Mandalasari', 2),
(16, 'Mekarjaya', 2),
(17, 'Puteran', 2),
(18, 'Rende', 2),
(19, 'Tenjolaut', 2),
(20, 'Wangunjaya', 2),
(21, 'Cihampelas', 3),
(22, 'Cipatik', 3),
(23, 'Citapen', 3),
(24, 'Mekarjaya', 3),
(25, 'Mekarmukti', 3),
(26, 'Pataruman', 3),
(27, 'Singajaya', 3),
(28, 'Situwangi', 3),
(29, 'Tanjungjaya', 3),
(30, 'Tanjungwangi', 3),
(31, 'Batulayang', 4),
(32, 'Bongas', 4),
(33, 'Budiharja', 4),
(34, 'Cililin', 4),
(35, 'Karanganyar', 4),
(36, 'Karangtanjung', 4),
(37, 'Karyamukti', 4),
(38, 'Kidangpananjung', 4),
(39, 'Mukapayung', 4),
(40, 'Nanggerang', 4),
(41, 'Rancapanggung', 4),
(42, 'Cipatat', 5),
(43, 'Ciptaharja', 5),
(44, 'Cirawamekar', 5),
(45, 'Citatah', 5),
(46, 'Gunungmasigit', 5),
(47, 'Kertamukti', 5),
(48, 'Mandalasari', 5),
(49, 'Mandalawangi', 5),
(50, 'Nyalindung', 5),
(51, 'Rajamandala Kulon', 5),
(52, 'Sarimukti', 5),
(53, 'Sumurbandung', 5),
(54, 'Bojongmekar', 6),
(55, 'Ciharashas', 6),
(56, 'Cipeundeuy', 6),
(57, 'Ciroyom', 6),
(58, 'Jatimekar', 6),
(59, 'Margalaksana', 6),
(60, 'Margaluyu', 6),
(61, 'Nanggeleng', 6),
(62, 'Nyenang', 6),
(63, 'Sirnagalih', 6),
(64, 'Sirnaraja', 6),
(65, 'Sukahaji', 6),
(66, 'Baranangsiang', 7),
(67, 'Cibenda', 7),
(68, 'Cicangkang Hilir', 7),
(69, 'Cijambu', 7),
(70, 'Cijenuk', 7),
(71, 'Cintaasih', 7),
(72, 'Citalem', 7),
(73, 'Girimukti', 7),
(74, 'Karangsari', 7),
(75, 'Mekarsari', 7),
(76, 'Neglasari', 7),
(77, 'Sarinagen', 7),
(78, 'Sirnagalih', 7),
(79, 'Sukamulya', 7),
(80, 'Cipada', 8),
(81, 'Jambudipa', 8),
(82, 'Kertawangi', 8),
(83, 'Padaasih', 8),
(84, 'Pasirhalang', 8),
(85, 'Pasirlangu', 8),
(86, 'Sadangmekar', 8),
(87, 'Tugumukti', 8),
(88, 'Bunijaya', 9),
(89, 'Celak', 9),
(90, 'Cilangari', 9),
(91, 'Gununghalu', 9),
(92, 'Sindangjaya', 9),
(93, 'Sirnajaya', 9),
(94, 'Sukasari', 9),
(95, 'Tamanjaya', 9),
(96, 'Wargasaluyu', 9),
(97, 'Bojongkoneng', 10),
(98, 'Cilame', 10),
(99, 'Cimanggu', 10),
(100, 'Cimareme', 10),
(101, 'Gadobangkong', 10),
(102, 'Margajaya', 10),
(103, 'Mekarsari', 10),
(104, 'Ngamprah', 10),
(105, 'Pakuhaji', 10),
(106, 'Sukatani', 10),
(107, 'Tanimulya', 10),
(108, 'Cempakamekar', 11),
(109, 'Ciburuy', 11),
(110, 'Cimerang', 11),
(111, 'Cipeundeuy', 11),
(112, 'Jayamekar', 11),
(113, 'Kertajaya', 11),
(114, 'Kertamulya', 11),
(115, 'Laksanamekar', 11),
(116, 'Padalarang', 11),
(117, 'Tagogapu', 11),
(118, 'Bojong', 12),
(119, 'Bojongsalam', 12),
(120, 'Cibedug', 12),
(121, 'Cibitung', 12),
(122, 'Cicadas', 12),
(123, 'Cinengah', 12),
(124, 'Sukamanah', 12),
(125, 'Sukaresmi', 12),
(126, 'Buninagara', 13),
(127, 'Cicangkang Girang', 13),
(128, 'Cikadu', 13),
(129, 'Cintakarya', 13),
(130, 'Mekarwangi', 13),
(131, 'Pasirpogor', 13),
(132, 'Puncaksari', 13),
(133, 'Ranca Senggang', 13),
(134, 'Sindangkerta', 13),
(135, 'Wangunsari', 13),
(136, 'Weninggalih', 13),
(137, 'Cibodas', 14),
(138, 'Cibogo', 14),
(139, 'Cikahuripan', 14),
(140, 'Cikidang', 14),
(141, 'Cikole', 14),
(142, 'Gudangkahuripan', 14),
(143, 'Jayagiri', 14),
(144, 'Kayuambon', 14),
(145, 'Langensari', 14),
(146, 'Lembang', 14),
(147, 'Mekarwangi', 14),
(148, 'Pagerwangi', 14),
(149, 'Sukajaya', 14),
(150, 'Suntenjaya', 14),
(151, 'Wangunharja', 14),
(152, 'Wangunsari', 14),
(153, 'Cipangeran', 15),
(154, 'Jati', 15),
(155, 'Cikande', 15),
(156, 'Bojongheulang', 15),
(157, 'Saguling', 15),
(158, 'Girimukti', 15),
(159, 'Cigugur Girang', 16),
(160, 'Cihanjuang Rahayu', 16),
(161, 'Cihanjuang', 16),
(162, 'Cihideung', 16),
(163, 'Ciwaruga', 16),
(164, 'Sariwangi', 16);

-- --------------------------------------------------------

--
-- Table structure for table `kontak`
--

CREATE TABLE `kontak` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `subjek` varchar(150) NOT NULL,
  `pesan` longtext NOT NULL,
  `tgl` datetime DEFAULT CURRENT_TIMESTAMP,
  `is_read` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontak`
--

INSERT INTO `kontak` (`id`, `nama`, `email`, `subjek`, `pesan`, `tgl`, `is_read`) VALUES
(1, 'Mochammad Lutfi A Hakim', 'mochammadlutfi125@gmail.com', 'Percobaan', 'asdasd', '2018-12-10 07:30:24', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lampiran`
--

CREATE TABLE `lampiran` (
  `id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `id_pengaduan` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lampiran`
--

INSERT INTO `lampiran` (`id`, `path`, `id_pengaduan`) VALUES
(1, '3651_Tugas_Basdat.docx', 190108001),
(2, '3651_Tugas_Basdat.pdf', 190108001),
(3, '3651_Tugas_Database_senin.docx', 190108001),
(4, '3651_Tugas_Basdat1.docx', 190108002),
(5, '3651_Tugas_Basdat1.pdf', 190108002),
(6, '3651_Tugas_Database_senin1.docx', 190108002),
(7, '3651_Tugas_Normalisasi.docx', 190113003),
(8, 'ali.jpg', 190113003),
(9, 'Book1.pdf', 190113003);

-- --------------------------------------------------------

--
-- Table structure for table `operator`
--

CREATE TABLE `operator` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `telp` varchar(15) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_kelurahan` int(11) NOT NULL,
  `active` enum('0','1') NOT NULL,
  `dihapus` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `operator`
--

INSERT INTO `operator` (`id`, `nama`, `username`, `email`, `password`, `telp`, `id_kecamatan`, `id_kelurahan`, `active`, `dihapus`) VALUES
(1, 'Desa Batujajar Barat', 'batujajarbarat', 'batujajarbarat@gmail.com', '$2a$08$PHTJ/TU90S3qq9S83Okoue7rsRFTy32A3Az4bGTs8gOJ8aXbrhDUG', '89726636271', 1, 1, '1', '0'),
(2, 'Desa Bojong Koneng', 'bojongkoneng', 'bojongkoneng@gmail.com', '$2a$08$eR/VWh4VQpvMaftSrIFHEexFbxsOjv25A7Ge4axfBsZTSpZGz.2/m', '123198421', 10, 97, '1', '0'),
(3, 'Desa Jambudipa', 'jambudipa', 'jambudipa@gmail.com', '$2a$08$YYA4x7QNCcrHdmcKMK1mOOnsGCUNPuT3k.wa1kNf2zewEeUehnQYK', '721881271', 8, 81, '1', '0'),
(4, 'Desa Pangauban', 'pangauban', 'pangabuan@gmail.com', '$2a$08$IwT.zeCOARNK7ewUvpkmGeurtuFwqgM2.Xv8wmatMOD7KHMiIp2Me', '8213129361', 1, 6, '1', '0'),
(5, 'Desa Mekarjaya', 'mekarjaya', 'mekarjaya@gmail.com', '$2a$08$jInao.Q0hGZMUn9nncNPZ.jSbWDHQsqA2JcM34fcr3GLaJ.WwM7Ka', '873281283', 2, 16, '1', '0'),
(6, 'Desa Batulayang', 'batulayang', 'batulayang@gmail.com', '$2a$08$BJ4IanMQGYLlwGcaK.XBDe7vR8v6dYGhI.VBVDYOEDADd2YWZBGve', '12123812', 4, 31, '1', '0'),
(7, 'Desa Pasirpogor', 'pasirpogor', 'pasirpogor@gmail.com', '$2a$08$ipiwjvLetsGUxJur07U4vejk8K1qdyFgv7JWBjnhX77Q0OnA49QSa', '12838613', 13, 131, '1', '0'),
(8, 'Desa Cijenuk', 'cijenuk', 'cijenuk@gmail.com', '$2a$08$3QeRaQfSB3FVWtgsIxjkd.PpzV2L2eRdCfjh3eG8L4j.qJP4rBDsa', '12345674321', 7, 70, '1', '0'),
(9, 'Desa Celak', 'celak', 'celak@gmail.com', '$2a$08$i2/G0X3XiC4dW1e1/CHrU.ziL8kJo3eHu8BPlRUrJJcs9aA5M06Ru', '89326', 9, 89, '1', '0'),
(10, 'Desa Cipangeran', 'cipangeran', 'cipangeran@gmail.com', '$2a$08$nLdQRiyeji4WMF7H5tqPs.XCEJTS5nOzy1omYeDWXakPDe6v.8zeG', '1274123', 15, 153, '1', '0'),
(11, 'Desa Padalarang', 'padalarang', 'padalarang@siduka.com', '$2a$08$lZmFnawaR4eZ.qpoDfBbJedjBKjmrr8zQVaYF3lebGe40K1Jx4bD6', 'asdals', 11, 116, '1', '0'),
(12, 'Desa Cisomang Barat', 'cisomangbarat', 'cisomang@siduka.com', '$2a$08$8leLh7GZp2AGHKrOAkLl5.MMSOKYUqRygK1DplKqyBsjmnPQTd7ly', '2129412', 2, 11, '1', '0'),
(13, 'Desa Cipatat', 'cipatat', 'cipatat@siduka.com', '$2a$08$u3sacq9kNIPAAk74FTQmv.0zAzol8fQbfnfPoRFty5qtrTalZUfKq', '123763', 5, 42, '1', '0'),
(14, 'Desa Nanggeleng', 'nanggeleng', 'nanggeleng@siduka.com', '$2a$08$5HANRFQA03mhTWoBGV7YiukDI7oVcckc627LnW/uTS5MIU6GSySju', '1298461982', 6, 61, '1', '0'),
(15, 'Desa Sukaresmi', 'sukaresmi', 'sukaresmi@siduka.com', '$2a$08$OL6KsmBDyMXeSrDb35Petu7pJ7VhpEA68y5CKW5XHtrkYEH6LoVqy', '129312', 12, 125, '1', '0'),
(16, 'Desa Cigugur Girang', 'cigugurgirang', 'cigugurgirang@siduka.com', '$2a$08$uzgOpmkWn4Q7.uC6kt86Le2BG6WRxL2BKKbeqGl9Lw8Cy7tyRkVqe', '9186329812', 16, 159, '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `pelapor`
--

CREATE TABLE `pelapor` (
  `id_pelapor` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` int(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `kd_aktivasi` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `tgl_daftar` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelapor`
--

INSERT INTO `pelapor` (`id_pelapor`, `nama`, `email`, `phone`, `password`, `kd_aktivasi`, `status`, `tgl_daftar`) VALUES
(1, 'Mochammad Lutfi', 'mochammadlutfi125@gmail.com', 2147483647, '$2a$08$fFsiUjFiS7.gqsQFPMB6Zeuv8qp37mS94LwYfjl6WsXkv4gCeNfYG', 'bdc217acb93368dcab888a9ab5a971ea', '1', '2019-01-13 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pengaduan`
--

CREATE TABLE `pengaduan` (
  `id_pengaduan` int(15) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `no_passport` varchar(80) NOT NULL,
  `tmp_lahir` varchar(150) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_kelurahan` int(11) NOT NULL,
  `alamat_asal` text NOT NULL,
  `alamat_kerja` text NOT NULL,
  `embarsi` varchar(150) NOT NULL,
  `debarsi` varchar(150) NOT NULL,
  `nm_pptkis` varchar(150) NOT NULL,
  `negara` varchar(150) NOT NULL,
  `mulai_kerja` date NOT NULL,
  `akhir_kerja` date NOT NULL,
  `lama_kerja` varchar(30) DEFAULT NULL,
  `nm_agency` varchar(150) NOT NULL,
  `alamat_agency` text NOT NULL,
  `f_nama_mjk` varchar(150) NOT NULL,
  `l_nama_mjk` varchar(150) NOT NULL,
  `jml_mjk` int(11) NOT NULL,
  `badan_usaha` varchar(150) NOT NULL,
  `alamat_mjk` text NOT NULL,
  `tuntutan` longtext NOT NULL,
  `masalah` longtext NOT NULL,
  `status_pmi` enum('0','1') NOT NULL,
  `status_pengaduan` enum('0','1') NOT NULL,
  `tgl` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_pelapor` int(11) DEFAULT NULL,
  `dihapus` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengaduan`
--

INSERT INTO `pengaduan` (`id_pengaduan`, `nama`, `no_passport`, `tmp_lahir`, `tgl_lahir`, `jk`, `id_kecamatan`, `id_kelurahan`, `alamat_asal`, `alamat_kerja`, `embarsi`, `debarsi`, `nm_pptkis`, `negara`, `mulai_kerja`, `akhir_kerja`, `lama_kerja`, `nm_agency`, `alamat_agency`, `f_nama_mjk`, `l_nama_mjk`, `jml_mjk`, `badan_usaha`, `alamat_mjk`, `tuntutan`, `masalah`, `status_pmi`, `status_pengaduan`, `tgl`, `id_pelapor`, `dihapus`) VALUES
(190108001, 'Mochammad Lutfi', '1234123512', 'Bandung', '2019-01-08', 'L', 1, 1, 'Jl. Entah Dimana Saya Juga bingung', 'Entah dimana saya juga bingung', 'Entah dimana', 'Gataulah', 'Tidak Diketahui', 'Saudi Arabia', '2019-01-15', '2019-01-09', 'asdasd', 'PT. Garuda Migrasi Indonesia', 'asdasd', 'Siti', 'Khomaerah', 2, 'tidak tau', 'kasnldans', 'dasdasdad', 'asdasdas', '1', '1', '2019-01-08 05:01:20', 1, '0'),
(190108002, 'Mochammad Lutfi', '1234123512', 'Bandung', '2019-01-08', 'L', 1, 1, 'Jl. Entah Dimana Saya Juga bingung', 'Entah dimana saya juga bingung', 'Entah dimana', 'Gataulah', 'Tidak Diketahui', 'Saudi Arabia', '2019-01-15', '2019-01-09', 'asdasd', 'PT. Garuda Migrasi Indonesia', 'asdasd', 'Siti', 'Khomaerah', 2, 'tidak tau', 'kasnldans', 'dasdasdad', 'asdasdas', '0', '0', '2019-01-08 05:01:27', 1, '0'),
(190113003, 'Dio Ilham Purnama Putra', '12839613289612', 'Bekasi', '1998-06-19', 'L', 1, 1, 'Tidak tau saya juga', 'Tidak tahu saya juga', 'tidak tau apa itu embarsi', 'tidak tau saya juga apa itu debarkasi', 'Tidak Diketahui', 'Saudi Arabia', '2019-01-12', '2019-01-11', '1 Tahun', 'Tidak tau apa yaa', 'tidak tau juga saya nihh', 'Apalagi ini saya gatau apa apa', 'Tidak tau niih', 1, 'Tidak ada', 'Tidak ada', 'Tidak ada tuntutan apa apa', 'Tidak tau mau laporin masalah apaan dah', '0', '0', '2019-01-13 03:07:50', 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `terusan`
--

CREATE TABLE `terusan` (
  `id` int(11) NOT NULL,
  `id_pengaduan` int(11) NOT NULL,
  `id_akses` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terusan`
--

INSERT INTO `terusan` (`id`, `id_pengaduan`, `id_akses`) VALUES
(1, 190108001, 6),
(2, 190108001, 7),
(3, 190108001, 8),
(4, 190108001, 9),
(5, 190108001, 10);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `username` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `telp` varchar(20) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `id_akses` int(11) NOT NULL,
  `active` enum('1','0') NOT NULL,
  `dihapus` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `username`, `email`, `telp`, `password`, `id_akses`, `active`, `dihapus`) VALUES
(1, 'Superadmin', 'superadmin', 'superadmin@gmail.com', '0123456789', '$2a$08$PHTJ/TU90S3qq9S83Okoue7rsRFTy32A3Az4bGTs8gOJ8aXbrhDUG', 1, '1', '0'),
(2, 'KADIS', 'kadis', 'kadis@gmail.com', '87213612351', '$2a$08$rDT5EOgxNfcuE4zeLNAGD.fKzfxNxO9im2cMZGZRK1O2.HXz0Yvau', 2, '1', '0'),
(3, 'KABID', 'kabid', 'kabid@gmail.com', '896123517251', '$2a$08$QuVhtsBxsIHPJGGtWXDnEOdj/GBCBJv9/ZYfRWiTFV8IxGSKnM8HG', 3, '1', '1'),
(5, 'KASUBBAG', 'kasubbag', 'kasubbag@gmail.com', '1234567', '$2a$08$7Dd8KPhqvydeFCrDFJSD7.Y5PLxguOZt0a53CJDGvEzPd5wf9SPJe', 4, '1', '0'),
(6, 'Admin', 'admin', 'admin@gmail.com', '124123213', '$2a$08$.Rkwl0sk8TG46NAfbl.7kOHPIlWu/Rb./WUTVbXS78vzxzdSKeJTK', 5, '1', '0'),
(7, 'Kemnaker', 'kemnaker', 'kemanaker@gmail.com', '87213612351', '$2a$08$rDT5EOgxNfcuE4zeLNAGD.fKzfxNxO9im2cMZGZRK1O2.HXz0Yvau', 9, '1', '0'),
(8, 'BNP2TKI', 'bnp2tki', 'bnp2tki@gmail.com', '896123517251', '$2a$08$QuVhtsBxsIHPJGGtWXDnEOdj/GBCBJv9/ZYfRWiTFV8IxGSKnM8HG', 6, '1', '0'),
(9, 'KEMENLU', 'kemenlu', 'kemenlu@gmail.com', '1234567', '$2a$08$7Dd8KPhqvydeFCrDFJSD7.Y5PLxguOZt0a53CJDGvEzPd5wf9SPJe', 8, '1', '0'),
(10, 'BP3TKI', 'bp3tki', 'bp3tki@gmail.com', '896123517251', '$2a$08$QuVhtsBxsIHPJGGtWXDnEOdj/GBCBJv9/ZYfRWiTFV8IxGSKnM8HG', 7, '1', '0'),
(11, 'Disnakertrans Jabar', 'disnakerjabar', 'disnakerjabar@gmail.com', '897666488262', '$2a$08$J8waj7MkT8J7qFgNA1POHeZT7C.DKw1a3D79Ob5XCQ7p3OzXi41eq', 10, '1', '0'),
(12, 'Percobaan', 'percobaan', 'percobaan@gmail.com', NULL, '$2a$08$eQyVy2x.gJUihVOkDR8cFuCJMsns15tJi2K3Lkc81wn.gcfgSX4eW', 2, '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user_akses`
--

CREATE TABLE `user_akses` (
  `id` int(11) NOT NULL,
  `nama` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_akses`
--

INSERT INTO `user_akses` (`id`, `nama`) VALUES
(1, 'Superadmin'),
(2, 'DISNAKERTRANS'),
(3, 'KABID'),
(4, 'KASUBBAG'),
(5, 'Admin'),
(6, 'BNP2TKI'),
(7, 'BP3TKI'),
(8, 'KEMENLU'),
(9, 'KEMNAKER'),
(10, 'DISNAKERTRANS-JABAR');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `baca_operator`
--
ALTER TABLE `baca_operator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `baca_user`
--
ALTER TABLE `baca_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kasus` (`id_pengaduan`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id_kelurahan`),
  ADD KEY `id_kecamatan` (`id_kecamatan`);

--
-- Indexes for table `kontak`
--
ALTER TABLE `kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lampiran`
--
ALTER TABLE `lampiran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kasus` (`id_pengaduan`);

--
-- Indexes for table `operator`
--
ALTER TABLE `operator`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kecamatan` (`id_kecamatan`),
  ADD KEY `id_kelurahan` (`id_kelurahan`);

--
-- Indexes for table `pelapor`
--
ALTER TABLE `pelapor`
  ADD PRIMARY KEY (`id_pelapor`);

--
-- Indexes for table `pengaduan`
--
ALTER TABLE `pengaduan`
  ADD PRIMARY KEY (`id_pengaduan`);

--
-- Indexes for table `terusan`
--
ALTER TABLE `terusan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pengaduan` (`id_pengaduan`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_akses` (`id_akses`);

--
-- Indexes for table `user_akses`
--
ALTER TABLE `user_akses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `baca_operator`
--
ALTER TABLE `baca_operator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `baca_user`
--
ALTER TABLE `baca_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `kelurahan`
--
ALTER TABLE `kelurahan`
  MODIFY `id_kelurahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;

--
-- AUTO_INCREMENT for table `kontak`
--
ALTER TABLE `kontak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lampiran`
--
ALTER TABLE `lampiran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `operator`
--
ALTER TABLE `operator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pelapor`
--
ALTER TABLE `pelapor`
  MODIFY `id_pelapor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pengaduan`
--
ALTER TABLE `pengaduan`
  MODIFY `id_pengaduan` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190113004;

--
-- AUTO_INCREMENT for table `terusan`
--
ALTER TABLE `terusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user_akses`
--
ALTER TABLE `user_akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD CONSTRAINT `kelurahan_ibfk_1` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`);

--
-- Constraints for table `lampiran`
--
ALTER TABLE `lampiran`
  ADD CONSTRAINT `lampiran_ibfk_1` FOREIGN KEY (`id_pengaduan`) REFERENCES `pengaduan` (`id_pengaduan`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
